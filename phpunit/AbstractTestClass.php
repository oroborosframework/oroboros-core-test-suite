<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests;

use \PHPUnit\Framework\TestCase;

/**
 * Provides some useful utility methods for
 */
abstract class AbstractTestClass
    extends TestCase
{

    /**
     * Provides support for suppressing false-flag errors
     * that interrupt the CI pipeline.
     */
    use \oroboros\tests\traits\utility\TestCaseExpectedErrorHandlerTrait;

/**
     * Provides support for casting parameters to a readable string
     * context for assertion messages.
     */
    use \oroboros\tests\traits\utility\ValueCastingUtilityTrait;

/**
     * Provides assertions to enforce the integrity of the
     * standardized Oroboros class identity schema.
     */
    use \oroboros\tests\traits\assertions\integrity\OroborosClassIntegrityTrait;

/**
     * Add baseline container assertion logic.
     */
    use \oroboros\tests\traits\assertions\container\ContainerEvaluationTrait;

/**
     * Add baseline contextual assertion logic.
     */
    use \oroboros\tests\traits\assertions\context\ContextualEvaluationTrait;

/**
     * Add serializable contextual assertion logic.
     */
    use \oroboros\tests\traits\assertions\context\ContextualSerialEvaluationTrait;

/**
     * Add json serializable contextual assertion logic.
     */
    use \oroboros\tests\traits\assertions\context\ContextualJsonSerialEvaluationTrait;

    /**
     * Can be overridden to provide a baseline expected test
     * object using the getTestClass method.
     */
    const TEST_CLASS = '\\stdClass';

    /**
     * Can be overridden to provide a baseline invalid expected test
     * object using the getInvalidTestClass method.
     */
    const TEST_CLASS_INVALID = false;

    /**
     * Can be overridden to provide a baseline misconfigured test
     * object using the getMisconfiguredTestClass method.
     */
    const TEST_CLASS_MISCONFIGURED = false;

    /**
     * Can be overridden to designate that the given
     * contract interface should be integrity checked.
     * If true, the provided testing class MUST implement
     * the given contract.
     */
    const TEST_CLASS_VALIDATE_CLASS_CONTRACT = false;

    /**
     * Can be overridden to designate that the given
     * api interface should be integrity checked.
     */
    const TEST_CLASS_VALIDATE_CLASS_API = false;

    /**
     * Can be overridden to designate that the given
     * enumerated interface should be integrity checked.
     */
    const TEST_CLASS_VALIDATE_CLASS_ENUMERATOR = false;

    /**
     * Can be overridden to designate that the given
     * set of interfaces should be integrity checked.
     * If true, ALL interfaces listed in the getExpectedInterfaces
     * method MUST be implemented by the testing subject.
     */
    const TEST_CLASS_VALIDATE_INTERFACES = false;

    /**
     * Can be overridden to designate that the given
     * set of interfaces should be integrity checked.
     * If true, ALL interfaces listed in the getExpectedInterfaces
     * method MUST be implemented by the testing subject.
     */
    const TEST_CLASS_VALIDATE_BASELINE_INTERFACES = false;

    /**
     * Can be overridden to designate that the given
     * class context designations should be integrity checked.
     */
    const TEST_CLASS_VALIDATE_DESIGNATIONS = false;

    /**
     * Can be overridden to designate that the given
     * class context should be integrity checked.
     */
    const TEST_CLASS_VALIDATE_CLASS_CONTEXT = false;

    /**
     * Can be overridden to designate that the given
     * class scope should be integrity checked.
     */
    const TEST_CLASS_VALIDATE_CLASS_TYPE = false;

    /**
     * Can be overridden to designate that the given
     * class scope should be integrity checked.
     */
    const TEST_CLASS_VALIDATE_CLASS_SCOPE = false;

    /**
     * Declares the root level interface that defines the
     * acceptable class contexts for the current scope.
     */
    const TEST_CLASS_ALLOWED_CLASS_CONTEXTS = '\\oroboros\\core\\interfaces\\enumerated\\context\\ClassContext';

    /**
     * Declares the root level interface that defines the
     * acceptable class types for the current scope.
     */
    const TEST_CLASS_ALLOWED_CLASS_TYPES = '\\oroboros\\core\\interfaces\\enumerated\\type\\ClassType';

    /**
     * Declares the root level interface that defines the
     * acceptable class scopes for the current scope.
     */
    const TEST_CLASS_ALLOWED_CLASS_SCOPES = '\\oroboros\\core\\interfaces\\enumerated\\scope\\ClassScope';

    /**
     * Can be overridden to provide a contract interface that
     * the testing class subject is expected to implement.
     */
    const TEST_CLASS_EXPECTED_CONTRACT = false;

    /**
     * Can be overridden to provide an api interface that
     * the testing class subject is expected to either implement,
     * or declare indirectly via the OROBOROS_API class constant.
     */
    const TEST_CLASS_EXPECTED_API = false;

    /**
     * Can be overridden to provide an enumerated interface that
     * the testing class subject is expected to implement.
     */
    const TEST_CLASS_EXPECTED_ENUMERATOR = false;

    /**
     * Can be overridden to provide an explicit class context that
     * the testing class subject is expected to declare via the
     * OROBOROS_CLASS_CONTEXT class constant.
     */
    const TEST_CLASS_EXPECTED_CLASS_CONTEXT = false;

    /**
     * Can be overridden to provide an explicit class context that
     * the testing class subject is expected to declare via the
     * OROBOROS_CLASS_TYPE class constant.
     */
    const TEST_CLASS_EXPECTED_CLASS_TYPE = false;

    /**
     * Can be overridden to provide an explicit class context that
     * the testing class subject is expected to declare via the
     * OROBOROS_CLASS_SCOPE class constant.
     */
    const TEST_CLASS_EXPECTED_CLASS_SCOPE = false;

    /**
     * Override this constant to declare a default contextual type
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_TYPE = null;

    /**
     * Override this constant to declare a default contextual sub-type
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_SUBTYPE = null;

    /**
     * Override this constant to declare a default contextual context
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_CONTEXT = null;

    /**
     * Override this constant to declare a default contextual value
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_VALUE = null;

    /**
     * Override this constant to declare a default contextual category
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_CATEGORY = null;

    /**
     * Override this constant to declare a default contextual sub-category
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_SUBCATEGORY = null;

    /**
     * Override this constant to declare a default contextual package
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_PACKAGE = null;

    /**
     * Override this constant to declare a default contextual sub-package
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_SUBPACKAGE = null;

    /**
     * Override this constant to declare an expected contextual type
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_TYPE = null;

    /**
     * Override this constant to declare an expected contextual sub-type
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_SUBTYPE = null;

    /**
     * Override this constant to declare an expected contextual context
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_CONTEXT = null;

    /**
     * Override this constant to declare an expected contextual value
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_VALUE = null;

    /**
     * Override this constant to declare an expected contextual category
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_CATEGORY = null;

    /**
     * Override this constant to declare an expected contextual sub-category
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_SUBCATEGORY = null;

    /**
     * Override this constant to declare an expected contextual package
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_PACKAGE = null;

    /**
     * Override this constant to declare an expected contextual sub-package
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_SUBPACKAGE = null;

    /**
     * Can be overridden by child classes to designate parameters that should
     * always be provided when the given test class is initialized, or the
     * first parameter that is passed directly to new instances of the
     * defined test class in the TEST_CLASS class constant.
     * @var null|array
     */
    protected $_test_parameters = null;

    /**
     * Can be overridden by child classes to designate dependencies that should
     * always be provided when the given test class is initialized, or the
     * second parameter that is passed directly to new instances of the
     * defined test class in the TEST_CLASS class constant.
     * @var null|array
     */
    protected $_test_dependencies = null;

    /**
     * Can be overridden by child classes to designate flags that should
     * always be provided when the given test class is initialized, or the
     * third parameter that is passed directly to new instances of the
     * defined test class in the TEST_CLASS class constant.
     * @var null|array
     */
    protected $_test_flags = null;

    /**
     * Designates whether the baseline test suite setup has occurred.
     * This operation is only performed one time per runtime.
     * @var bool
     */
    private static $_core_test_setup_initialized = false;

    /**
     * Represents the baseline pre-initialized mock core object,
     * which is cloned out upon request for testing purposes.
     * @var \oroboros\testclass\core\MockOroboros
     */
    private static $_core_preinitialized_mock_object;

    /**
     * Performs the baseline initialization if it has not already been done,
     * and otherwise just passes through to the normal PhpUnit constructor.
     * @param type $name
     * @param array $data
     * @param type $dataName
     */
    public function __construct( $name = null, array $data = array(),
        $dataName = '' )
    {
        parent::__construct( $name, $data, $dataName );
    }

    /**
     * Provides the declared test parameters,
     * or a key from them if it is an array.
     * @param scalar $key
     * @return mixed
     */
    protected function getTestParameters( $key = null )
    {
        if ( is_null( $key ) )
        {
            return $this->_test_parameters;
        }
        if ( is_array( $this->_test_parameters ) && array_key_exists( $key,
                $this->_test_parameters ) )
        {
            return $this->_test_parameters[$key];
        }
        return null;
    }

    /**
     * Provides the declared test dependencies,
     * or a key from them if it is an array.
     * @param scalar $key
     * @return mixed
     */
    protected function getTestDependencies( $key = null )
    {
        if ( is_null( $key ) )
        {
            return $this->_test_dependencies;
        }
        if ( is_array( $this->_test_dependencies ) && array_key_exists( $key,
                $this->_test_dependencies ) )
        {
            return $this->_test_dependencies[$key];
        }
        return null;
    }

    /**
     * Provides the declared test flags,
     * or a key from them if it is an array.
     * @param scalar $key
     * @return mixed
     */
    protected function getTestFlags( $key = null )
    {
        if ( is_null( $key ) )
        {
            return $this->_test_flags;
        }
        if ( is_array( $this->_test_flags ) && array_key_exists( $key,
                $this->_test_flags ) )
        {
            return $this->_test_flags[$key];
        }
        return null;
    }

    /**
     * Returns an empty, uninitialized mock core object for test purposes.
     * This should be loaded by base level trees of the test classes one time
     * and retained to prevent excessive initialization overhead.
     *
     * A pre-initialized, otherwise empty version is provided using getMockCore,
     * which will prevent the need to re-run initialization. That method should
     * be used in the majority of cases.
     *
     * @return \oroboros\testclass\core\MockOroboros
     */
    protected function getMockCore()
    {
        if ( is_null( self::$_core_preinitialized_mock_object ) )
        {
            $this->instantiateBaselineCoreTestObject();
        }
        return clone self::$_core_preinitialized_mock_object;
    }

    /**
     * Returns an empty, uninitialized mock core object for test purposes.
     * This should be loaded by base level trees of the test classes one time
     * and retained to prevent excessive initialization overhead.
     *
     * A pre-initialized, otherwise empty version is provided using getMockCore,
     * which will prevent the need to re-run initialization. That method should
     * be used in the majority of cases.
     *
     * @return \oroboros\testclass\core\MockOroboros
     */
    protected function getNewMockCore()
    {
        $mock = new \oroboros\testclass\core\MockOroboros();
        $mock::reset();
        return $mock;
    }

    /**
     * Returns a pre-initialized test class based on what is
     * declared in the TEST_CLASS class constant for simplified
     * testing purposes of valid test objects. If this method cannot
     * resolve, the test will exit with a fatal error, and print a
     * verbose error analysis in the console.
     * @return object
     */
    protected function getTestClass()
    {
        $class = $this::TEST_CLASS;
        try
        {
            return new $class();
        } catch ( \Exception $e )
        {
            $this->renderTestclassError( $this::TEST_CLASS,
                sprintf( 'An exception occurred while loading the testing class instance: [%s]',
                    $e->getMessage() ), $e->getTrace() );
        }
    }

    /**
     * Returns a pre-initialized invalid test class based on what is
     * declared in the TEST_CLASS_INVALID class constant for simplified
     * testing purposes of invalid test objects. If this method cannot
     * resolve, the test will exit with a fatal error, and print a
     * verbose error analysis in the console.
     * @return object
     */
    protected function getInvalidTestClass()
    {
        $class = $this::TEST_CLASS_INVALID;
        if ( !$class )
        {
            return false;
        }
        try
        {
            return new $class();
        } catch ( \Exception $e )
        {
            $this->renderTestclassError( $this::TEST_CLASS_INVALID,
                sprintf( 'An exception occurred while loading the testing invalid class instance: [%s]',
                    $e->getMessage() ), $e->getTrace() );
        }
    }

    /**
     * Returns a pre-initialized invalid test class based on what is
     * declared in the TEST_CLASS_MISCONFIGURED class constant for simplified
     * testing purposes of misconfigured test objects. If this method cannot
     * resolve, the test will exit with a fatal error, and print a
     * verbose error analysis in the console.
     * @return object
     */
    protected function getMisconfiguredTestClass()
    {
        $class = $this::TEST_CLASS_MISCONFIGURED;
        if ( !$class )
        {
            return false;
        }
        try
        {
            return new $class();
        } catch ( \Exception $e )
        {
            $this->renderTestclassError( $this::TEST_CLASS_MISCONFIGURED,
                sprintf( 'An exception occurred while loading the testing misconfigured class instance: [%s]',
                    $e->getMessage() ), $e->getTrace() );
        }
    }

    /**
     * Returns a simple stringable object for testing purposes of
     * string casting or validation logic.
     * @param type $value
     * @return \oroboros\testclass\assets\StringableObject
     */
    protected function getStringableObject( $value )
    {
        $class = '\\oroboros\\testclass\\assets\\StringableObject';
        if ( !is_scalar( $value ) )
        {
            $this->renderTestclassError( $class,
                sprintf( 'Stringable object value must be scalar, but received type: [%s]',
                    gettype( $value ) ), debug_backtrace( 1 ) );
        }
        return new $class( $value );
    }

    /**
     * Generates a console-formatted output error and exits
     * testing with a fail status.
     * @param type $subject
     * @param type $context
     * @param type $trace
     */
    protected function renderTestclassError( $subject, $context, $trace )
    {
        $error_prefix = \oroboros\cli\interfaces\enumerated\ColorCodes::CLI_TEXT_ESCAPE_PREFIX
            . \oroboros\cli\interfaces\enumerated\ColorCodes::CLI_COLOR_FOREGROUND_RED
            . \oroboros\cli\interfaces\enumerated\ColorCodes::CLI_TEXT_ESCAPE_SUFFIX;
        $error_suffix = \oroboros\cli\interfaces\enumerated\ColorCodes::CLI_TEXT_ESCAPE_PREFIX
            . \oroboros\cli\interfaces\enumerated\ColorCodes::CLI_TEXT_RESET
            . \oroboros\cli\interfaces\enumerated\ColorCodes::CLI_TEXT_ESCAPE_SUFFIX;
        if ( is_array( $trace ) )
        {
            $trace = \oroboros\format\BacktraceFormatter::toString( $trace );
        }
        $class = get_class( $this );
        $body = sprintf( 'A test setup error occurred in [%2$s]%1$sThe following error occurred [%3$s]%1$sIn given subject [%4$s]%1$sBacktrace:%1$s%1$s%5$s%1$s%1$s',
            PHP_EOL, $class, $subject, $context, $trace );
        echo $error_prefix . sprintf( '%1$s---------------- << SETUP ERROR >> ----------------%1$s'
            . '%1$s%2$s'
            . '%1$s-------------- << TEST TERMINATED >> --------------'
            . '%1$s', PHP_EOL, $body ) . $error_suffix;
        die( PHP_EOL . $error_prefix
            . '<<< Failed to load test object >>>'
            . $error_suffix . PHP_EOL
        );
    }

    /**
     * Gets the class name from objects
     * @param type $class
     * @return type
     * @internal
     */
    private function _getClassName( $class )
    {
        if ( is_object( $class ) )
        {
            $class = get_class( $class );
        }
        return $class;
    }

    /**
     * Performs the initial Oroboros core test suite environment
     * setup if it has not already been done.
     * @return void
     * @internal
     */
    private function setupCoreTestEnvironment()
    {
        if ( self::$_core_test_setup_initialized )
        {
            //we only need to do this one time.
            return;
        }
        $this->instantiateBaselineCoreTestObject();
        self::$_core_test_setup_initialized = true;
    }

    /**
     * Provides a pre-initialized mock core object for testing purposes
     * against a default, blank Oroboros Core object.
     */
    private function instantiateBaselineCoreTestObject()
    {
        $mock = $this->getNewMockCore();
        $mock::staticInitialize();
        self::$_core_preinitialized_mock_object = $mock;
    }

}
