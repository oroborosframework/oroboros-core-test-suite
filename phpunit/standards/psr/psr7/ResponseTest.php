<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\standards\psr\psr7;

/**
 * <Psr7 Response Stream Test Cases>
 * These tests prove the stable compliance of Psr-7 response streams.
 * @group standards
 * @group psr
 * @group psr7
 * @group stream
 * @group response
 * @group message
 * @covers \oroboros\message\Response
 * @covers \oroboros\message\abstracts\AbstractResponse
 * @covers \oroboros\message\traits\ResponseTrait
 * @covers \oroboros\message\interfaces\contract\ResponseContract
 * @covers \oroboros\core\traits\core\StaticBaselineTrait
 * @covers \oroboros\core\traits\core\BaselineTrait
 * @covers \oroboros\core\traits\core\BaselineInternalsTrait
 * @covers \oroboros\collection\traits\CollectionTrait
 * @covers \oroboros\collection\traits\ContainerTrait
 * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
 * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
 */
class ResponseTest
    extends MessageTest
{

    const CONCRETE_CLASS = '\\oroboros\\message\\Response';

    /**
     * @group standards
     * @group psr
     * @group psr7
     * @group stream
     * @group response
     * @group message
     * @covers \oroboros\message\Response
     * @covers \oroboros\message\abstracts\AbstractResponse
     * @covers \oroboros\message\traits\ResponseTrait
     * @covers \oroboros\message\interfaces\contract\ResponseContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testResponseDefaults()
    {
        $response = $this->_getDefaultObject();
        $this->assertTrue( $response->getStatusCode() === 200 );
        $this->assertTrue( $response->getReasonPhrase() === 'OK' );
    }

    /**
     * @group standards
     * @group psr
     * @group psr7
     * @group stream
     * @group response
     * @group message
     * @covers \oroboros\message\Response
     * @covers \oroboros\message\abstracts\AbstractResponse
     * @covers \oroboros\message\traits\ResponseTrait
     * @covers \oroboros\message\interfaces\contract\ResponseContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testResponseStatusCode()
    {
        $response = $this->_getDefaultObject();
        $this->assertTrue( $response->withStatus( 201 )->getStatusCode() === 201 );
        $this->assertTrue( $response->withStatus( 301, 'Foobar' )->getStatusCode()
            === 301 );
    }

    /**
     * @group standards
     * @group psr
     * @group psr7
     * @group stream
     * @group response
     * @group message
     * @covers \oroboros\message\Response
     * @covers \oroboros\message\abstracts\AbstractResponse
     * @covers \oroboros\message\traits\ResponseTrait
     * @covers \oroboros\message\interfaces\contract\ResponseContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testResponseReasonPhrase()
    {
        $response = $this->_getDefaultObject();
        $this->assertTrue( $response->withStatus( 201 )->getReasonPhrase() === 'Created' );
        $this->assertTrue( $response->withStatus( 301, 'Foobar' )->getReasonPhrase()
            === 'Foobar' );
    }

    /**
     * Fetches the default initialization parameters
     * @return array
     */
    protected function _getDefaultParams()
    {
        $params = array(
            'stream' => file_get_contents( __DIR__ . '/streamfile.txt' ),
            'headers' => array(
                'content-type' => 'text/plain',
                'content-disposition' => 'attachment; filename=foobarzippityzang.txt',
                'test-header' => 'foo, bar, baz'
            ),
            'protocol' => '1.1',
            'code' => 200,
            'reason' => 'OK'
        );
        return $params;
    }

    /**
     * Fetches the default test object
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function _getDefaultObject()
    {
        $class = $this::CONCRETE_CLASS;
        $params = $this->_getDefaultParams();
        return new $class( $params['stream'], $params['headers'],
            $params['protocol'], $params['code'], $params['reason'] );
    }

}
