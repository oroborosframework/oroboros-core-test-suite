<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\standards\psr\psr7;

/**
 * <Psr7 Stream Test Cases>
 * These tests prove the stable compliance of Psr-7 streams.
 * @group standards
 * @group psr
 * @group psr7
 * @group stream
 * @covers \oroboros\message\Stream
 * @covers \oroboros\message\abstracts\AbstractStream
 * @covers \oroboros\message\traits\StreamTrait
 * @covers \oroboros\message\interfaces\contract\StreamContract
 * @covers \oroboros\core\traits\core\StaticBaselineTrait
 * @covers \oroboros\core\traits\core\BaselineTrait
 * @covers \oroboros\core\traits\core\BaselineInternalsTrait
 * @covers \oroboros\collection\traits\CollectionTrait
 * @covers \oroboros\collection\traits\ContainerTrait
 * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
 * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
 */
class StreamTest
    extends \oroboros\tests\AbstractTestClass
{

    const CONCRETE_CLASS = '\\oroboros\\message\\Stream';

    /**
     * @group standards
     * @group psr
     * @group psr7
     * @group stream
     * @covers \oroboros\message\Stream
     * @covers \oroboros\message\abstracts\AbstractStream
     * @covers \oroboros\message\traits\StreamTrait
     * @covers \oroboros\message\interfaces\contract\StreamContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testStreamValid()
    {
        $class = $this::CONCRETE_CLASS;
        $stream = $this->_getDefaultObject();
        $this->assertTrue( $stream instanceof $class );
    }

    /**
     * @group standards
     * @group psr
     * @group psr7
     * @group stream
     * @covers \oroboros\message\Stream
     * @covers \oroboros\message\abstracts\AbstractStream
     * @covers \oroboros\message\traits\StreamTrait
     * @covers \oroboros\message\interfaces\contract\StreamContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testStreamDetach()
    {
        $stream = $this->_getDefaultObject();
        $this->assertTrue( is_resource( $stream->detach() ) );
        $this->assertTrue( is_null( $stream->detach() ) );
    }

    /**
     * @group standards
     * @group psr
     * @group psr7
     * @group stream
     * @covers \oroboros\message\Stream
     * @covers \oroboros\message\abstracts\AbstractStream
     * @covers \oroboros\message\traits\StreamTrait
     * @covers \oroboros\message\interfaces\contract\StreamContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testStreamToString()
    {
        $stream = $this->_getDefaultObject();
        $stream->rewind();
        $this->assertTrue( trim( $stream->getContents() ) === 'Test stream content.' );
        $this->assertTrue( trim( (string) $stream ) === 'Test stream content.' );
    }

    /**
     * @group standards
     * @group psr
     * @group psr7
     * @group stream
     * @covers \oroboros\message\Stream
     * @covers \oroboros\message\abstracts\AbstractStream
     * @covers \oroboros\message\traits\StreamTrait
     * @covers \oroboros\message\interfaces\contract\StreamContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testStreamMetaData()
    {
        $stream = $this->_getDefaultObject();
        $metadata = $stream->getMetaData();
        $this->assertTrue( $metadata["timed_out"] === false );
        $this->assertTrue( $metadata["blocked"] === true );
        $this->assertTrue( $metadata["eof"] === false );
        $this->assertTrue( $metadata["wrapper_type"] === "plainfile" );
        $this->assertTrue( $metadata["stream_type"] === "STDIO" );
        $this->assertTrue( $metadata["unread_bytes"] === 0 );
        $this->assertTrue( $metadata["seekable"] );
    }

    /**
     * @group standards
     * @group psr
     * @group psr7
     * @group stream
     * @covers \oroboros\message\Stream
     * @covers \oroboros\message\abstracts\AbstractStream
     * @covers \oroboros\message\traits\StreamTrait
     * @covers \oroboros\message\interfaces\contract\StreamContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testStreamCache()
    {
        $stream = $this->_getDefaultObject();
        $this->assertTrue( $stream->isReadable() );
        $this->assertTrue( $stream->isWritable() );
    }

    /**
     * @group standards
     * @group psr
     * @group psr7
     * @group stream
     * @covers \oroboros\message\Stream
     * @covers \oroboros\message\abstracts\AbstractStream
     * @covers \oroboros\message\traits\StreamTrait
     * @covers \oroboros\message\interfaces\contract\StreamContract
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testStreamPointer()
    {
        $stream = $this->_getDefaultObject();
        $stream->rewind();
        $size = $stream->getSize();
        $this->assertTrue( $stream->tell() === 0 );
        $stream->seek( $size );
        $this->assertTrue( $stream->eof() );
        $stream->rewind();
        $this->assertTrue( $stream->tell() === 0 );
        $this->assertFalse( $stream->eof() );
    }

    /**
     * Fetches the default initialization parameters
     * @return array
     */
    protected function _getDefaultParams()
    {
        $params = array(
            'stream' => fopen( __DIR__ . '/streamfile.txt', 'r+b' ),
        );
        return $params;
    }

    /**
     * Fetches the default test object
     * @return \Psr\Http\Message\StreamInterface
     */
    protected function _getDefaultObject()
    {
        $class = $this::CONCRETE_CLASS;
        $params = $this->_getDefaultParams();
        return new $class( $params['stream'] );
    }

}
