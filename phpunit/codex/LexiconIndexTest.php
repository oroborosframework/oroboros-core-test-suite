<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\codex;

use PHPUnit\Framework\TestCase;

/**
 * @group codex
 * @group lexicon
 * @group container
 * @group index
 * @covers \oroboros\core\traits\core\StaticBaselineTrait
 * @covers \oroboros\core\traits\core\BaselineTrait
 * @covers \oroboros\core\traits\core\BaselineInternalsTrait
 * @covers \oroboros\collection\traits\CollectionTrait
 * @covers \oroboros\collection\traits\ContainerTrait
 * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
 * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
 */
class LexiconIndexTest
    extends TestCase
{

    /**
     * @group codex
     * @group lexicon
     * @group container
     * @group index
     * @covers \oroboros\codex\traits\LexiconTrait
     * @covers \oroboros\codex\traits\LexiconIndexTrait
     * @covers \oroboros\codex\traits\LexiconArchiveTrait
     * @covers \oroboros\codex\traits\LexiconEntryTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\enum\traits\InterfaceEnumeratorTrait
     * @covers \oroboros\parse\traits\JsonTrait
     * @covers \oroboros\core\traits\patterns\behavioral\ManagerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\DirectorTrait
     * @covers \oroboros\core\traits\patterns\behavioral\WorkerTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testLexiconIndexValid()
    {
        $subjects = $this->_getTestInstances();

        $lex = new \oroboros\codex\LexiconIndex( 'unit-test',
            $subjects );
        $this->assertTrue( $lex->isInitialized() );
        $serial = serialize( $lex );
        $this->assertInstanceOf( '\\oroboros\\codex\\interfaces\\contract\\LexiconIndexContract',
            unserialize( $serial ) );
        $lex->save( $this->_getSavePath() );
        $this->assertEquals( $lex->getKey(), 'unit-test' );
        $this->assertInstanceOf( '\\oroboros\\collection\\interfaces\\contract\\CollectionContract',
            $lex->versions( 'unit-test', $this->_getSavePath() ) );
    }

    /**
     * @group codex
     * @group lexicon
     * @group container
     * @group index
     * @covers \oroboros\codex\traits\LexiconTrait
     * @covers \oroboros\codex\traits\LexiconIndexTrait
     * @covers \oroboros\codex\traits\LexiconArchiveTrait
     * @covers \oroboros\codex\traits\LexiconEntryTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\enum\traits\InterfaceEnumeratorTrait
     * @covers \oroboros\parse\traits\JsonTrait
     * @covers \oroboros\core\traits\patterns\behavioral\ManagerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\DirectorTrait
     * @covers \oroboros\core\traits\patterns\behavioral\WorkerTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testLexiconIndexFromApiInterface()
    {
        $subject = '\\oroboros\\core\\interfaces\\api\\standards\\psr\\Psr7Api';

        $lex = \oroboros\codex\LexiconIndex::import( $subject );
        $this->assertTrue( $lex->isInitialized() );
        $serial = serialize( $lex );
        $this->assertInstanceOf( '\\oroboros\\codex\\interfaces\\contract\\LexiconIndexContract',
            unserialize( $serial ) );
        $lex->save( $this->_getSavePath() );
        $this->assertEquals( $subject::API_CODEX, $lex->getKey() );
        $this->assertEquals( $subject::API_PACKAGE, $lex->getPackage() );
        $this->assertInstanceOf( '\\oroboros\\collection\\interfaces\\contract\\CollectionContract',
            $lex->versions( $subject::API_CODEX, $this->_getSavePath() ) );
        $this->assertTrue( $lex->checkEntry( 'stream-concrete-class' ) );
        $this->assertFalse( $lex->checkEntry( 'stream-fake-class' ) );
        $entry = $lex->pullEntry( 'stream-concrete-class' );
        $this->assertInstanceOf( '\\oroboros\\codex\\interfaces\\contract\\LexiconEntryContract',
            $entry );
        $this->assertInstanceOf( '\\SplFileInfo', $entry->getFile() );
    }

    /**
     * Returns an array of testing entries for the LexiconEntry
     * @return array
     */
    private function _getTestInstances()
    {
        $objects = array();
        $classes = array(
            'trait' => '\\oroboros\\core\\traits\\OroborosTrait',
            'interface' => '\\oroboros\\core\\interfaces\\api\\OroborosApi',
            'abstract' => '\\oroboros\\core\\abstracts\\libraries\\AbstractLibrary',
            'concrete' => '\\oroboros\\message\\Stream',
//            'closure' => function()
//            {
//
//            },
//            'function' => '\\oroboros\\testclasses\\DefaultFunction',
//            'invalid' => '\\not\\a\\real\\Class'
        );
        foreach ( $classes as
            $key =>
            $classname )
        {
            $classes[$key] = new \oroboros\codex\LexiconEntry( $classname );
        }
        return $classes;
    }

    private function _getSavePath()
    {
        $dir = \oroboros\Oroboros::TEMP_DIRECTORY . 'codex/lexicon/';
        if ( !realpath( $dir ) )
        {
            mkdir( $dir );
        }
        return $dir;
    }

}
