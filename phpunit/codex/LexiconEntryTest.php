<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\codex;

use PHPUnit\Framework\TestCase;

/**
 * @group codex
 * @group lexicon
 * @group entry
 * @covers \oroboros\core\traits\utilities\core\ConstructUtilityTrait
 * @covers \oroboros\codex\traits\LexiconEntryTrait
 * @covers \oroboros\core\traits\utilities\reflection\ReflectionTrait
 * @covers \oroboros\enum\traits\InterfaceEnumeratorTrait
 * @covers \oroboros\core\traits\core\StaticBaselineTrait
 * @covers \oroboros\core\traits\core\BaselineTrait
 * @covers \oroboros\core\traits\core\BaselineInternalsTrait
 * @covers \oroboros\collection\traits\CollectionTrait
 * @covers \oroboros\collection\traits\ContainerTrait
 * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
 * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
 */
class LexiconEntryTest
    extends TestCase
{

    /**
     * @group codex
     * @group lexicon
     * @group entry
     * @covers \oroboros\core\traits\patterns\behavioral\ManagerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\DirectorTrait
     * @covers \oroboros\core\traits\patterns\behavioral\WorkerTrait
     * @covers \oroboros\codex\traits\LexiconTrait
     * @covers \oroboros\codex\traits\LexiconIndexTrait
     * @covers \oroboros\core\traits\utilities\reflection\ReflectionTrait
     * @covers \oroboros\enum\traits\InterfaceEnumeratorTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testLexInterface()
    {
        $subjects = $this->_getTestInstances();
        try
        {
            //this is an invalid case
            new \oroboros\codex\LexiconEntry( false );
            throw new Exception( 'LexiconEntry failed to bounce bad parameter [bool: false]' );
        } catch ( \oroboros\core\utilities\exception\InvalidArgumentException $e )
        {
            $this->assertTrue( $e->getCode() === \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS );
        }
        $lex = new \oroboros\codex\LexiconEntry( $subjects['interface'] );
        $serial = serialize( $lex );
        $this->assertStringEndsWith( '}', $serial );
        $this->assertTrue( unserialize( $serial ) instanceof \oroboros\codex\interfaces\contract\LexiconEntryContract );
        $this->assertTrue( $lex->getVendor() === \oroboros\Oroboros::OROBOROS_VENDOR_NAMESPACE );
        $this->assertTrue( $lex->getReflector() instanceof \oroboros\core\utilities\reflection\Reflection );
        $this->assertTrue( $lex->getApi() instanceof \oroboros\enum\InterfaceEnumerator );
        $this->assertTrue( $lex->getDocBlock() instanceof \oroboros\collection\interfaces\contract\CollectionContract );
        $this->assertNull( $lex->getMethods() );
        $this->assertTrue( $lex->getConstants() instanceof \oroboros\collection\interfaces\contract\CollectionContract );
        $this->assertNull( $lex->getProperties() );
        $this->assertTrue( $lex->getInterfaces() instanceof \oroboros\collection\interfaces\contract\CollectionContract );
        $this->assertNull( $lex->getTraits() );
        $this->assertNull( $lex->getParent() );
        $this->assertTrue( $lex->getFile() instanceof \SplFileInfo );
        $this->assertTrue( $lex->getDirectory( true ) instanceof \DirectoryIterator );
        $this->assertTrue( is_string( $lex->getDirectory() ) );
        $this->assertTrue( $lex->inNamespace( 'oroboros\\core' ) );
        $this->assertFalse( $lex->inNamespace( 'oroboros\\boogity' ) );
        $this->assertTrue( $lex->isInstanceOf( 'oroboros\\core\\interfaces\\api\\BaseApi' ) );
        $this->assertFalse( $lex->isInstanceOf( 'oroboros\\core\\interfaces\\Foobar' ) );
        $this->assertTrue( $lex->isCompiled() );
        $this->assertFalse( $lex->isClosure() );
        $this->assertFalse( $lex->isCallable() );
        $this->assertTrue( $lex->isInterface() );
        $this->assertFalse( $lex->isTrait() );
        $this->assertTrue( $lex->hasInterface( 'oroboros\\core\\interfaces\\api\\BaseApi' ) );
        $this->assertFalse( $lex->hasInterface( 'oroboros\\core\\interfaces\\Foobar' ) );
        $this->assertFalse( $lex->hasInterface( 'oroboros\\core\\traits\\OroborosTrait' ) );
        $this->assertTrue( $lex->hasParent( 'oroboros\\core\\interfaces\\api\\BaseApi' ) );
        $this->assertTrue( is_array( $lex->getApi()->toArray() ) );
        $this->assertTrue( is_string( $lex->getDocBlock( true ) ) );
    }

    /**
     * @group codex
     * @group lexicon
     * @group entry
     * @covers \oroboros\core\traits\utilities\core\ConstructUtilityTrait
     * @covers \oroboros\codex\traits\LexiconEntryTrait
     * @covers \oroboros\core\traits\utilities\reflection\ReflectionTrait
     * @covers \oroboros\enum\traits\InterfaceEnumeratorTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testLexConcrete()
    {
        $subjects = $this->_getTestInstances();
        $lex = new \oroboros\codex\LexiconEntry( $subjects['concrete'] );
        $serial = serialize( $lex );
        $this->assertStringEndsWith( '}', $serial );
        $this->assertTrue( unserialize( $serial ) instanceof \oroboros\codex\interfaces\contract\LexiconEntryContract );
        $this->assertTrue( $lex->getVendor() === \oroboros\Oroboros::OROBOROS_VENDOR_NAMESPACE );
        $this->assertTrue( $lex->getReflector() instanceof \oroboros\core\utilities\reflection\Reflection );
        $this->assertTrue( $lex->getApi() instanceof \oroboros\enum\InterfaceEnumerator );
        $this->assertTrue( $lex->getDocBlock() instanceof \oroboros\collection\interfaces\contract\CollectionContract );
        $this->assertTrue( $lex->getMethods() instanceof \oroboros\collection\interfaces\contract\CollectionContract );
        $this->assertTrue( $lex->getConstants() instanceof \oroboros\collection\interfaces\contract\CollectionContract );
        $this->assertNull( $lex->getProperties() ); //properties come from trait, not the class
//        $this->assertTrue( $lex->getProperties() instanceof \oroboros\collection\interfaces\contract\CollectionContract );
        $this->assertTrue( $lex->getInterfaces() instanceof \oroboros\collection\interfaces\contract\CollectionContract );
        $this->assertNull( $lex->getTraits() );
        $this->assertTrue( $lex->getTraits( 1 ) instanceof \oroboros\collection\interfaces\contract\CollectionContract );
        $this->assertNull( $lex->getTraits() );
        $this->assertTrue( $lex->getParent() instanceof \oroboros\codex\interfaces\contract\LexiconEntryContract );
        $this->assertTrue( $lex->getFile() instanceof \SplFileInfo );
        $this->assertTrue( $lex->getDirectory( true ) instanceof \DirectoryIterator );
        $this->assertTrue( is_string( $lex->getDirectory() ) );
        $this->assertTrue( $lex->inNamespace( 'oroboros\\message' ) );
        $this->assertFalse( $lex->inNamespace( 'oroboros\\boogity' ) );
        $this->assertTrue( $lex->isInstanceOf( 'oroboros\\core\\interfaces\\api\\BaseApi' ) );
        $this->assertFalse( $lex->isInstanceOf( 'oroboros\\core\\interfaces\\Foobar' ) );
        $this->assertTrue( $lex->isCompiled() );
        $this->assertFalse( $lex->isClosure() );
        $this->assertFalse( $lex->isCallable() );
        $this->assertFalse( $lex->isInterface() );
        $this->assertFalse( $lex->isTrait() );
        $this->assertTrue( $lex->hasInterface( 'oroboros\\core\\interfaces\\api\\BaseApi' ) );
        $this->assertFalse( $lex->hasInterface( 'oroboros\\core\\interfaces\\Foobar' ) );
        $this->assertFalse( $lex->hasInterface( 'oroboros\\core\\traits\\OroborosTrait' ) );
        $this->assertTrue( $lex->hasParent( 'oroboros\\core\\interfaces\\api\\BaseApi' ) );
        $this->assertTrue( is_array( $lex->getApi()->toArray() ) );
        $this->assertTrue( is_string( $lex->getDocBlock( true ) ) );
    }

    /**
     * Returns an array of testing entries for the LexiconEntry
     * @return array
     */
    private function _getTestInstances()
    {
        return array(
            'trait' => '\\oroboros\\core\\traits\\OroborosTrait',
            'interface' => '\\oroboros\\core\\interfaces\\api\\OroborosApi',
            'abstract' => '\\oroboros\\core\\abstracts\\AbstractBase',
            'concrete' => '\\oroboros\\message\\Stream',
            'closure' => function()
            {

            },
            'function' => '\\oroboros\\testclasses\\DefaultFunction',
            'invalid' => '\\not\\a\\real\\Class'
        );
    }

}
