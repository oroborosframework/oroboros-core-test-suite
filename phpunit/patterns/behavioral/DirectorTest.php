<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\patterns\behavioral;

/**
 * <Director Pattern Test Cases>
 * These tests prove the stable compliance of director patterns.
 * A Director issues commands to a worker, or relays commands from external sources.
 * @group patterns
 * @group behavioral
 * @group director
 * @group worker
 * @covers \oroboros\core\interfaces\contract\patterns\behavioral\DirectorContract
 * @covers \oroboros\core\traits\patterns\behavioral\DirectorTrait
 * @covers \oroboros\core\traits\core\BaselineTrait
 * @covers \oroboros\core\traits\core\BaselineInternalsTrait
 * @covers \oroboros\collection\traits\CollectionTrait
 * @covers \oroboros\collection\traits\ContainerTrait
 * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
 * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
 */
class DirectorTest
    extends WorkerTest
{

    const TEST_CLASS = '\\oroboros\\testclass\\patterns\\behavioral\\director\\DefaultDirector';

    protected $expected_director_category = 'unit-test';
    protected $expected_scope = 'director-test';

    /**
     * @group patterns
     * @group behavioral
     * @group director
     * @group worker
     * @covers \oroboros\core\interfaces\contract\patterns\behavioral\DirectorContract
     * @covers \oroboros\core\traits\patterns\behavioral\DirectorTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testDirectorValid()
    {
        $test = $this->getTestClass();
        $this->assertFalse( $test->getDirectorCategory() );
        try
        {
            $test->setBadDirectorCategory();
        } catch ( \oroboros\core\utilities\exception\InvalidArgumentException $e )
        {

        }
        $test->setTestParameters();
        $this->assertEquals( $this->expected_director_category,
            $test->getDirectorCategory() );
        $this->assertFalse( $test->setWorker( $test->getOutOfScopeWorker() ) );
        $this->assertTrue( $test->setWorker( $test->getDefaultWorker() ) );
        $this->assertTrue( $test->checkWorkerScope( $this->expected_worker_scope ) );
        $this->assertFalse( $test->checkWorkerScope( 'fake-scope' ) );
        $this->assertTrue( in_array( $this->expected_worker_scope,
                $test->getWorkerScopes() ) );
    }

}
