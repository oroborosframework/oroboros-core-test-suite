<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\patterns\creational;

/**
 * <Factory Test Cases>
 * These tests prove the stable functionality of the Factory.
 * @group patterns
 * @group creational
 * @group factory
 * @covers \oroboros\core\traits\patterns\creational\PrototyperTrait
 * @covers \oroboros\core\traits\patterns\creational\PrototypicalTrait
 * @covers \oroboros\core\traits\core\StaticBaselineTrait
 * @covers \oroboros\core\traits\core\BaselineTrait
 * @covers \oroboros\core\traits\core\BaselineInternalsTrait
 * @covers \oroboros\collection\traits\CollectionTrait
 * @covers \oroboros\collection\traits\ContainerTrait
 * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
 * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
 */
class FactoryTest
    extends \oroboros\tests\patterns\PatternTest
{

    const TEST_CLASS = '\\oroboros\\testclass\\patterns\\creational\\factory\\DefaultFactory';
    const TEST_CLASS_EXPECTED_CONTRACT = '\\oroboros\\core\\interfaces\\contract\\patterns\\creational\\FactoryContract';

    protected $_factory_parameters = array(
        'valid-create' => array(
            'value',
            'foobar'
        ),
        'valid-create-keyed' => array(
            'context' => 'value',
            'value' => 'foobar'
        ),
        'invalid-create' => array(
            null,
            null
        ),
        'valid-vendor' => 'oroboros\\testclass',
        'invalid-vendor' => 'not\\a\\real\\vendor',
        'valid-path' => 'internal\\container\\Container',
        'invalid-path' => 'not\\a\\real\\class',
        'invalid-path-abstract' => 'abstracts\\core\\context\\AbstractContext',
        'invalid-path-bad-constructor' => 'internal\\context\\MetaContextPrivateConstructor',
        'valid-contract' => '\\oroboros\\core\\interfaces\\contract\\core\\context\\ContextualContract',
        'invalid-contract-not-exists' => '\\FakeInterface',
        'invalid-contract-not-constrained' => '\\Serializable',
    );

    /**
     * @group patterns
     * @group creational
     * @group factory
     * @covers \oroboros\core\traits\patterns\creational\FactoryTrait
     * @covers \oroboros\core\traits\core\context\CoreContextTrait
     * @covers \oroboros\core\traits\core\context\JsonSerialContextTrait
     * @covers \oroboros\core\traits\core\context\SerialContextTrait
     * @covers \oroboros\core\traits\core\context\ContextTrait
     * @uses \oroboros\core\traits\core\context\meta\MetaContextTrait
     */
    public function testCleanInitialization()
    {
        try
        {
            $test = $this->getTestClass();
            $classname = $this::TEST_CLASS;
            $valid = $this->_factory_parameters['valid-create-keyed'];
            $valid['extraRedundantParameterThatShouldNotMatter'] = 'foobar';
            $this->assertTrue( $test->isValid( $this->_factory_parameters['valid-create'] ) );
            $this->assertTrue( $test->isValid( $this->_factory_parameters['valid-create-keyed'] ) );
            $this->assertTrue( $test->isValid( $valid ) );
            $expected_class = trim( $test->getValue(), '\\' );
            $expected_contract = trim( $test->getContext(), '\\' );
            $parameters = $test->getParameters();
            $required_parameters = $test->getRequiredParameters();
            if ( !empty( $required_parameters ) )
            {
                $this->assertFalse( $test->isValid() );
            }
            foreach ( $required_parameters as
                $pos =>
                $ref )
            {
                //All required parameters should be in the array of full parameters.
                $this->assertTrue( in_array( $ref, $parameters ) );
            }
            $result = $test->create( $this->_factory_parameters['valid-create'] );
            $result2 = $test->create( $this->_factory_parameters['valid-create-keyed'] );
            $this->assertEquals( $result, $result2 );
            $this->assertInstanceOf( $expected_contract, $result );
            $this->assertEquals( $expected_class, get_class( $result ) );
            $test2 = $test->withContract( $this->_factory_parameters['valid-contract'] );
            $this->assertNotSame( $test->getContext(), $test2->getContext() );
            $test2 = $test->withVendor( $this->_factory_parameters['valid-vendor'] );
            $this->assertNotSame( $test->getValue(), $test2->getValue() );
            $test2 = $test->withPath( $this->_factory_parameters['valid-path'] );
            $this->assertNotEquals( $test->getParameters(),
                $test2->getParameters() );

            try
            {
                //Bad class, from new
                new $classname( $this->_factory_parameters['invalid-path'] );
                $this->assertTrue( false,
                    'Failed to throw an expected exception.' );
            } catch ( \PHPUnit\Exception $e )
            {
                //Do now suppress testing framework exceptions
                print_r(array($e->getMessage(), $e->getTraceAsString()));
                throw $e;
            } catch ( \InvalidArgumentException $e )
            {
                //expected
                $this->assertTrue( true );
            }
            try
            {
                //Bad class, from recast
                $test->withPath( $this->_factory_parameters['invalid-path'] );
                $this->assertTrue( false,
                    'Failed to throw an expected exception.' );
            } catch ( \PHPUnit\Exception $e )
            {
                //Do now suppress testing framework exceptions
                throw $e;
            } catch ( \InvalidArgumentException $e )
            {
                //expected
                $this->assertTrue( true );
            }
            try
            {
                //Type error on contract declaration
                $test->withContract( false );
                $this->assertTrue( false,
                    'Failed to throw an expected exception.' );
            } catch ( \PHPUnit\Exception $e )
            {
                //Do now suppress testing framework exceptions
                throw $e;
            } catch ( \InvalidArgumentException $e )
            {
                //expected
                $this->assertTrue( true );
            }
            try
            {
                //Contract does not extend the existing one, from new
                new $classname( null, null,
                    $this->_factory_parameters['invalid-contract-not-constrained'] );
                $this->assertTrue( false,
                    'Failed to throw an expected exception.' );
            } catch ( \PHPUnit\Exception $e )
            {
                //Do now suppress testing framework exceptions
                throw $e;
            } catch ( \InvalidArgumentException $e )
            {
                //expected
                $this->assertTrue( true );
            }
            try
            {
                //Contract does not extend the existing one, from recast
                $test->withContract( $this->_factory_parameters['invalid-contract-not-constrained'] );
                $this->assertTrue( false,
                    'Failed to throw an expected exception.' );
            } catch ( \PHPUnit\Exception $e )
            {
                //Do now suppress testing framework exceptions
                throw $e;
            } catch ( \InvalidArgumentException $e )
            {
                //expected
                $this->assertTrue( true );
            }
            try
            {
                //Contract is not an interface
                new $classname( null, null,
                    $this->_factory_parameters['invalid-contract-not-exists'] );
                $this->assertTrue( false,
                    'Failed to throw an expected exception.' );
            } catch ( \PHPUnit\Exception $e )
            {
                //Do now suppress testing framework exceptions
                throw $e;
            } catch ( \InvalidArgumentException $e )
            {
                //expected
                $this->assertTrue( true );
            }
            try
            {
                //Abstract subject
                new $classname( $this->_factory_parameters['invalid-path-abstract'] );
                $this->assertTrue( false,
                    'Failed to throw an expected exception.' );
            } catch ( \PHPUnit\Exception $e )
            {
                //Do now suppress testing framework exceptions
                throw $e;
            } catch ( \InvalidArgumentException $e )
            {
                //expected
                $this->assertTrue( true );
            }
            try
            {
                //Private constructor
                $test->withVendor( $this->_factory_parameters['valid-vendor'] )->withPath( $this->_factory_parameters['invalid-path-bad-constructor'] );
                $this->assertTrue( false,
                    'Failed to throw an expected exception.' );
            } catch ( \PHPUnit\Exception $e )
            {
                //Do now suppress testing framework exceptions
                throw $e;
            } catch ( \InvalidArgumentException $e )
            {
                //expected
                $this->assertTrue( true );
            }
            try
            {
                //Not in contract scope
                $test->withContract( $this->_factory_parameters['valid-contract'] )->withPath( $this->_factory_parameters['valid-path'] );
                $this->assertTrue( false,
                    'Failed to throw an expected exception.' );
            } catch ( \PHPUnit\Exception $e )
            {
                //Do now suppress testing framework exceptions
                throw $e;
            } catch ( \InvalidArgumentException $e )
            {
                //expected
                $this->assertTrue( true );
            }
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertTrue( false,
                sprintf( 'An unexpected exception was raised while testing default '
                    . 'factory instance [%1$s] of type [%2$s] with message'
                    . ' [%3$s] at method [%4$s] of test class [%5$s].',
                    $this::TEST_CLASS, get_class( $e ), $e->getMessage(),
                    __METHOD__, get_class( $this ) ) );
        }
    }

    /**
     * Sets up the expected set of 3rd party or PHP internal
     * interfaces that the given test class MUST honor
     */
    public static function declareExpectedInterfaces()
    {
        return array_merge(
            parent::declareExpectedInterfaces(),
            array(
            '\\oroboros\\core\\interfaces\\contract\\patterns\\creational\\FactoryContract',
            '\\oroboros\\core\\interfaces\\contract\\core\\context\\ContextualContract'
            ) );
    }

    protected function getTestClass()
    {
        $class = $this::TEST_CLASS;
        return new $class();
    }

}
