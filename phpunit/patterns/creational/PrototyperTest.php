<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\patterns\creational;

/**
 * <Prototyper Test Cases>
 * These tests prove the stable functionality of the Prototyper,
 * and that it correctly handles errors. As of this writing,
 * only the base level prototyper has a concrete implementation,
 * so more granular scope and config tests do not yet apply.
 * They will be enabled when overrides are in place that render
 * them warranted, and this comment will be revised to reflect this.
 * @group patterns
 * @group creational
 * @group prototype
 * @covers \oroboros\core\traits\patterns\creational\PrototyperTrait
 * @covers \oroboros\core\traits\patterns\creational\PrototypicalTrait
 * @covers \oroboros\core\traits\core\StaticBaselineTrait
 * @covers \oroboros\core\traits\core\BaselineTrait
 * @covers \oroboros\core\traits\core\BaselineInternalsTrait
 * @covers \oroboros\collection\traits\CollectionTrait
 * @covers \oroboros\collection\traits\ContainerTrait
 * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
 * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
 */
class PrototyperTest
    extends \oroboros\tests\patterns\PatternTest
{

    /**
     * @group patterns
     * @group creational
     * @group prototype
     * @covers \oroboros\core\traits\patterns\creational\PrototyperTrait
     * @covers \oroboros\core\traits\patterns\creational\PrototypicalTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testCleanInitialization()
    {
        try
        {
            $testclass = new \oroboros\testclass\patterns\creational\prototyper\DefaultPrototyper();
            $testclass->initialize();
            $this->assertTrue( true );
        } catch ( Exception $e )
        {
            $this->assertTrue( false );
        }
    }

    /**
     * @group patterns
     * @group creational
     * @group prototype
     * @covers \oroboros\core\traits\patterns\creational\PrototyperTrait
     * @covers \oroboros\core\traits\patterns\creational\PrototypicalTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testCleanDestructor()
    {
        try
        {
            $testclass = new \oroboros\testclass\patterns\creational\prototyper\DefaultPrototyper();
            $testclass->initialize();
            unset( $testclass );
            $this->assertTrue( true );
        } catch ( Exception $e )
        {
            $this->assertTrue( false );
        }
    }

    /**
     * @group patterns
     * @group creational
     * @group prototype
     * @covers \oroboros\core\traits\patterns\creational\PrototyperTrait
     * @covers \oroboros\core\traits\patterns\creational\PrototypicalTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testPrototypeTestClass()
    {
        try
        {
            $testclass = new \oroboros\testclass\patterns\creational\prototyper\ValidPrototype();
            $testclass->initialize();
            $this->assertTrue( true );
        } catch ( Exception $e )
        {
            $this->assertTrue( false );
        }
    }

    /**
     * @group patterns
     * @group creational
     * @group prototype
     * @covers \oroboros\core\traits\patterns\creational\PrototyperTrait
     * @covers \oroboros\core\traits\patterns\creational\PrototypicalTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testPrototypeRegistration()
    {
        $test_prototyper = new \oroboros\testclass\patterns\creational\prototyper\DefaultPrototyper();
        $test_prototyper->initialize();
        $test_prototypical = new \oroboros\testclass\patterns\creational\prototyper\ValidPrototype();
        $test_prototypical->initialize();
        $this->assertNull( $test_prototyper->setPrototype( $test_prototypical ) );
        $test_prototyper->set( 'test', $test_prototypical );
        $result = $test_prototyper->get( 'test' );
    }

    /**
     * @group patterns
     * @group creational
     * @group prototype
     * @covers \oroboros\core\traits\patterns\creational\PrototyperTrait
     * @covers \oroboros\core\traits\patterns\creational\PrototypicalTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testPrototypeUnregistration()
    {
        $test_prototyper = new \oroboros\testclass\patterns\creational\prototyper\DefaultPrototyper();
        $test_prototyper->initialize();
        $test_prototypical = new \oroboros\testclass\patterns\creational\prototyper\ValidPrototype();
        $test_prototypical->initialize();
        $test_prototyper->setPrototype( $test_prototypical );
        $this->assertTrue( $test_prototyper->unSetPrototype( get_class( $test_prototypical ) ) );
    }

    /**
     * @group patterns
     * @group creational
     * @group prototype
     * @covers \oroboros\core\traits\patterns\creational\PrototyperTrait
     * @covers \oroboros\core\traits\patterns\creational\PrototypicalTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testFailCaseInvalidArgument()
    {
        $test_prototyper = new \oroboros\testclass\patterns\creational\prototyper\DefaultPrototyper();
        $test_prototyper->initialize();
        try
        {
            //Insures invalid classes throw an exception
            $test_prototyper->setPrototype( '\\ThisIsNotARealClassNameLol' );
            //the following line should never be reached
            $this->assertTrue( false );
        } catch ( \InvalidArgumentException $e )
        {
            $this->assertTrue( true );
        } catch ( \Exception $e )
        {
            //If we reach here, it threw the wrong type of exception. Fail it.
            $this->assertTrue( false );
        }
        try
        {
            //Insures passing an integer throws an exception
            $test_prototyper->setPrototype( 0 ); //should only take a valid object or classname
            //the following line should never be reached
            $this->assertTrue( false );
        } catch ( \InvalidArgumentException $e )
        {
            $this->assertTrue( true );
        }
        try
        {
            //insures passing null explicitly throws an exception
            $test_prototyper->setPrototype( null ); //should only take a valid object or classname
            //the following line should never be reached
            $this->assertTrue( false );
        } catch ( \InvalidArgumentException $e )
        {
            $this->assertTrue( true );
        }
        try
        {
            //insures passing boolean explicitly throws an exception
            $test_prototyper->setPrototype( true ); //should only take a valid object or classname
            //the following line should never be reached
            $this->assertTrue( false );
        } catch ( \InvalidArgumentException $e )
        {
            $this->assertTrue( true );
        }
        try
        {
            //insures passing a resource throws an exception
            $test_prototyper->setPrototype( fopen( 'php://memory', 'w' ) ); //should only take a valid object or classname
            //the following line should never be reached
            $this->assertTrue( false );
        } catch ( \InvalidArgumentException $e )
        {
            $this->assertTrue( true );
        }
        try
        {
            //insures passing a resource throws an exception
            $test_prototyper->get( 'settings' ); //should only take a valid object or classname
            //the following line should never be reached
            $this->assertTrue( false );
        } catch ( \InvalidArgumentException $e )
        {
            $this->assertTrue( true );
        }
        try
        {
            //insures passing a resource throws an exception
            $test_prototyper->set( 'settings', 'nope' ); //should only take a valid object or classname
            //the following line should never be reached
            $this->assertTrue( false );
        } catch ( \InvalidArgumentException $e )
        {
            $this->assertTrue( true );
        }
        try
        {
            //insures passing a resource throws an exception
            $test_prototyper->unsetPrototype( 'settings' ); //should only take a valid object or classname
            //the following line should never be reached
            $this->assertTrue( false );
        } catch ( \InvalidArgumentException $e )
        {
            $this->assertTrue( true );
        }
        try
        {
            //insures passing a resource throws an exception
            $test_prototyper->setPrototype( 'settings' ); //should only take a valid object or classname
            //the following line should never be reached
            $this->assertTrue( false );
        } catch ( \InvalidArgumentException $e )
        {
            $this->assertTrue( true );
        }
    }

    /**
     * @group patterns
     * @group creational
     * @group prototype
     * @covers \oroboros\core\traits\patterns\creational\PrototyperTrait
     * @covers \oroboros\core\traits\patterns\creational\PrototypicalTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testFailCaseObjectNotResolvedCorrectly()
    {
        $test_prototyper = new \oroboros\testclass\patterns\creational\prototyper\DefaultPrototyper();
        $test_prototyper->initialize();
        $test_prototypical = new \oroboros\testclass\patterns\creational\prototyper\MisconfiguredValidPrototype();
        $test_prototypical->initialize();
        try
        {
            $test_prototyper->setPrototype( $test_prototypical );
            $expected_failcase = $test_prototyper->getPrototype( get_class( $test_prototypical ) );
            //the following line should never be reached
            $this->assertTrue( false );
        } catch ( \Exception $e )
        {
            $this->assertTrue( true );
        }
    }

    /**
     * @group patterns
     * @group creational
     * @group prototype
     * @covers \oroboros\core\traits\patterns\creational\PrototyperTrait
     * @covers \oroboros\core\traits\patterns\creational\PrototypicalTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testFailCaseNotPrototypical()
    {
        $test_prototyper = new \oroboros\testclass\patterns\creational\prototyper\DefaultPrototyper();
        $test_prototyper->initialize();
        $test_prototypical = new \oroboros\testclass\patterns\creational\prototyper\InvalidPrototype(); //this is an empty class that doesn't initialize
        try
        {
            $test_prototyper->setPrototype( $test_prototypical );
            //the following line should never be reached
            $this->assertTrue( false );
        } catch ( \Exception $e )
        {
            $this->assertTrue( true );
        }
    }

    /**
     * @group patterns
     * @group creational
     * @group prototype
     * @covers \oroboros\core\traits\patterns\creational\PrototyperTrait
     * @covers \oroboros\core\traits\patterns\creational\PrototypicalTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testFailCaseNotRegistered()
    {
        $test_prototyper = new \oroboros\testclass\patterns\creational\prototyper\DefaultPrototyper();
        $test_prototyper->initialize();
        $this->assertFalse( $test_prototyper->getPrototype( '\\AClassThatIsntRegisteredorDoesntExistAtAll' ) );
    }

    /**
     * @group patterns
     * @group creational
     * @group prototype
     * @covers \oroboros\core\traits\patterns\creational\PrototyperTrait
     * @covers \oroboros\core\traits\patterns\creational\PrototypicalTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testFailCaseInvalidUnregistration()
    {
        $test_prototyper = new \oroboros\testclass\patterns\creational\prototyper\DefaultPrototyper();
        $test_prototyper->initialize();
        $this->assertFalse( $test_prototyper->unSetPrototype( '\\AClassThatIsntRegisteredorDoesntExistAtAll' ) );
    }

    /**
     * @group patterns
     * @group creational
     * @group prototype
     * @covers \oroboros\core\traits\patterns\creational\PrototyperTrait
     * @covers \oroboros\core\traits\patterns\creational\PrototypicalTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testFailCaseAlreadyRegistered()
    {
        $test_prototyper = new \oroboros\testclass\patterns\creational\prototyper\DefaultPrototyper();
        $test_prototyper->initialize();
        $test_prototypical = new \oroboros\testclass\patterns\creational\prototyper\ValidPrototype();
        $test_prototypical->initialize();
        $test_prototyper->setPrototype( $test_prototypical );
        try
        {
            $test_prototyper->setPrototype( $test_prototypical );
            //this line should never be reached
            $this->assertTrue( false );
        } catch ( \oroboros\core\utilities\exception\UnexpectedValueException $e )
        {
            $this->assertTrue( true );
        }
    }

    /**
     * @group patterns
     * @group creational
     * @group prototype
     * @covers \oroboros\core\traits\patterns\creational\PrototyperTrait
     * @covers \oroboros\core\traits\patterns\creational\PrototypicalTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testPrototypicalCheck()
    {
        $test_prototyper = new \oroboros\testclass\patterns\creational\prototyper\DefaultPrototyper();
        $test_prototyper->initialize();
        $test_prototypical = new \oroboros\testclass\patterns\creational\prototyper\ValidPrototype();
        $test_prototypical->initialize();
        $not_prototypical = new \stdClass();
        $this->assertTrue( $test_prototyper->checkIfPrototypical( $test_prototypical ) );
        $this->assertFalse( $test_prototyper->checkIfPrototypical( $not_prototypical ) );
    }

    /**
     * @group patterns
     * @group creational
     * @group prototype
     * @covers \oroboros\core\traits\patterns\creational\PrototyperTrait
     * @covers \oroboros\core\traits\patterns\creational\PrototypicalTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testListPrototypes()
    {
        $test_prototyper = new \oroboros\testclass\patterns\creational\prototyper\DefaultPrototyper();
        $test_prototyper->initialize();
        $test_prototypical = new \oroboros\testclass\patterns\creational\prototyper\ValidPrototype();
        $test_prototypical->initialize();
        $test_prototyper->setPrototype( $test_prototypical );
        $this->assertTrue( count( $test_prototyper->listPrototypes() ) === 1 );
        $this->assertTrue( $test_prototyper->checkPrototype( $test_prototypical ) );
        $test_prototyper->reset();
        $this->assertTrue( empty( $test_prototyper->listPrototypes() ) );
        $this->assertFalse( $test_prototyper->checkPrototype( $test_prototypical ) );
    }

}
