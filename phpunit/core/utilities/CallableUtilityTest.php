<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\core\baseline;

use \PHPUnit\Framework\TestCase;

/**
 * @group core
 * @group utilities
 * @covers \oroboros\core\traits\utilities\core\CallableUtilityTrait
 */
class CallableUtilityTest
    extends TestCase
{

    use \oroboros\core\traits\utilities\core\CallableUtilityTrait;

    /**
     * This SHOULD assert true.
     * @group core
     * @group utilities
     * @covers \oroboros\core\traits\utilities\core\CallableUtilityTrait
     */
    public function testCallable()
    {
        $test = new \oroboros\testclass\core\utilities\DefaultCallableTester();
        $class = get_class( $test );
        $this->assertTrue( true );
        $param = 'foo';
        $method_base = 'call';
        $static_method_base = 'static_call';
        $method = $method_base . '0';
        $static_method = $static_method_base . '0';
        try
        {
            //This should throw a BadFunctionCallException.
            $this->_callableUtilityCall( false );
            throw new \Exception( 'Failed to throw expected exception on non-callable operation in [\\oroboros\\core\\traits\\utilities\\core\\CallableUtilityTrait].' );
        } catch ( \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract $e )
        {
            $this->assertInstanceOf( '\\oroboros\\core\\utilities\\exception\\BadFunctionCallException',
                $e );
            $this->assertEquals( $e->getCode(),
                \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PHP_BAD_FUNCTION_CALL );
        }
        $this->assertTrue( $this->_callableUtilityCall( array(
                $test,
                $method ) ) );
        $this->assertTrue( $this->_callableUtilityCall(
                $class . '::' . $static_method
        ) );
        for ( $i = 0;
            $i < 16;
            $i++ )
        {
            $params = array();
            for ( $ii = 0;
                $ii < $i;
                $ii++ )
            {
                $params[] = $param;
            }
            $method = $method_base . (string) count( $params );
            $static_method = $static_method_base . (string) count( $params );
            $this->assertTrue( $this->_callableUtilityCall( array(
                    $class,
                    $static_method ), $params ) );
            $this->assertTrue( $this->_callableUtilityCall( array(
                    $test,
                    $method ), $params ) );
            $this->assertTrue( $this->_callableUtilityCall( $class . '::' . $static_method,
                    $params ) );
        }
    }

}
