<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\core\container;

/**
 * <Container Set Container Test Cases>
 * These tests prove the stable functionality of core set container objects.
 * @group core
 * @group container
 * @covers \oroboros\core\traits\core\container\ContainerSetTrait
 * @covers \oroboros\core\traits\core\container\SetTrait
 * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
 * @covers \oroboros\core\traits\core\container\ContainerTrait
 */
class ContainerSetTest
    extends ConstrainedContainerTest
{

    use \oroboros\tests\traits\assertions\container\ContainerSetEvaluationTrait;
    use \oroboros\tests\traits\tests\container\ContainerSetTestTrait;

    const TEST_CLASS = '\\oroboros\\core\\internal\\container\\ContainerSet';
    const TEST_CLASS_EXPECTED_CONTRACT = '\\oroboros\\core\\interfaces\\contract\\core\\container\\ContainerSetContract';
    const TEST_CLASS_EXPECTED_INTERFACE = '\\Psr\\Container\\ContainerInterface';
    const TEST_CLASS_EXPECTED_API = '\\oroboros\\core\\interfaces\\api\\core\\CoreApi';

    /**
     * Performs assertions to insure that negative constraint checks
     * fire the correct exceptions.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\ContainerSetTrait
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     * @return void
     */
    public function testSetFailConstraints()
    {
        if ( !get_class( $this ) === __CLASS__ )
        {
            //Only run this test one time
            //from the root container test instance.
            $this->assertTrue( true );
            return;
        }
        $class = $this->getTestClass();
        $classname = get_class( $class );
        $test_array = array(
            'foo' => array(),
            'bar' => new \stdClass()
        );
        foreach ( $test_array as
            $key =>
            $value )
        {
            try
            {
                //Class MUST raise a ContainerException on an invalid key/value pair.
                $class->offsetSet( $key, $value );
                $this->assertTrue( false,
                    sprintf( 'Failed to throw expected exception [%1$s] for offsetSet '
                        . 'in instance of [%2$s] for invalid set key in [%3$s] of test class [%4$s]',
                        '\\Psr\\Container\\ContainerExceptionInterface',
                        $classname, __METHOD__, get_class( $this ) )
                );
            } catch ( \PHPUnit\Exception $e )
            {
                //Do now suppress testing framework exceptions
                throw $e;
            } catch ( \Exception $e )
            {
                $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                    $e,
                    sprintf( 'Failed to assert expected validation exception [%1$s] occurred on '
                        . 'an invalid set key as an instance of [%2$s] for offsetSet '
                        . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                        get_class( $e ),
                        '\\Psr\\Container\\ContainerExceptionInterface',
                        $classname, __METHOD__, get_class( $this ) )
                );
            }
        }
        try
        {
            //Class MUST raise a ContainerException on a duplicate key.
            $class->offsetSet( 'val',
                new \oroboros\core\internal\container\Container() );
            $class->offsetSet( 'val',
                new \oroboros\core\internal\container\Container() );
            $this->assertTrue( false,
                sprintf( 'Failed to throw expected exception [%1$s] for duplicate key detection via offsetSet '
                    . 'in instance of [%2$s] for invalid set key in [%3$s] of test class [%4$s]',
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected validation exception [%1$s] occurred on '
                    . 'an invalid set key as an instance of [%2$s] for offsetSet '
                    . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        }
    }

}
