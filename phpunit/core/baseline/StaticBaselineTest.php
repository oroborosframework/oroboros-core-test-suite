<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\core\baseline;

/**
 * <Static Baseline Test Cases>
 * These tests prove the stable functionality of the StaticBaselineTrait,
 * which is used as the basis of logic for abstract level classes and most all
 * classes that provide only static functionality. This trait constitutes the
 * foundation of Oroboros Core static abstraction.
 *
 * This test also covers foundational functionality relied upon universally
 * of some other expected traits, which provide foundational logic to the core.
 * As their stability is required for the baseline internal stability, they are
 * also marked as covered within the scope of their operations with baseline
 * logic, though testing them fully is not the focus of this test class.
 *
 * @group core
 * @group baseline
 * @covers \oroboros\core\traits\core\BaselineInternalsTrait
 * @covers \oroboros\validate\traits\ValidatorTrait
 * @covers \oroboros\core\traits\utilities\exception\ExceptionUtilityTrait
 * @covers \oroboros\core\traits\OroborosTrait
 */
class StaticBaselineTest
    extends BaselineInternalsTest
{

    const TEST_CLASS = '\\oroboros\\testclass\\core\\baseline\\DefaultBaselineInternal';

    /**
     * @group core
     * @group baseline
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\core\traits\OroborosTrait
     */
    public function testStaticInitialization()
    {
        $test = $this->getTestClass();
        $this->assertTrue( true );
        $modes = $test->_validationModes();
    }

    /**
     * Sets up the expected set of 3rd party or PHP internal
     * interfaces that the given test class MUST honor
     */
    public static function declareExpectedInterfaces()
    {
        return array_merge(
            parent::declareExpectedInterfaces(), array() );
    }

}
