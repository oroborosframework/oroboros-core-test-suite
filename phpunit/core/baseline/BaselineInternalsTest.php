<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\core\baseline;

/**
 * <Baseline Internals Test Cases>
 * These tests prove the stable functionality of the BaselineInternalsTrait,
 * which is used as the basis of logic for both the StaticBaselineTrait
 * and BaselineTrait. These two traits constitute the core foundation of
 * Oroboros core universal functionality.
 *
 * This test also covers foundational functionality relied upon universally
 * of some other expected traits, which provide foundational logic to the core.
 * As their stability is required for the baseline internal stability, they are
 * also marked as covered within the scope of their operations with baseline
 * logic, though testing them fully is not the focus of this test class.
 *
 * @group core
 * @group baseline
 * @covers \oroboros\core\traits\core\BaselineInternalsTrait
 * @covers \oroboros\validate\traits\ValidatorTrait
 * @covers \oroboros\core\traits\utilities\exception\ExceptionUtilityTrait
 * @covers \oroboros\core\traits\OroborosTrait
 */
class BaselineInternalsTest
    extends \oroboros\tests\AbstractTestClass
{

    const TEST_CLASS = '\\oroboros\\testclass\\core\\baseline\\DefaultBaselineInternal';

    public $test_prop;
    private $_validation_subjects;

    /**
     * @group core
     * @group baseline
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     * @covers \oroboros\core\traits\utilities\exception\ExceptionUtilityTrait
     * @covers \oroboros\core\traits\OroborosTrait
     */
    public function testBaselineValidationMethodsProvided()
    {
        $test = $this->getTestClass();
        $this->assertTrue( true );
        $modes = $test->_validationModes();
        foreach ( $this->_getValidationSubjects() as
            $mode =>
            $props )
        {
            $this->assertTrue( in_array( $mode, $modes ),
                sprintf( 'Validation mode [%1$s] must exist in the given validation index BaselineInternals [_validationModes]',
                    $mode ) );
            $this->assertTrue( $test->_validate( $mode, $props['valid'],
                    $props['expected'] ),
                sprintf( 'Failed to assert that validation mode [%1$s] in [BaselineInternalsTrait] returned true for a valid property.',
                    $mode ) );
            $this->assertFalse( $test->_validate( $mode, $props['invalid'],
                    $props['expected'] ),
                sprintf( 'Failed to assert that validation mode [%1$s] in [BaselineInternalsTrait] returned false for an invalid property.',
                    $mode ) );
            try
            {
                //Test automatic invalid argument exception logic
                $test->_validate( $mode, $props['invalid'], $props['expected'],
                    true );
                $this->assertFalse( true,
                    sprintf( 'Failed to assert that validation mode [%1$s] in [BaselineInternalsTrait] threw an expected exception for an invalid property.',
                        $mode ) );
            } catch ( \oroboros\core\utilities\exception\InvalidArgumentException $e )
            {
                $this->assertTrue( true );
            }
        }
        $this->assertTrue( $test->_validate( 'number', 123.456, true ),
            sprintf( 'Failed to assert that validation mode [%1$s] in [BaselineInternalsTrait] returned true for a valid property.',
                $mode ) );
        $this->assertFalse( $test->_validate( 'property-exists',
                'invalid-property', 'invalid-class' ),
            sprintf( 'Failed to assert that validation mode [%1$s] in [BaselineInternalsTrait] returned false for a property of an invalid class/interface.',
                $mode ) );
        try
        {
            //Assert that a mode not in _validationModes()
            //throws an expected exception.
            $test->_validate( 'nonexistent-validation-mode', false, false );
            $this->assertFalse( true,
                sprintf( 'Failed to assert that invalid validation mode [%1$s] in [BaselineInternalsTrait] threw an expected exception for an invalid method passthrough.',
                    'nonexistent-validation-mode' ) );
        } catch ( \oroboros\core\utilities\exception\BadMethodCallException $e )
        {
            $this->assertTrue( true );
        }
    }

    private function _getValidationSubjects()
    {
        if ( is_null( $this->_validation_subjects ) )
        {
            $this->_validation_subjects = array(
                'regex' => array(
                    'expected' => \oroboros\regex\interfaces\enumerated\Regex::REGEX_ALPHANUMERIC,
                    'valid' => 'abcd123',
                    'invalid' => 'lol!' ),
                'any-of' => array(
                    'expected' => array(
                        'foo',
                        'bar' ),
                    'valid' => 'foo',
                    'invalid' => 'baz' ),
                'none-of' => array(
                    'expected' => array(
                        'foo',
                        'bar' ),
                    'valid' => 'baz',
                    'invalid' => 'foo' ),
                'not-type-of' => array(
                    'expected' => 'string',
                    'valid' => false,
                    'invalid' => 'foo' ),
                'type-of' => array(
                    'expected' => 'string',
                    'valid' => 'this is a string',
                    'invalid' => array(
                        0 => array(
                            0 => array(
                                0 => array(
                                    0 => array() ) ) ) ) ),
                'instance-of' => array(
                    'expected' => new \LogicException(),
                    'valid' => new \InvalidArgumentException(),
                    'invalid' => '\\stdClass' ),
                'child-of' => array(
                    'expected' => new \LogicException(),
                    'valid' => new \InvalidArgumentException(),
                    'invalid' => __CLASS__ ),
                'parent-of' => array(
                    'expected' => new \InvalidArgumentException(),
                    'valid' => new \LogicException(),
                    'invalid' => '\\stdClass' ),
                'class' => array(
                    'expected' => null,
                    'valid' => $this,
                    'invalid' => 'fail' ),
                'trait' => array(
                    'expected' => null,
                    'valid' => '\\oroboros\\core\\traits\\core\\BaselineInternalsTrait',
                    'invalid' => __CLASS__ ),
                'interface' => array(
                    'expected' => null,
                    'valid' => '\\oroboros\\core\\interfaces\\enumerated\\BaseEnum',
                    'invalid' => __CLASS__ ),
                'file' => array(
                    'expected' => null,
                    'valid' => __FILE__,
                    'invalid' => false ),
                'directory' => array(
                    'expected' => null,
                    'valid' => __DIR__,
                    'invalid' => 'foobar' ),
                'writable' => array(
                    'expected' => null,
                    'valid' => \oroboros\Oroboros::TEMP_DIRECTORY,
                    'invalid' => 'foobar' ),
                'executable' => array(
                    'expected' => null,
                    'valid' => OROBOROS_ROOT_DIRECTORY . 'oroboros',
                    'invalid' => 'nope' ),
                'equals' => array(
                    'expected' => true,
                    'valid' => 1,
                    'invalid' => false ),
                'exact' => array(
                    'expected' => true,
                    'valid' => true,
                    'invalid' => 1 ),
                'between' => array(
                    'expected' => array(
                        1,
                        10 ),
                    'valid' => 5,
                    'invalid' => 11 ),
                'property-exists' => array(
                    'expected' => $this,
                    'valid' => 'test_prop',
                    'invalid' => 'not_a_prop' ),
                'constant-exists' => array(
                    'expected' => $this,
                    'valid' => 'TEST_CLASS',
                    'invalid' => 'lol_no' ),
                'method-exists' => array(
                    'expected' => 'testBaselineValidationMethodsProvided',
                    'valid' => $this,
                    'invalid' => 'fail' ),
                'stringable' => array(
                    'expected' => null,
                    'valid' => 2,
                    'invalid' => array() ),
                'iterable' => array(
                    'expected' => null,
                    'valid' => array(),
                    'invalid' => 2 ),
                'scalar' => array(
                    'expected' => null,
                    'valid' => 'this is a string',
                    'invalid' => null ),
                'callable' => array(
                    'expected' => null,
                    'valid' => 'var_dump',
                    'invalid' => false ),
                'number' => array(
                    'expected' => false,
                    'valid' => 2,
                    'invalid' => 'not a number' ),
            );
        }
        return $this->_validation_subjects;
    }

}
