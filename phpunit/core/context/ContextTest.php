<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\core\context;

/**
 * <Context Test Cases>
 * These tests prove the stable functionality core context objects.
 * All other context object tests should extend from this class, so that
 * extended functionality can be proven to follow the Liskov substitution
 * principle.
 *
 * @group core
 * @group context
 * @covers \oroboros\core\traits\core\context\ContextTrait
 * @covers \oroboros\validate\traits\ValidatorTrait
 */
class ContextTest
    extends \oroboros\tests\AbstractTestClass
{

    use \oroboros\tests\traits\assertions\context\ContextualEvaluationTrait;
    use \oroboros\tests\traits\tests\context\ContextualTestTrait;

    const TEST_CLASS = '\\oroboros\\testclass\\core\\context\\DefaultTestContext';

    /**
     * Override this constant to declare a default contextual type
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_TYPE = 'unit-test';

    /**
     * Override this constant to declare a default contextual sub-type
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_SUBTYPE = null;

    /**
     * Override this constant to declare a default contextual context
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_CONTEXT = 'unit-test';

    /**
     * Override this constant to declare a default contextual value
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_VALUE = __CLASS__;

    /**
     * Override this constant to declare a default contextual category
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_CATEGORY = null;

    /**
     * Override this constant to declare a default contextual sub-category
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_SUBCATEGORY = null;

    /**
     * Override this constant to declare a default contextual package
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_PACKAGE = null;

    /**
     * Override this constant to declare a default contextual sub-package
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_SUBPACKAGE = null;

    /**
     * Override this constant to declare an expected contextual type
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_TYPE = null;

    /**
     * Override this constant to declare an expected contextual sub-type
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_SUBTYPE = null;

    /**
     * Override this constant to declare an expected contextual context
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_CONTEXT = 'unit-test';

    /**
     * Override this constant to declare an expected contextual value
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_VALUE = __CLASS__;

    /**
     * Override this constant to declare an expected contextual category
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_CATEGORY = null;

    /**
     * Override this constant to declare an expected contextual sub-category
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_SUBCATEGORY = null;

    /**
     * Override this constant to declare an expected contextual package
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_PACKAGE = null;

    /**
     * Override this constant to declare an expected contextual sub-package
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_SUBPACKAGE = null;

}
