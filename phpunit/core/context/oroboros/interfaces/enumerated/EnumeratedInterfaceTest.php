<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\core\context\oroboros\interfaces\enumerated;

/**
 * <Enumerated Interface Test Cases>
 * These tests prove the stable functionality enumerated interface test objects.
 * @group core
 * @group context
 * @group enum
 * @covers \oroboros\core\traits\core\context\oroboros\interfaces\enumerated\EnumeratedInterfaceContextTrait
 * @covers \oroboros\core\traits\core\context\CoreContextTrait
 * @covers \oroboros\core\traits\core\context\JsonSerialContextTrait
 * @covers \oroboros\core\traits\core\context\SerialContextTrait
 * @covers \oroboros\core\traits\core\context\ContextTrait
 * @covers \oroboros\validate\traits\ValidatorTrait
 */
class EnumeratedInterfaceTest
    extends \oroboros\tests\core\context\CoreContextTest
{

    const TEST_CLASS = '\\oroboros\\core\\internal\\context\\oroboros\\interfaces\\enumerated\\EnumeratedInterfaceContext';

    /**
     * The default contextual type for Oroboros enumerated-interface
     * objects is 'enumerated-interface'.
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_TYPE = 'enumerated-interface';

    /**
     * Use the default sub-type.
     * This will be provided dynamically as extensions occur.
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_SUBTYPE = null;

    /**
     * The default context is a string representation indicating a
     * canonicalized keyword name for the given enumerated interface subject.
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_CONTEXT = 'exception-codes';

    /**
     * The default parameter for enumerated-interfaces is the core exception code interface,
     * which should always validate as a valid enumerated interface.
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_VALUE = '\\oroboros\\core\\interfaces\\enumerated\\exception\\ExceptionCode';

    /**
     * The default contextual category for enumerated-interfaces should bear the
     * oroboros-context class type as its category.
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_CATEGORY = \oroboros\core\interfaces\enumerated\type\CoreClassTypes::CLASS_TYPE_CORE_CONTEXT_OROBOROS;

    /**
     * The default contextual sub-category for enumerated-interfaces is
     * the core class scope designated for enumerated-interfaces.
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_SUBCATEGORY = \oroboros\core\interfaces\enumerated\scope\CoreClassScopes::CLASS_SCOPE_CORE_CONTEXT_OROBOROS_INTERFACE_ENUMERATED;

    /**
     * The default package for enumerated-interfaces is the oroboros root namespace
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_PACKAGE = \oroboros\Oroboros::API_CODEX;

    /**
     * The default sub-package for enumerated-interfaces is the core package
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_SUBPACKAGE = \oroboros\Oroboros::API_SCOPE;

    /**
     * The expected contextual type for Oroboros enumerated-interface
     * objects is 'enumerated-interface'.
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_TYPE = 'enumerated-interface';

    /**
     * Use the default sub-type.
     * This will validate dynamically as extensions occur.
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_SUBTYPE = null;

    /**
     * The expected context is the default scalar assignment
     * corresponding to the default.
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_CONTEXT = 'exception-codes';

    /**
     * The expected contextual value by default is core exception code interface, which should
     * always be valid, and corresponds to the default parameter for contract
     * interface test validation.
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_VALUE = '\\oroboros\\core\\interfaces\\enumerated\\exception\\ExceptionCode';

    /**
     * Api Interface contexts should bear the oroboros context
     * class type as their category.
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_CATEGORY = \oroboros\core\interfaces\enumerated\type\CoreClassTypes::CLASS_TYPE_CORE_CONTEXT_OROBOROS;

    /**
     * The default enumerated-interface should bear the enumerated-interface subcategory.
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_SUBCATEGORY = \oroboros\core\interfaces\enumerated\scope\CoreClassScopes::CLASS_SCOPE_CORE_CONTEXT_OROBOROS_INTERFACE_ENUMERATED;

    /**
     * Api Interface contexts should be under the oroboros namespace
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_PACKAGE = \oroboros\Oroboros::API_CODEX;

    /**
     * Api Interface contexts should be part of the core package
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_SUBPACKAGE = \oroboros\Oroboros::API_SCOPE;

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group context
     * @group enum
     * @covers \oroboros\core\traits\core\context\oroboros\interfaces\enumerated\EnumeratedInterfaceContextTrait
     * @covers \oroboros\core\traits\core\context\CoreContextTrait
     * @covers \oroboros\core\traits\core\context\JsonSerialContextTrait
     * @covers \oroboros\core\traits\core\context\SerialContextTrait
     * @covers \oroboros\core\traits\core\context\ContextTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     */
    public function testValidEnumerate()
    {

        try
        {
            //We're going to manually instantiate this so it does not cause
            //an exit fault on failure, because the object MUST NOT resolve
            //if parameters are incorrect.
            $class = $this::TEST_CLASS;
            $test = new $class( $this->getExpectedContextualContext(),
                $this->getExpectedContextualValue() );
            $test_class = $this->getExpectedContextualValue();
            $this->assertEquals( $this->getExpectedContextualValue(),
                $test->getValue(),
                sprintf( 'Result of getValue() at [%1$s] in instance of [%2$s]'
                    . ' MUST equal the result of the enumerated interface of the testing'
                    . ' class instance. Expected [%3$s] but received [%4$s]',
                    __METHOD__, get_class( $test ), $test_class,
                    $test->getValue() ) );
        } catch ( \oroboros\core\utilities\exception\InvalidArgumentException $e )
        {
            $this->assertTrue( false,
                sprintf( 'Failed to resolve object creation in [%1$s] for '
                    . 'instance of [%2$s]. Valid enumerated interface [%3$s] could not '
                    . 'be used to create a valid enumerated interface of [%2$s]. '
                    . 'Error message: [%4$s]', __METHOD__, $this::TEST_CLASS,
                    $this->getExpectedContextualValue(), $e->getMessage() ) );
        }
    }

    /**
     * Tests context matching for invalid parameters.
     * @group core
     * @group context
     * @group enum
     * @covers \oroboros\core\traits\core\context\oroboros\interfaces\enumerated\EnumeratedInterfaceContextTrait
     * @covers \oroboros\core\traits\core\context\CoreContextTrait
     * @covers \oroboros\core\traits\core\context\JsonSerialContextTrait
     * @covers \oroboros\core\traits\core\context\SerialContextTrait
     * @covers \oroboros\core\traits\core\context\ContextTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     */
    public function testInvalidEnumerate()
    {
        try
        {
            $class = $this::TEST_CLASS;
            $valid = '\\oroboros\\core\\interfaces\\enumerated\\BaseEnum';
            //We know that only the actual enumerated interface should resolve,
            //so we will use an api interface instead.
            //It is a valid interface, but it is not a enumerated interface.
            $bad_value = '\\oroboros\\core\\interfaces\\api\\ApiBase';
            $test = new $class( $this->getExpectedContextualContext(),
                $bad_value );
            $this->assertTrue( false,
                sprintf( 'Failed to reject invalid enumerated interface in [%1$s] for '
                    . 'instance of [%2$s]. Only an interface that is an instance of [%3$s] should have '
                    . 'resolved for [%2$s], but object validation failed to '
                    . 'reject bad enumerated interface [%4$s]', __METHOD__,
                    get_class( $test ), $valid, $bad_value ) );
        } catch ( \oroboros\core\utilities\exception\InvalidArgumentException $e )
        {
            //this is what should happen here.
            $this->assertTrue( true );
        }
    }

}
