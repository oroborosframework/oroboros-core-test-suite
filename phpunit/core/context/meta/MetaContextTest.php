<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\core\context\meta;

/**
 * <Meta Context Test Cases>
 * These tests prove the stable functionality meta context test objects.
 * @group core
 * @group context
 * @covers \oroboros\core\traits\core\context\meta\MetaContextTrait
 * @covers \oroboros\core\traits\core\context\CoreContextTrait
 * @covers \oroboros\core\traits\core\context\JsonSerialContextTrait
 * @covers \oroboros\core\traits\core\context\SerialContextTrait
 * @covers \oroboros\core\traits\core\context\ContextTrait
 * @covers \oroboros\validate\traits\ValidatorTrait
 */
class MetaContextTest
    extends \oroboros\tests\core\context\CoreContextTest
{

    use \oroboros\core\traits\utilities\core\StringUtilityTrait;

    const TEST_CLASS = '\\oroboros\\core\\internal\\context\\meta\\MetaContext';

    /**
     * The default type for meta-contexts is 'meta-context'
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_TYPE = 'meta-context';

    /**
     * The default meta-context does not declare a sub-type.
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_SUBTYPE = null;

    /**
     * The default context for meta-contexts is the meta-type value from
     * the enumerated interface the base logic validates against.
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_CONTEXT = \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_CONTEXT_META;

    /**
     * The default parameter for meta-contexts is a string/scalar parameter
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_VALUE = 'context-meta';

    /**
     * The default contextual category for meta-contexts is
     * the core class type designated for meta-contexts.
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_CATEGORY = \oroboros\core\interfaces\enumerated\type\CoreClassTypes::CLASS_TYPE_CORE_CONTEXT_META;

    /**
     * The default contextual sub-category for meta-contexts is
     * the core class scope designated for abstract meta-contexts.
     * This must be overridden to test focused meta-context objects.
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_SUBCATEGORY = \oroboros\core\interfaces\enumerated\scope\CoreClassScopes::CLASS_SCOPE_CORE_CONTEXT_META;

    /**
     * The default package for meta-contexts is the oroboros root namespace
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_PACKAGE = \oroboros\Oroboros::API_CODEX;

    /**
     * The default sub-package for meta-contexts is the core package
     */
    const TEST_CLASS_DEFAULT_CONTEXTUAL_SUBPACKAGE = \oroboros\Oroboros::API_SCOPE;

    /**
     * Override this constant to declare an expected contextual type
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_TYPE = 'meta-context';

    /**
     * Default meta-context logic does not declare a subtype.
     * Child extensions of the default abstract declare a sub-type,
     * and should override this constant declaration.
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_SUBTYPE = null;

    /**
     * The default meta-type context should validate for the default meta-context
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_CONTEXT = null;

    /**
     * A default string value should validate for all meta-contexts
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_VALUE = 'context-meta';

    /**
     * Meta-contexts should bear the core meta-context
     * class type as their category.
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_CATEGORY = \oroboros\core\interfaces\enumerated\type\CoreClassTypes::CLASS_TYPE_CORE_CONTEXT_META;

    /**
     * The default meta-context should bear the meta-context-abstract subcategory.
     * Children of the base meta-context abstract must override this value.
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_SUBCATEGORY = \oroboros\core\interfaces\enumerated\scope\CoreClassScopes::CLASS_SCOPE_CORE_CONTEXT_META;

    /**
     * Meta-contexts should be under the oroboros namespace
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_PACKAGE = \oroboros\Oroboros::API_CODEX;

    /**
     * Meta-contexts should be part of the core package
     */
    const TEST_CLASS_EXPECTED_CONTEXTUAL_SUBPACKAGE = \oroboros\Oroboros::API_SCOPE;

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group context
     * @covers \oroboros\core\traits\core\context\meta\MetaContextTrait
     * @covers \oroboros\core\traits\core\context\CoreContextTrait
     * @covers \oroboros\core\traits\core\context\JsonSerialContextTrait
     * @covers \oroboros\core\traits\core\context\SerialContextTrait
     * @covers \oroboros\core\traits\core\context\ContextTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     */
    public function testMetaContexts()
    {
        $class = $this::TEST_CLASS;
        $enum_interface = '\\oroboros\\core\\interfaces\\enumerated\\context\\MetaContextTypes';
        $enum = new \oroboros\enum\InterfaceEnumerator( $enum_interface );
        foreach ( $enum->filterKey( 'CONTEXT_TYPE_' )->toArray() as
            $const =>
            $context )
        {
            $value = $this->_stringUtilityCanonicalize( $this->_stringUtilityExtract( $const,
                    'CONTEXT_TYPE_' ) );
            try
            {
                $getter_method = 'get' . ucfirst( $context );
                $check_method = 'has' . ucfirst( $context );
                $test = new $class( $context, $value );
                $this->assertTrue( method_exists( $test, $getter_method ),
                    sprintf( 'Valid meta context [%1$s] MUST correspond to a valid '
                        . 'public getter method [%2$s] in context object class '
                        . '[%3$s], but the method does not exist. Assertion '
                        . 'failed in test run at [%4$s]', $context,
                        $getter_method, $this::TEST_CLASS, __METHOD__ ) );
                $this->assertTrue( method_exists( $test, $check_method ),
                    sprintf( 'Valid meta context [%1$s] MUST correspond to a valid '
                        . 'public validation method [%2$s] in context object class '
                        . '[%3$s], but the method does not exist. Assertion '
                        . 'failed in test run at [%4$s]', $context,
                        $check_method, $this::TEST_CLASS, __METHOD__ ) );
            } catch ( \oroboros\core\utilities\exception\InvalidArgumentException $e )
            {
                $this->assertTrue( false,
                    sprintf( 'Failed to generate meta context in [%1$s] for '
                        . 'testing class [%2$s] with expected valid value [%3$s] '
                        . 'and expected valid context [%4$s]. Failure message: [%5$s]',
                        __METHOD__, $this::TEST_CLASS, $value, $context,
                        $e->getMessage() ) );
            }
            try
            {
                //This SHOULD generate an invalid argument exception.
                $bad_test = new $class( 'foo', 'bar' );
                $this->assertFalse( true,
                    sprintf( 'Meta contexts MUST NOT allow creation of invalid '
                        . 'meta contexts for instance of [%1$s] in test method '
                        . '[%2$s]. Only values existing in the meta enumerated '
                        . 'interface [%3$s] are acceptable contexts, which '
                        . 'correspond to the following values [%4$s]',
                        $this::TEST_CLASS, __METHOD__, $enum_interface,
                        implode( ', ',
                            $enum->filterKey( 'CONTEXT_TYPE_' )->toArray() ) ) );
            } catch ( \oroboros\core\utilities\exception\InvalidArgumentException $e )
            {
                //no-op
            }
        }
    }

}
