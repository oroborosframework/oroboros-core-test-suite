<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\core\context;

/**
 * <Context Set Container Test Cases>
 * These tests prove the stable functionality of core context set container objects.
 * @group core
 * @group context
 * @group container
 * @covers \oroboros\core\traits\core\context\ContextSetTrait
 * @covers \oroboros\core\traits\core\container\SetTrait
 * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
 * @covers \oroboros\core\traits\core\container\ContainerTrait
 */
class ContextSetTest
    extends \oroboros\tests\core\container\ConstrainedContainerTest
{

    const TEST_CLASS = '\\oroboros\\core\\internal\\context\\ContextSet';
    const TEST_CLASS_EXPECTED_CONTRACT = '\\oroboros\\core\\interfaces\\contract\\core\\context\\ContextSetContract';
    const TEST_CLASS_EXPECTED_INTERFACE = '\\Psr\\Container\\ContainerInterface';
    const TEST_CLASS_EXPECTED_API = '\\oroboros\\core\\interfaces\\api\\core\\CoreApi';

    protected $_test_values = array();
    protected static $_test_contexts = array();

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group context
     * @group container
     * @covers \oroboros\core\traits\core\context\ContextSetTrait
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainer()
    {
        $class = $this->getTestClass();
        $this->assertContractInterfaceValid( $class );
        $this->assertInterfaceValid( $class );
        $this->assertApiInterfaceValid( $class );
        $classname = get_class( $class );
        try
        {
            //Testing invalid parameter injection into constructor
            $fail = new $classname( new \stdClass() );
            $this->assertFalse( true,
                sprintf( 'Failed to throw expected ContainerExceptionInterface when '
                    . 'instantiated with invalid parameters getter get() '
                    . 'for instance of [%1$s] in '
                    . '[%2$s] of test class [%3$s]', $classname, __METHOD__,
                    get_class( $this ) ) );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Psr\Container\ContainerExceptionInterface $e )
        {
            //expected
            $this->assertTrue( true );
        }
        $this->checkContextSetFailConstraints();
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group context
     * @group container
     * @covers \oroboros\core\traits\core\context\ContextSetTrait
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerSerializable()
    {
        $class = $this->getTestClass();
        $this->assertSerializableValid( $class );
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group context
     * @group container
     * @covers \oroboros\core\traits\core\context\ContextSetTrait
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerJsonSerializable()
    {
        $class = $this->getTestClass();
        $this->assertJsonSerializableValid( $class );
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group context
     * @group container
     * @covers \oroboros\core\traits\core\context\ContextSetTrait
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerIterable()
    {
        $class = $this->getTestClass();
        $this->assertIterableValid( $class );
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group context
     * @group container
     * @covers \oroboros\core\traits\core\context\ContextSetTrait
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerCountable()
    {
        $class = $this->getTestClass();
        $this->assertCountableValid( $class );
    }

    /**
     * Tests context matching for resizing, truncation of excess,
     * and disallowing overflow setting.
     * @group core
     * @group context
     * @group container
     * @covers \oroboros\core\traits\core\context\ContextSetTrait
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerResize()
    {
        $class = $this->getTestClass();
        $this->assertResizeKeyIntegrity( $class );
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group context
     * @group container
     * @covers \oroboros\core\traits\core\context\ContextSetTrait
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerArrayAccess()
    {
        $class = $this->getTestClass();
        $this->assertArrayAccessValid( $class );
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group context
     * @group container
     * @covers \oroboros\core\traits\core\context\ContextSetTrait
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerPsr11()
    {
        $class = $this->getTestClass();
        $this->assertPsr11Valid( $class );
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group context
     * @group container
     * @covers \oroboros\core\traits\core\context\ContextSetTrait
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerSizeable()
    {
        $class = $this->getTestClass();
        $this->assertSizeableValid( $class );
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group context
     * @group container
     * @covers \oroboros\core\traits\core\context\ContextSetTrait
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerPairs()
    {
        $class = $this->getTestClass();
        $this->assertPairsValid( $class );
    }

    /**
     * Tests equivalent interaction between arrays and object representation.
     * @group core
     * @group context
     * @group container
     * @covers \oroboros\core\traits\core\context\ContextSetTrait
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testArrayObjectInterchangable()
    {
        $class = $this->getTestClass();
        $this->assertArrayObjectSubstitution( $class );
    }

    /**
     * Tests key and value constraints for containers.
     * @group core
     * @group context
     * @group container
     * @covers \oroboros\core\traits\core\context\ContextSetTrait
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerConstraints()
    {
        $class = $this->getTestClass();
        $this->assertConstraintsValid( $class );
    }

    /**
     * Tests integrity of hasType and fromType.
     * @group core
     * @group context
     * @group container
     * @covers \oroboros\core\traits\core\context\ContextSetTrait
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerTypeValid()
    {
        $class = $this->getTestClass();
        $this->assertContextSetTypeValid( $class );
    }

    /**
     * Tests integrity of hasContext and fromContext.
     * @group core
     * @group context
     * @group container
     * @covers \oroboros\core\traits\core\context\ContextSetTrait
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerContextValid()
    {
        $class = $this->getTestClass();
        $this->assertContextSetContextValid( $class );
    }

    /**
     * Tests integrity of hasValue and fromValue.
     * @group core
     * @group context
     * @group container
     * @covers \oroboros\core\traits\core\context\ContextSetTrait
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerValueValid()
    {
        $class = $this->getTestClass();
        $this->assertContextSetValueValid( $class );
    }

    /**
     * Tests integrity of hasCategory and fromCategory.
     * @group core
     * @group context
     * @group container
     * @covers \oroboros\core\traits\core\context\ContextSetTrait
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerCategoryValid()
    {
        $class = $this->getTestClass();
        $this->assertContextSetCategoryValid( $class );
    }

    /**
     * Tests integrity of hasSubcategory and fromSubcategory.
     * @group core
     * @group context
     * @group container
     * @covers \oroboros\core\traits\core\context\ContextSetTrait
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerSubcategoryValid()
    {
        $class = $this->getTestClass();
        $this->assertContextSetSubcategoryValid( $class );
    }

    /**
     * Performs assertions to insure that the given class correctly
     * implements hasType and fromType
     * @param \oroboros\core\interfaces\contract\core\context\ContextSet $class
     * @return void
     */
    protected function assertContextSetTypeValid( $class )
    {
        $classname = get_class( $class );
        //We will use infinite sizing to
        //rule out any expected size errors, which would
        //otherwise potentially cause false positives.
        $class->setSize( -1 );
        $array = $this->getTestAssociativeArray();
        $expected = array();
        foreach ( $array as
            $key =>
            $context )
        {
            $expected[$key] = $context->getType();
            $class[$context->getType()] = $context;
        }
        foreach ( $expected as
            $key =>
            $value )
        {
            $obj = $array[$key];
            if ( is_string( $obj->getType() ) )
            {
                $stringable = $this->getStringableObject( $obj->getType() );
                $this->assertTrue( $class->hasType( $stringable ) );
            }
            $this->assertTrue( $class->hasType( $obj->getType() ) );
            $this->assertTrue( $class->hasType( $obj ) );
            $this->assertInstanceOf( $this::TEST_CLASS_EXPECTED_CONTRACT,
                $class->fromType( $obj->getType() ) );
            $this->assertInstanceOf( $this::TEST_CLASS_EXPECTED_CONTRACT,
                $class->fromType( $obj ) );
        }
        try
        {
            //Class MUST raise a ContainerException on a non-scalar resolving argument.
            $class->hasType( new \stdClass() );
            $this->assertTrue( false,
                sprintf( 'Failed to throw expected exception [%1$s] for non-scalar argument via hasType '
                    . 'in instance of [%2$s] for invalid set key in [%3$s] of test class [%4$s]',
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected validation exception [%1$s] occurred on '
                    . 'an invalid set key as an instance of [%2$s] for hasType '
                    . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        }
        try
        {
            //Class MUST raise a ContainerException on a non-scalar resolving argument.
            $class->fromType( new \stdClass() );
            $this->assertTrue( false,
                sprintf( 'Failed to throw expected exception [%1$s] for non-scalar argument via fromType '
                    . 'in instance of [%2$s] for invalid set key in [%3$s] of test class [%4$s]',
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected validation exception [%1$s] occurred on '
                    . 'an invalid set key as an instance of [%2$s] for fromType '
                    . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        }
        try
        {
            //Class MUST raise a NotFoundException on a missing key argument.
            $class->fromType( $this->getStringableObject( sha1( microtime() . get_class( $this ) ) ) );
            $this->assertTrue( false,
                sprintf( 'Failed to throw expected exception [%1$s] for nonexistent argument via fromType '
                    . 'in instance of [%2$s] for invalid set key in [%3$s] of test class [%4$s]',
                    '\\Psr\\Container\\NotFoundExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\NotFoundExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected validation exception [%1$s] occurred on '
                    . 'an invalid set key as an instance of [%2$s] for fromType '
                    . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\NotFoundExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        }
    }

    /**
     * Performs assertions to insure that the given class correctly
     * implements hasContext and fromContext
     * @param \oroboros\core\interfaces\contract\core\context\ContextSet $class
     * @return void
     */
    protected function assertContextSetContextValid( $class )
    {
        $classname = get_class( $class );
        //We will use infinite sizing to
        //rule out any expected size errors, which would
        //otherwise potentially cause false positives.
        $class->setSize( -1 );
        $array = $this->getTestAssociativeArray();
        $expected = array();
        foreach ( $array as
            $key =>
            $context )
        {
            $expected[$key] = $context->getContext();
            $class[$context->getType()] = $context;
        }
        foreach ( $expected as
            $key =>
            $value )
        {
            $obj = $array[$key];
            if ( is_string( $obj->getContext() ) )
            {
                $stringable = $this->getStringableObject( $obj->getContext() );
                $this->assertTrue( $class->hasContext( $stringable ) );
            }
            $stringable = $this->getStringableObject( $value );
            $this->assertTrue( $class->hasContext( $obj->getContext() ) );
            $this->assertTrue( $class->hasContext( $obj ) );
            $this->assertInstanceOf( $this::TEST_CLASS_EXPECTED_CONTRACT,
                $class->fromContext( $obj->getContext() ) );
            $this->assertInstanceOf( $this::TEST_CLASS_EXPECTED_CONTRACT,
                $class->fromContext( $obj ) );
        }
        try
        {
            //Class MUST raise a ContainerException on a non-scalar resolving argument.
            $class->hasContext( new \stdClass() );
            $this->assertTrue( false,
                sprintf( 'Failed to throw expected exception [%1$s] for non-scalar argument via hasContext '
                    . 'in instance of [%2$s] for invalid set key in [%3$s] of test class [%4$s]',
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected validation exception [%1$s] occurred on '
                    . 'an invalid set key as an instance of [%2$s] for hasContext '
                    . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        }
        try
        {
            //Class MUST raise a ContainerException on a non-scalar resolving argument.
            $class->fromContext( new \stdClass() );
            $this->assertTrue( false,
                sprintf( 'Failed to throw expected exception [%1$s] for non-scalar argument via fromContext '
                    . 'in instance of [%2$s] for invalid set key in [%3$s] of test class [%4$s]',
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected validation exception [%1$s] occurred on '
                    . 'an invalid set key as an instance of [%2$s] for fromContext '
                    . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        }
        try
        {
            //Class MUST raise a NotFoundException on a missing key argument.
            $class->fromContext( $this->getStringableObject( sha1( microtime() . get_class( $this ) ) ) );
            $this->assertTrue( false,
                sprintf( 'Failed to throw expected exception [%1$s] for nonexistent argument via fromContext '
                    . 'in instance of [%2$s] for invalid set key in [%3$s] of test class [%4$s]',
                    '\\Psr\\Container\\NotFoundExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\NotFoundExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected validation exception [%1$s] occurred on '
                    . 'an invalid set key as an instance of [%2$s] for fromContext '
                    . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\NotFoundExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        }
    }

    /**
     * Performs assertions to insure that the given class correctly
     * implements hasValue and fromValue
     * @param \oroboros\core\interfaces\contract\core\context\ContextSet $class
     * @return void
     */
    protected function assertContextSetValueValid( $class )
    {
        $classname = get_class( $class );
        //We will use infinite sizing to
        //rule out any expected size errors, which would
        //otherwise potentially cause false positives.
        $class->setSize( -1 );
        $array = $this->getTestAssociativeArray();
        $expected = array();
        foreach ( $array as
            $key =>
            $context )
        {
            $expected[$key] = $context->getValue();
            $class[$context->getType()] = $context;
        }
        foreach ( $expected as
            $key =>
            $value )
        {
            $obj = $array[$key];
//            $stringable = $this->getStringableObject($value);
            $this->assertTrue( $class->hasValue( $obj->getValue() ) );
            $this->assertTrue( $class->hasValue( $obj ) );
            $this->assertInstanceOf( $this::TEST_CLASS_EXPECTED_CONTRACT,
                $class->fromValue( $obj->getValue() ) );
            $this->assertInstanceOf( $this::TEST_CLASS_EXPECTED_CONTRACT,
                $class->fromValue( $obj ) );
        }
        try
        {
            //Class MUST raise a NotFoundException on a missing key argument.
            $class->fromValue( $this->getStringableObject( sha1( microtime() . get_class( $this ) ) ) );
            $this->assertTrue( false,
                sprintf( 'Failed to throw expected exception [%1$s] for nonexistent argument via fromValue '
                    . 'in instance of [%2$s] for invalid set key in [%3$s] of test class [%4$s]',
                    '\\Psr\\Container\\NotFoundExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\NotFoundExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected validation exception [%1$s] occurred on '
                    . 'an invalid set key as an instance of [%2$s] for fromValue '
                    . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\NotFoundExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        }
    }

    /**
     * Performs assertions to insure that the given class correctly
     * implements hasCategory and fromCategory
     * @param \oroboros\core\interfaces\contract\core\context\ContextSet $class
     * @return void
     */
    protected function assertContextSetCategoryValid( $class )
    {
        $classname = get_class( $class );
        //We will use infinite sizing to
        //rule out any expected size errors, which would
        //otherwise potentially cause false positives.
        $class->setSize( -1 );
        $array = $this->getTestAssociativeArray();
        $expected = array();
        foreach ( $array as
            $key =>
            $context )
        {
            $expected[$key] = $context->getCategory();
            $class[$context->getType()] = $context;
        }
        foreach ( $expected as
            $key =>
            $value )
        {
            $obj = $array[$key];
            if ( is_string( $obj->getCategory() ) )
            {
                $stringable = $this->getStringableObject( $obj->getCategory() );
                $this->assertTrue( $class->hasCategory( $stringable ) );
            }
            $this->assertTrue( $class->hasCategory( $obj->getCategory() ) );
            $this->assertTrue( $class->hasCategory( $obj ) );
            $this->assertInstanceOf( $this::TEST_CLASS_EXPECTED_CONTRACT,
                $class->fromCategory( $obj->getCategory() ) );
            $this->assertInstanceOf( $this::TEST_CLASS_EXPECTED_CONTRACT,
                $class->fromCategory( $obj ) );
        }
        try
        {
            //Class MUST raise a ContainerException on a non-scalar resolving argument.
            $class->hasCategory( new \stdClass() );
            $this->assertTrue( false,
                sprintf( 'Failed to throw expected exception [%1$s] for non-scalar argument via hasCategory '
                    . 'in instance of [%2$s] for invalid set key in [%3$s] of test class [%4$s]',
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected validation exception [%1$s] occurred on '
                    . 'an invalid set key as an instance of [%2$s] for hasCategory '
                    . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        }
        try
        {
            //Class MUST raise a ContainerException on a non-scalar resolving argument.
            $class->fromCategory( new \stdClass() );
            $this->assertTrue( false,
                sprintf( 'Failed to throw expected exception [%1$s] for non-scalar argument via fromCategory '
                    . 'in instance of [%2$s] for invalid set key in [%3$s] of test class [%4$s]',
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected validation exception [%1$s] occurred on '
                    . 'an invalid set key as an instance of [%2$s] for fromCategory '
                    . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        }
        try
        {
            //Class MUST raise a NotFoundException on a missing key argument.
            $class->fromCategory( $this->getStringableObject( sha1( microtime() . get_class( $this ) ) ) );
            $this->assertTrue( false,
                sprintf( 'Failed to throw expected exception [%1$s] for nonexistent argument via fromCategory '
                    . 'in instance of [%2$s] for invalid set key in [%3$s] of test class [%4$s]',
                    '\\Psr\\Container\\NotFoundExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\NotFoundExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected validation exception [%1$s] occurred on '
                    . 'an invalid set key as an instance of [%2$s] for fromCategory '
                    . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\NotFoundExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        }
    }

    /**
     * Performs assertions to insure that the given class correctly
     * implements hasSubcategory and fromSubcategory
     * @param \oroboros\core\interfaces\contract\core\context\ContextSet $class
     * @return void
     */
    protected function assertContextSetSubcategoryValid( $class )
    {
        $classname = get_class( $class );
        //We will use infinite sizing to
        //rule out any expected size errors, which would
        //otherwise potentially cause false positives.
        $class->setSize( -1 );
        $array = $this->getTestAssociativeArray();
        $expected = array();
        foreach ( $array as
            $key =>
            $context )
        {
            $obj = $array[$key];
            $expected[$key] = $context->getSubcategory();
            $class[$context->getType()] = $context;
        }
        foreach ( $expected as
            $key =>
            $value )
        {
            $obj = $array[$key];
            if ( is_string( $obj->getSubcategory() ) )
            {
                $stringable = $this->getStringableObject( $obj->getSubcategory() );
                $this->assertTrue( $class->hasSubcategory( $stringable ) );
            }
            $this->assertTrue( $class->hasSubcategory( $obj->getSubcategory() ) );
            $this->assertTrue( $class->hasSubcategory( $obj ) );
            $this->assertInstanceOf( $this::TEST_CLASS_EXPECTED_CONTRACT,
                $class->fromSubcategory( $obj->getSubcategory() ) );
            $this->assertInstanceOf( $this::TEST_CLASS_EXPECTED_CONTRACT,
                $class->fromSubcategory( $obj ) );
        }
        try
        {
            //Class MUST raise a ContainerException on a non-scalar resolving argument.
            $class->hasType( new \stdClass() );
            $this->assertTrue( false,
                sprintf( 'Failed to throw expected exception [%1$s] for non-scalar argument via hasSubcategory '
                    . 'in instance of [%2$s] for invalid set key in [%3$s] of test class [%4$s]',
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected validation exception [%1$s] occurred on '
                    . 'an invalid set key as an instance of [%2$s] for hasSubcategory '
                    . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        }
        try
        {
            //Class MUST raise a ContainerException on a non-scalar resolving argument.
            $class->fromSubcategory( new \stdClass() );
            $this->assertTrue( false,
                sprintf( 'Failed to throw expected exception [%1$s] for non-scalar argument via fromSubcategory '
                    . 'in instance of [%2$s] for invalid set key in [%3$s] of test class [%4$s]',
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected validation exception [%1$s] occurred on '
                    . 'an invalid set key as an instance of [%2$s] for fromSubcategory '
                    . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        }
        try
        {
            //Class MUST raise a NotFoundException on a missing key argument.
            $class->fromSubcategory( $this->getStringableObject( sha1( microtime() . get_class( $this ) ) ) );
            $this->assertTrue( false,
                sprintf( 'Failed to throw expected exception [%1$s] for nonexistent argument via fromSubcategory '
                    . 'in instance of [%2$s] for invalid set key in [%3$s] of test class [%4$s]',
                    '\\Psr\\Container\\NotFoundExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\NotFoundExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected validation exception [%1$s] occurred on '
                    . 'an invalid set key as an instance of [%2$s] for fromSubcategory '
                    . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\NotFoundExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        }
    }

    /**
     * Returns a test array for checking fromArray
     * @return array
     */
    protected function getTestAssociativeArray()
    {
        $array = $this::$_test_contexts;
        ksort( $array );
        return $array;
    }

    /**
     * Performs assertions to insure that negative constraint checks
     * fire the correct exceptions.
     * @return void
     */
    private function checkContextSetFailConstraints()
    {
        if ( !get_class( $this ) === __CLASS__ )
        {
            //Only run this test one time
            //from the root container test instance.
            return;
        }
        $class = $this->getTestClass();
        $classname = get_class( $class );
        $test_array = $this->getTestAssociativeArray();
        foreach ( $test_array as
            $key =>
            $value )
        {
            try
            {
                //Class MUST raise a ContainerException on a duplicate key.
                $class->offsetSet( $key, $value );
                $class->offsetSet( $key, $value );
                $this->assertTrue( false,
                    sprintf( 'Failed to throw expected exception [%1$s] for duplicate key detection via offsetSet '
                        . 'in instance of [%2$s] for invalid set key in [%3$s] of test class [%4$s]',
                        '\\Psr\\Container\\ContainerExceptionInterface',
                        $classname, __METHOD__, get_class( $this ) )
                );
            } catch ( \PHPUnit\Exception $e )
            {
                //Do now suppress testing framework exceptions
                throw $e;
            } catch ( \Exception $e )
            {
                $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                    $e,
                    sprintf( 'Failed to assert expected validation exception [%1$s] occurred on '
                        . 'an invalid set key as an instance of [%2$s] for offsetSet '
                        . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                        get_class( $e ),
                        '\\Psr\\Container\\ContainerExceptionInterface',
                        $classname, __METHOD__, get_class( $this ) )
                );
            }
        }
        try
        {
            //Class setter MUST raise a ContainerException on a non-scalar resolving argument.
            $class->offsetSet( 'bad-parameter', new \stdClass() );
            $this->assertTrue( false,
                sprintf( 'Failed to throw expected exception [%1$s] for non-scalar argument via offsetSet '
                    . 'in instance of [%2$s] for invalid set key in [%3$s] of test class [%4$s]',
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                $e,
                sprintf( 'Failed to assert expected validation exception [%1$s] occurred on '
                    . 'an invalid set key as an instance of [%2$s] for offsetSet '
                    . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                    get_class( $e ),
                    '\\Psr\\Container\\ContainerExceptionInterface', $classname,
                    __METHOD__, get_class( $this ) )
            );
        }
        foreach (
        array(
            'hasType',
            'hasContext',
            'hasCategory',
            'hasSubcategory',
            'fromType',
            'fromContext',
            'fromCategory',
            'fromSubcategory'
        ) as
            $method )
        {
            try
            {
                //Class MUST raise a ContainerException on a non-scalar resolving argument.
                $class->$method( new \stdClass() );
                $this->assertTrue( false,
                    sprintf( 'Failed to throw expected exception [%1$s] for non-scalar argument via ' . $method
                        . ' in instance of [%2$s] for invalid set key in [%3$s] of test class [%4$s]',
                        '\\Psr\\Container\\ContainerExceptionInterface',
                        $classname, __METHOD__, get_class( $this ) )
                );
            } catch ( \PHPUnit\Exception $e )
            {
                //Do now suppress testing framework exceptions
                throw $e;
            } catch ( \Exception $e )
            {
                $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                    $e,
                    sprintf( 'Failed to assert expected validation exception [%1$s] occurred on '
                        . 'an invalid set key as an instance of [%2$s] for ' . $method
                        . ' in instance of [%3$s] in [%4$s] of test class [%5$s]',
                        get_class( $e ),
                        '\\Psr\\Container\\ContainerExceptionInterface',
                        $classname, __METHOD__, get_class( $this ) )
                );
            }
        }

        //The following test class will perform the internal exception assertions
        //and fire the redundancies to catch misconfigured class issues.
        $invalid = new \oroboros\testclass\core\context\ContextSetTestRefuseValues();
        $classname = get_class( $invalid );
        foreach ( array(
        'fromType',
        'fromContext',
        'fromValue',
        'fromCategory',
        'fromSubcategory' ) as
            $method )
        {
            try
            {
                //Class MUST raise a ContainerException on a non-resolving value argument.
                $invalid->$method( 'fail-me' );
                $this->assertType( false,
                    sprintf( 'Failed to throw expected exception [%1$s] for non-scalar argument via ' . $method . ' '
                        . 'in instance of [%2$s] for invalid set key in [%3$s] of test class [%4$s]',
                        '\\Psr\\Container\\ContainerExceptionInterface',
                        $classname, __METHOD__, get_class( $this ) )
                );
            } catch ( \PHPUnit\Exception $e )
            {
                //Do now suppress testing framework exceptions
                throw $e;
            } catch ( \Exception $e )
            {
                $this->assertInstanceOf( '\\Psr\\Container\\ContainerExceptionInterface',
                    $e,
                    sprintf( 'Failed to assert internal redundancy exception handled exception [%1$s] occurred on '
                        . 'an invalid set key as an instance of [%2$s] for ' . $method . ' '
                        . 'in instance of [%3$s] in [%4$s] of test class [%5$s]',
                        get_class( $e ),
                        '\\Psr\\Container\\ContainerExceptionInterface',
                        $classname, __METHOD__, get_class( $this ) )
                );
            }
        }
    }

    /**
     * Initialize the mock core object, and run its initialization
     * @beforeClass
     */
    public static function bootstrapTestContexts()
    {
        $test_meta_context = new \oroboros\core\internal\context\meta\MetaContext( 'value',
            'foobar' );
        $test_class_context = new \oroboros\core\internal\context\php\classes\ClassContext( \oroboros\Oroboros::OROBOROS_CLASS_CONTEXT,
            '\\oroboros\\Oroboros' );
        $test_class_type = new \oroboros\core\internal\context\oroboros\type\ClassTypeContext( \oroboros\Oroboros::OROBOROS_CLASS_TYPE,
            'oroboros\\Oroboros' );
        $test_class_scope = new \oroboros\core\internal\context\oroboros\scope\ClassScopeContext( \oroboros\Oroboros::OROBOROS_CLASS_SCOPE,
            '\\oroboros\\Oroboros' );
        $test_api = new \oroboros\core\internal\context\oroboros\interfaces\api\ApiInterfaceContext( 'core-api',
            '\\oroboros\\core\\interfaces\\api\\core\\CoreApi' );
        $test_contract = new \oroboros\core\internal\context\oroboros\interfaces\contract\ContractInterfaceContext( 'core-contract',
            '\\oroboros\\core\\interfaces\\contract\\core\\CoreContract' );
        $test_enum = new \oroboros\core\internal\context\oroboros\interfaces\enumerated\EnumeratedInterfaceContext( 'core-contract',
            '\\oroboros\\core\\interfaces\\enumerated\\exception\\ExceptionCode' );
        self::$_test_contexts[$test_meta_context->getType()] = $test_meta_context;
        self::$_test_contexts[$test_class_context->getType()] = $test_class_context;
        self::$_test_contexts[$test_class_scope->getType()] = $test_class_scope;
        self::$_test_contexts[$test_class_type->getType()] = $test_class_type;
        self::$_test_contexts[$test_api->getType()] = $test_api;
        self::$_test_contexts[$test_contract->getType()] = $test_contract;
        self::$_test_contexts[$test_enum->getType()] = $test_enum;
    }

    /**
     * Reset the mock object to its default state.
     * @afterClass
     */
    public static function tearDownTestContexts()
    {
        self::$_test_contexts = array();
    }

}
