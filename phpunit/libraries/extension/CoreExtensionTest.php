<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\libraries\extension;

/**
 * <Core Extension Test Cases>
 * These tests prove the stable compliance of the validation extension.
 * @group extensions
 * @covers \oroboros\core\traits\extensions\ExtensionTrait
 * @covers \oroboros\core\traits\patterns\structural\StaticControlApiTrait
 * @covers \oroboros\core\traits\patterns\structural\StaticControlApiExtensionTrait
 * @covers \oroboros\core\traits\core\StaticBaselineTrait
 * @covers \oroboros\core\traits\core\BaselineInternalsTrait
 * @covers \oroboros\collection\traits\CollectionTrait
 * @covers \oroboros\collection\traits\ContainerTrait
 * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
 *
 */
class CoreExtensionTest
    extends ExtensionTest
{

    const TEST_EXTENDABLE_CLASS = '\\oroboros\\testclass\\core\\MockOroboros';
    const TEST_CLASS = '\\oroboros\\testclass\\core\\extensions\\ValidCoreExtension';
    const TEST_CLASS_EXPECTED_CONTRACT = '\\oroboros\\core\\interfaces\\contract\\extensions\\core\\CoreExtensionContract';
    const TEST_RUN_BASELINE_EXTENDABLE = false;

    protected $expected_extension_context = 'core';
    protected $expected_extension_id = 'test-foo';
    protected $expected_extension_indexes = array(
        'foo'
    );
    protected $expected_extension_api = array(
        'foo' => array(
            'foo',
            'bar',
            'baz'
        )
    );

    protected static $_mock_core;

    /**
     * Returns an instance of the valid extendable
     * class for testing the extension.
     * @return \oroboros\testclass\core\extensions\ValidExtendable
     */
    protected function getExtendableClass()
    {
        return self::$_mock_core;
    }

    /**
     * Initialize the mock core object, and run its initialization
     * @beforeClass
     */
    public static function bootstrapMockCoreObject()
    {
        $mock = new \oroboros\testclass\core\MockOroboros();
        $mock::initialize();
        self::$_mock_core = $mock;
    }

    /**
     * Reset the mock object to its default state.
     * @afterClass
     */
    public static function tearDownMockCoreObject()
    {
        $mock = self::$_mock_core;
        $mock::reset();
        self::$_mock_core = null;
    }

}
