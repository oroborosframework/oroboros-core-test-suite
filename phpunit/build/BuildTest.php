<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\build;

use PHPUnit\Framework\TestCase;

/**
 * @group initialization
 * @group build
 * @group core
 * @covers \oroboros\Oroboros
 * @covers \oroboros\core\traits\OroborosTrait
 * @covers \oroboros\log\traits\LogExtensionTrait
 * @covers \oroboros\core\traits\patterns\behavioral\ManagerTrait
 * @covers \oroboros\core\traits\patterns\behavioral\DirectorTrait
 * @covers \oroboros\core\traits\patterns\behavioral\WorkerTrait
 * @covers \oroboros\log\traits\LogManagerTrait
 * @covers \oroboros\log\traits\LoggerTrait
 * @covers \oroboros\log\traits\LogWriterTrait
 * @covers \oroboros\log\traits\NullLogWriterTrait
 * @covers \oroboros\log\traits\NullLoggerTrait
 * @covers \oroboros\log\traits\FileLoggerTrait
 * @covers \oroboros\validate\traits\ValidatorTrait
 * @covers \oroboros\format\traits\BacktraceTrait
 * @covers \oroboros\format\traits\StringTrait
 * @covers \oroboros\core\traits\utilities\core\StringUtilityTrait
 * @covers \oroboros\core\traits\utilities\core\BacktraceUtilityTrait
 * @covers \oroboros\core\traits\core\BaselineTrait
 * @covers \oroboros\core\traits\core\BaselineInternalsTrait
 * @covers \oroboros\core\traits\core\StaticBaselineTrait
 * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
 */
class BuildTest
    extends TestCase
{

    /**
     * @group initialization
     * @group build
     * @group core
     * @covers \oroboros\Oroboros
     * @covers \oroboros\core\traits\OroborosTrait
     * @covers \oroboros\log\traits\LogExtensionTrait
     * @covers \oroboros\core\traits\patterns\behavioral\ManagerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\DirectorTrait
     * @covers \oroboros\core\traits\patterns\behavioral\WorkerTrait
     * @covers \oroboros\log\traits\LogManagerTrait
     * @covers \oroboros\log\traits\LoggerTrait
     * @covers \oroboros\log\traits\LogWriterTrait
     * @covers \oroboros\log\traits\NullLogWriterTrait
     * @covers \oroboros\log\traits\NullLoggerTrait
     * @covers \oroboros\log\traits\FileLoggerTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     * @covers \oroboros\format\traits\BacktraceTrait
     * @covers \oroboros\format\traits\StringTrait
     * @covers \oroboros\core\traits\utilities\core\StringUtilityTrait
     * @covers \oroboros\core\traits\utilities\core\BacktraceUtilityTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testCleanInitialization()
    {
        try
        {
            //This should not be a valid initialization
            \oroboros\Oroboros::initialize( array(),
                array(
                'env' => 'unit-tests' ),
                array(
                'foo',
                'bar',
                'baz' ) );
        } catch ( \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract $e )
        {
            $this->assertInstanceOf( '\\oroboros\\core\\utilities\\exception\\RuntimeException',
                $e );
        }
        //This should be a valid initialization
        \oroboros\Oroboros::initialize();
        $this->assertTrue( \oroboros\Oroboros::isInitialized() );
        $this->assertTrue( is_array( \oroboros\Oroboros::getInitializationHistory() ) );
    }

    /**
     * @group psr3
     * @group core
     * @covers \oroboros\Oroboros
     * @covers \oroboros\core\traits\OroborosTrait
     * @covers \oroboros\log\traits\LogExtensionTrait
     * @covers \oroboros\core\traits\patterns\behavioral\ManagerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\DirectorTrait
     * @covers \oroboros\core\traits\patterns\behavioral\WorkerTrait
     * @covers \oroboros\log\traits\LogManagerTrait
     * @covers \oroboros\log\traits\LoggerTrait
     * @covers \oroboros\log\traits\LogWriterTrait
     * @covers \oroboros\log\traits\NullLogWriterTrait
     * @covers \oroboros\log\traits\NullLoggerTrait
     * @covers \oroboros\log\traits\FileLoggerTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     * @covers \oroboros\format\traits\BacktraceTrait
     * @covers \oroboros\format\traits\StringTrait
     * @covers \oroboros\core\traits\utilities\core\StringUtilityTrait
     * @covers \oroboros\core\traits\utilities\core\BacktraceUtilityTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testLog()
    {
        $this->_runLoggerEntries();
    }

    /**
     * @group psr3
     * @group core
     * @covers \oroboros\Oroboros
     * @covers \oroboros\core\traits\OroborosTrait
     * @covers \oroboros\log\traits\LogExtensionTrait
     * @covers \oroboros\core\traits\patterns\behavioral\ManagerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\DirectorTrait
     * @covers \oroboros\core\traits\patterns\behavioral\WorkerTrait
     * @covers \oroboros\log\traits\LogManagerTrait
     * @covers \oroboros\log\traits\LoggerTrait
     * @covers \oroboros\log\traits\LogWriterTrait
     * @covers \oroboros\log\traits\NullLogWriterTrait
     * @covers \oroboros\log\traits\NullLoggerTrait
     * @covers \oroboros\log\traits\FileLoggerTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     * @covers \oroboros\format\traits\BacktraceTrait
     * @covers \oroboros\format\traits\StringTrait
     * @covers \oroboros\core\traits\utilities\core\StringUtilityTrait
     * @covers \oroboros\core\traits\utilities\core\BacktraceUtilityTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testFileLog()
    {
        $file_writer = new \oroboros\log\FileLogWriter( array(
            'use_header' => false,
            'use_buffer' => true,
            'header_timestamp' => true,
            'timestamp_format' => 'Y-m-d H:i:s',
            'header_title' => 'Oroboros Unit Test Log',
            'default_error_logfile' => 'oroboros_unit_tests.log'
            ) );
        $file_logger = new \oroboros\log\Logger( $file_writer );
        \oroboros\Oroboros::log( 'set', $file_logger );
        $this->_runLoggerEntries();
    }

    /**
     * @group psr3
     * @group core
     * @covers \oroboros\Oroboros
     * @covers \oroboros\core\traits\OroborosTrait
     * @covers \oroboros\log\traits\LogExtensionTrait
     * @covers \oroboros\core\traits\patterns\behavioral\ManagerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\DirectorTrait
     * @covers \oroboros\core\traits\patterns\behavioral\WorkerTrait
     * @covers \oroboros\log\traits\LogManagerTrait
     * @covers \oroboros\log\traits\LoggerTrait
     * @covers \oroboros\log\traits\LogWriterTrait
     * @covers \oroboros\log\traits\NullLogWriterTrait
     * @covers \oroboros\log\traits\NullLoggerTrait
     * @covers \oroboros\log\traits\FileLoggerTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     * @covers \oroboros\format\traits\BacktraceTrait
     * @covers \oroboros\format\traits\StringTrait
     * @covers \oroboros\core\traits\utilities\core\StringUtilityTrait
     * @covers \oroboros\core\traits\utilities\core\BacktraceUtilityTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testNullLog()
    {
        $file_writer = new \oroboros\log\FileLogWriter( array(
            'use_header' => false,
            'use_buffer' => true,
            'header_timestamp' => true,
            'timestamp_format' => 'Y-m-d H:i:s',
            'header_title' => 'Oroboros Unit Test Log',
            'default_error_logfile' => 'oroboros_unit_tests.log'
            ) );
        $file_logger = new \oroboros\log\NullLogger( $file_writer );
        \oroboros\Oroboros::log( 'set', $file_logger );
        $this->_runLoggerEntries();
    }

    /**
     * @group psr3
     * @group core
     * @covers \oroboros\Oroboros
     * @covers \oroboros\core\traits\OroborosTrait
     * @covers \oroboros\log\traits\LogExtensionTrait
     * @covers \oroboros\core\traits\patterns\behavioral\ManagerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\DirectorTrait
     * @covers \oroboros\core\traits\patterns\behavioral\WorkerTrait
     * @covers \oroboros\log\traits\LogManagerTrait
     * @covers \oroboros\log\traits\LoggerTrait
     * @covers \oroboros\log\traits\LogWriterTrait
     * @covers \oroboros\log\traits\NullLogWriterTrait
     * @covers \oroboros\log\traits\NullLoggerTrait
     * @covers \oroboros\log\traits\FileLoggerTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     * @covers \oroboros\format\traits\BacktraceTrait
     * @covers \oroboros\format\traits\StringTrait
     * @covers \oroboros\core\traits\utilities\core\StringUtilityTrait
     * @covers \oroboros\core\traits\utilities\core\BacktraceUtilityTrait
     * @covers \oroboros\core\traits\core\BaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\utilities\logic\BackReferenceTrait
     */
    public function testNullLogWriter()
    {
        $file_writer = new \oroboros\log\NullLogWriter( array(
            'use_header' => false,
            'use_buffer' => true,
            'header_timestamp' => true,
            'timestamp_format' => 'Y-m-d H:i:s',
            'header_title' => 'Oroboros Unit Test Log',
            'default_error_logfile' => 'oroboros_unit_tests.log'
            ) );
        $file_logger = new \oroboros\log\Logger( $file_writer );
        \oroboros\Oroboros::log( 'set', $file_logger );
        $this->_runLoggerEntries();
    }

    private function _runLoggerEntries()
    {
        $this->assertNull( \oroboros\Oroboros::log( 'debug', 'testing debug' ) );
        $this->assertNull( \oroboros\Oroboros::log( 'info', 'testing info' ) );
        $this->assertNull( \oroboros\Oroboros::log( 'notice', 'testing notice' ) );
        $this->assertNull( \oroboros\Oroboros::log( 'warning', 'testing warning' ) );
        $this->assertNull( \oroboros\Oroboros::log( 'error', 'testing error' ) );
        $this->assertNull( \oroboros\Oroboros::log( 'critical',
                'testing critical' ) );
        $this->assertNull( \oroboros\Oroboros::log( 'alert', 'testing alert' ) );
        $this->assertNull( \oroboros\Oroboros::log( 'emergency',
                'testing emergency' ) );
    }

}
