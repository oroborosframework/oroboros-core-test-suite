<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\build;

use PHPUnit\Framework\TestCase;

/**
 * @group initialization
 * @group core
 */
class InitTest
    extends TestCase
{

    /**
     * @group initialization
     * @group core
     * @covers oroboros\core\utilities\core\CoreConfig
     * @covers oroboros\adapter\AdapterExtension
     * @covers oroboros\archive\ArchiveExtension
     * @covers oroboros\auth\AuthExtension
     * @covers oroboros\autoload\AutoloadExtension
     * @covers oroboros\bootstrap\BootstrapExtension
     * @covers oroboros\cache\CacheExtension
     * @covers oroboros\cli\CliExtension
     * @covers oroboros\cluster\ClusterExtension
     * @covers oroboros\codex\CodexExtension
     * @covers oroboros\collection\CollectionExtension
     * @covers oroboros\component\ComponentExtension
     * @covers oroboros\config\ConfigExtension
     * @covers oroboros\controller\ControllerExtension
     * @covers oroboros\database\DatabaseExtension
     * @covers oroboros\debug\DebugExtension
     * @covers oroboros\entity\EntityExtension
     * @covers oroboros\enum\EnumExtension
     * @covers oroboros\environment\EnvironmentExtension
     * @covers oroboros\event\EventExtension
     * @covers oroboros\file\FileExtension
     * @covers oroboros\format\FormatExtension
     * @covers oroboros\http\HttpExtension
     * @covers oroboros\job\JobExtension
     * @covers oroboros\load\LoadExtension
     * @covers oroboros\log\LogExtension
     * @covers oroboros\math\MathExtension
     * @covers oroboros\message\MessageExtension
     * @covers oroboros\model\ModelExtension
     * @covers oroboros\module\ModuleExtension
     * @covers oroboros\package\PackageExtension
     * @covers oroboros\parse\ParseExtension
     * @covers oroboros\prototype\PrototypeExtension
     * @covers oroboros\proxy\ProxyExtension
     * @covers oroboros\regex\RegexExtension
     * @covers oroboros\rest\RestExtension
     * @covers oroboros\router\RouterExtension
     * @covers oroboros\routine\RoutineExtension
     * @covers oroboros\service\ServiceExtension
     * @covers oroboros\stream\StreamExtension
     * @covers oroboros\template\TemplateExtension
     * @covers oroboros\validate\ValidationExtension
     * @covers oroboros\view\ViewExtension
     * @covers oroboros\autoload\traits\AutoloadManagerTrait
     * @covers oroboros\autoload\traits\Psr4Trait
     * @covers oroboros\Oroboros
     * @covers oroboros\core\traits\OroborosTrait
     * @covers oroboros\core\traits\patterns\structural\StaticControlApiTrait
     * @covers oroboros\core\traits\utilities\core\CallableUtilityTrait
     * @covers oroboros\core\traits\utilities\core\StringUtilityTrait
     * @covers oroboros\core\traits\core\StaticBaselineTrait
     * @covers oroboros\core\traits\core\BaselineInternalsTrait
     */
    public function testCleanInitialization()
    {
        require_once OROBOROS_ROOT_DIRECTORY . 'bin/bootload/bootload.routine.php';
        require_once OROBOROS_ROOT_DIRECTORY . 'bin/bootload/subroutines/autoload.routine.php';
        require_once OROBOROS_ROOT_DIRECTORY . 'bin/bootload/subroutines/build.routine.php';
        \oroboros\core\utilities\core\CoreConfig::flush();
        $config = new \oroboros\core\utilities\core\CoreConfig();
        \oroboros\Oroboros::initialize();
        $this->assertTrue( \oroboros\Oroboros::isInitialized() );
    }

    /**
     * @group directory-permissions
     */
    public function testCacheWriteable()
    {
        $this->assertTrue( OROBOROS_CACHE && is_writable( OROBOROS_CACHE ) );
    }

    /**
     * @group directory-permissions
     */
    public function testConfWriteable()
    {
        $this->assertTrue( OROBOROS_CONFIG && is_writable( OROBOROS_CONFIG ) );
    }

    /**
     * @group directory-permissions
     */
    public function testDocsWriteable()
    {
        $this->assertTrue( OROBOROS_DOCS && is_writable( OROBOROS_DOCS ) );
    }

    /**
     * @group directory-permissions
     */
    public function testTmpWriteable()
    {
        $this->assertTrue( OROBOROS_TEMP_DIRECTORY && is_writable( OROBOROS_TEMP_DIRECTORY ) );
    }

    /**
     * @group initialization
     */
    public function testPhpVersion()
    {
        $this->assertTrue( PHP_VERSION_ID >= 54000 );
    }

}
