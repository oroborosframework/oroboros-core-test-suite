<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\extensions\file;

/**
 * <File Extension Test Cases>
 * These tests prove the stable compliance of the file extension.
 * @group extensions
 * @group utilities
 * @group file
 * @covers \oroboros\file\FileExtension
 * @covers \oroboros\file\traits\FileTrait
 * @covers \oroboros\core\traits\extensions\ExtensionTrait
 * @covers \oroboros\core\traits\patterns\structural\StaticControlApiTrait
 * @covers \oroboros\core\traits\patterns\structural\StaticControlApiExtensionTrait
 * @covers \oroboros\core\traits\core\StaticBaselineTrait
 * @covers \oroboros\core\traits\core\BaselineInternalsTrait
 * @covers \oroboros\collection\traits\CollectionTrait
 * @covers \oroboros\collection\traits\ContainerTrait
 * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
 *
 */
class FileExtensionTest
    extends \oroboros\tests\libraries\extension\CoreExtensionTest
{

    const TEST_CLASS = '\\oroboros\\file\\FileExtension';
    const TEST_CLASS_EXPECTED_CONTRACT = '\\oroboros\\core\\interfaces\\contract\\extensions\\core\\CoreExtensionContract';
    const TEST_CLASS_EXPECTED_API = '\\oroboros\\file\\FileApi';

    protected $expected_extension_context = 'core';
    protected $expected_extension_id = 'file';
    protected $expected_extension_indexes = array();
    protected $expected_extension_api = array();

    /**
     * @group extensions
     * @group utilities
     * @group file
     * @covers \oroboros\file\FileExtension
     * @covers \oroboros\file\traits\FileTrait
     * @covers \oroboros\core\traits\extensions\ExtensionTrait
     * @covers \oroboros\core\traits\patterns\structural\StaticControlApiTrait
     * @covers \oroboros\core\traits\patterns\structural\StaticControlApiExtensionTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     *
     */
    public function testExtensionValidation()
    {
        $this->assertTrue( true );
        $this->runTestExtensionValidation();
    }

    /**
     * @group extensions
     * @group utilities
     * @group file
     * @covers \oroboros\file\FileExtension
     * @covers \oroboros\file\traits\FileTrait
     * @covers \oroboros\core\traits\extensions\ExtensionTrait
     * @covers \oroboros\core\traits\patterns\structural\StaticControlApiTrait
     * @covers \oroboros\core\traits\patterns\structural\StaticControlApiExtensionTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     *
     */
    public function testExtensionIndexes()
    {
        $this->assertTrue( true );
        $this->runTestExtensionIndexes();
    }

    /**
     * @group extensions
     * @group utilities
     * @group file
     * @covers \oroboros\file\FileExtension
     * @covers \oroboros\file\traits\FileTrait
     * @covers \oroboros\core\traits\extensions\ExtensionTrait
     * @covers \oroboros\core\traits\patterns\structural\StaticControlApiTrait
     * @covers \oroboros\core\traits\patterns\structural\StaticControlApiExtensionTrait
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\collection\traits\CollectionTrait
     * @covers \oroboros\collection\traits\ContainerTrait
     * @covers \oroboros\core\traits\patterns\behavioral\RegistryTrait
     *
     */
    public function testExtensionApi()
    {
        $this->assertTrue( true );
        $this->runTestExtensionApi();
    }

}
