<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\utilities\format;

use PHPUnit\Framework\TestCase;

/**
 * @group formatting
 * @covers \oroboros\format\traits\StringTrait
 * @covers \oroboros\core\traits\utilities\core\StringUtilityTrait
 * @covers \oroboros\core\traits\utilities\UtilityTrait
 */
class StringFormatterTest
    extends \oroboros\tests\AbstractTestClass
{

    const TEST_CLASS = '\\oroboros\\format\\StringFormatter';

    private $_test_params = array(
        'canonical' => 'this-is-a-canonical-string',
        'brutecase' => 'ThisIsABrutecaseString',
        'camelcase' => 'thisIsACamelcaseString',
        'interpolate' => 'this [explanation]',
        'interpolate-context' => array(
            'explanation' => 'is an interpolated string'
        ),
        'blocktext' =>
        'This is a block of text with a bunch of line breaks.' . PHP_EOL .
        'This is a block of text with a bunch of line breaks.' . PHP_EOL .
        'This is a block of text with a bunch of line breaks.' . PHP_EOL .
        'This is a block of text with a bunch of line breaks.' . PHP_EOL .
        'This is a block of text with a bunch of line breaks.' . PHP_EOL .
        'This is a block of text with a bunch of line breaks.' . PHP_EOL .
        'This is a block of text with a bunch of line breaks.' . PHP_EOL .
        'This is a block of text with a bunch of line breaks.',
        'exerpt' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, '
        . 'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. '
        . 'Tellus rutrum tellus pellentesque eu tincidunt tortor aliquam. Amet '
        . 'nisl purus in mollis nunc sed. Amet massa vitae tortor condimentum '
        . 'lacinia quis vel. Suscipit adipiscing bibendum est ultricies. '
        . 'Ullamcorper sit amet risus nullam eget felis eget nunc. Faucibus '
        . 'nisl tincidunt eget nullam non. Maecenas volutpat blandit aliquam '
        . 'etiam erat. Ut pharetra sit amet aliquam id diam. Commodo nulla '
        . 'facilisi nullam vehicula ipsum. Mauris in aliquam sem fringilla ut. '
        . 'Libero enim sed faucibus turpis in eu. Libero id faucibus nisl '
        . 'tincidunt. Viverra adipiscing at in tellus integer feugiat '
        . 'scelerisque varius. Augue lacus viverra vitae congue eu consequat '
        . 'ac felis donec. Lorem dolor sed viverra ipsum nunc aliquet bibendum '
        . 'enim. Elit duis tristique sollicitudin nibh sit. Nam libero justo '
        . 'laoreet sit amet cursus sit amet dictum. At augue eget arcu dictum '
        . 'varius duis at. Felis bibendum ut tristique et egestas. Egestas '
        . 'egestas fringilla phasellus faucibus scelerisque. Vulputate mi sit '
        . 'amet mauris commodo quis imperdiet massa. Scelerisque fermentum dui '
        . 'faucibus in ornare quam viverra orci sagittis. Donec adipiscing '
        . 'tristique risus nec feugiat in fermentum. Tempor orci dapibus '
        . 'ultrices in iaculis nunc. Urna et pharetra pharetra massa massa. '
        . 'Turpis egestas sed tempus urna. Est lorem ipsum dolor sit amet '
        . 'consectetur adipiscing. Gravida quis blandit turpis cursus. Hendrerit'
        . ' dolor magna eget est lorem ipsum. Aliquam malesuada bibendum arcu '
        . 'vitae elementum curabitur vitae nunc. Mus mauris vitae ultricies leo.'
        . ' Velit egestas dui id ornare arcu odio ut sem nulla. Varius duis at '
        . 'consectetur lorem donec massa sapien faucibus. Ac orci phasellus '
        . 'egestas tellus rutrum tellus pellentesque. Maecenas ultricies mi '
        . 'eget mauris pharetra et ultrices neque ornare. Amet risus nullam '
        . 'eget felis. Consectetur a erat nam at lectus urna duis convallis. '
        . 'Integer eget aliquet nibh praesent tristique magna sit amet purus. '
        . 'Massa tempor nec feugiat nisl pretium fusce id velit ut. Mauris '
        . 'ultrices eros in cursus turpis massa tincidunt dui ut. Blandit '
        . 'turpis cursus in hac habitasse platea dictumst quisque. Arcu non '
        . 'sodales neque sodales ut etiam sit. Quam quisque id diam vel. Sed '
        . 'risus ultricies tristique nulla. Phasellus faucibus scelerisque '
        . 'eleifend donec pretium vulputate sapien. Et tortor at risus viverra '
        . 'adipiscing at in tellus. Nisl nisi scelerisque eu ultrices vitae '
        . 'auctor eu augue ut. Enim diam vulputate ut pharetra. Facilisi etiam '
        . 'dignissim diam quis enim lobortis. In hac habitasse platea dictumst '
        . 'quisque sagittis purus sit amet. Tortor posuere ac ut consequat '
        . 'semper viverra. In mollis nunc sed id semper risus. Eu tincidunt '
        . 'tortor aliquam nulla facilisi cras. Faucibus nisl tincidunt eget '
        . 'nullam non. Amet porttitor eget dolor morbi. Vitae tempus quam '
        . 'pellentesque nec nam aliquam sem et. Sagittis vitae et leo duis ut '
        . 'diam. Ligula ullamcorper malesuada proin libero nunc consequat '
        . 'interdum. Facilisi morbi tempus iaculis urna. Vel facilisis volutpat '
        . 'est velit egestas dui id ornare arcu. Et pharetra pharetra massa '
        . 'massa ultricies mi quis hendrerit. Nulla aliquet enim tortor at. '
        . 'Non consectetur a erat nam at lectus urna duis convallis. Faucibus '
        . 'in ornare quam viverra orci sagittis eu volutpat. Pharetra pharetra '
        . 'massa massa ultricies. Maecenas volutpat blandit aliquam etiam erat '
        . 'velit scelerisque in dictum. Ultricies mi eget mauris pharetra et. '
        . 'Aliquam ultrices sagittis orci a scelerisque purus. Gravida arcu ac '
        . 'tortor dignissim convallis aenean et. Viverra mauris in aliquam sem '
        . 'fringilla ut morbi. Augue lacus viverra vitae congue eu consequat ac '
        . 'felis. Proin fermentum leo vel orci porta non. Massa id neque '
        . 'aliquam vestibulum morbi blandit. Duis at consectetur lorem donec. '
        . 'Nunc faucibus a pellentesque sit amet porttitor. Laoreet non '
        . 'curabitur gravida arcu ac tortor dignissim convallis. Erat imperdiet '
        . 'sed euismod nisi porta. Et odio pellentesque diam volutpat commodo '
        . 'sed egestas. Amet consectetur adipiscing elit pellentesque habitant '
        . 'morbi. Nunc non blandit massa enim nec. Morbi quis commodo odio '
        . 'aenean. Pretium aenean pharetra magna ac.',
        'no-space-exerpt' => 'ThisIsAReallyLongStringThatHasNoSpacesToTestHow'
        . 'WellTheExerptMethodWorksWhenItHasToDealWithSomeReallyObnoxious'
        . 'EdgeCaseLikeThisThatDoesn\'tReallyMakeMuchSenseButStillMight'
        . 'OccasionallyComeUpWhenThereIsSomeSillyPersonThatNeverBothersToUse'
        . 'DecentQualityControlOnTheirOutputAndExcpectsThirdPartyDevelopersTo'
        . 'CoddleThemLikeSomeKindOfManChildOrSomeSimilarlyImmatureMannerOf'
        . 'IrresponsibilityOfTheAdultPersuasionBecauseApparentlyNoMatterHow'
        . 'SloppyAnEndUserIsWhenImplementingAFreeLibraryForTheirOwnNeedsTheir'
        . 'MistakesAreAlwaysTheFaultOfTheDeveloperWhoDidn\'tHaveAnyWayOf'
        . 'GuessingHowBadlyTheyWouldMisuseTheFreeToolsTheyHaveBeenGraciously'
        . 'ProvidedWithAtAbsolutelyZeroCost',
    );

    /**
     * @group formatting
     * @covers \oroboros\format\traits\StringTrait
     * @covers \oroboros\core\traits\utilities\core\StringUtilityTrait
     * @covers \oroboros\core\traits\utilities\UtilityTrait
     */
    public function testParseArray()
    {
        $test = $this->getTestClass();
        $this->assertEquals( 'this-is-a-brutecase-string',
            $test::canonicalize( $this->_test_params['brutecase'] ) );
        $this->assertEquals( 'this-is-a-camelcase-string',
            $test::canonicalize( $this->_test_params['camelcase'] ) );
        $this->assertEquals( 'a-canonical',
            $test::extract( $this->_test_params['canonical'], 'this-is-',
                '-string' ) );
        $this->assertEquals( 'this-is-a-canonical',
            $test::extract( $this->_test_params['canonical'], null, '-string' ) );
        $this->assertEquals( 'a-canonical-string',
            $test::extract( $this->_test_params['canonical'], 'this-is-' ) );
        $this->assertEquals( 'thisIsACanonicalString',
            $test::camelcase( $this->_test_params['canonical'] ) );
        $this->assertEquals( 'ThisIsACanonicalString',
            $test::brutecase( $this->_test_params['canonical'] ) );
        $this->assertEquals( '    ' . $this->_test_params['canonical'],
            $test::leftPad( $this->_test_params['canonical'], 4 ) );
        $this->assertEquals( $this->_test_params['canonical'] . '    ',
            $test::rightPad( $this->_test_params['canonical'], 4 ) );
        $this->assertEquals( '    ' . $this->_test_params['canonical'],
            $test::indent( $this->_test_params['canonical'], 4 ) );
        $this->assertEquals( '[' . $this->_test_params['canonical'] . ']',
            $test::wrap( $this->_test_params['canonical'] ) );
        $this->assertTrue( is_array( $test::split( $this->_test_params['canonical'],
                    '-' ) ) );
        $this->assertTrue( count( $test::exerpt( $this->_test_params['exerpt'] ) )
            <= 300 );
        $this->assertTrue( count( $test::exerpt( $this->_test_params['no-space-exerpt'] ) )
            <= 300 );
        $this->assertEquals( 'this is an interpolated string',
            $test::interpolate( $this->_test_params['interpolate'],
                $this->_test_params['interpolate-context'] ) );
    }

}
