<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\utilities;

/**
 * <Utility Abstraction Test Cases>
 * These tests prove the stable functionality of the UtilityTrait,
 * which is used as the basis of logic for classes that extend native PHP,
 * common 3rd party classes, or perform a basic set of universally required
 * services or logic without regard to why they were asked to do so. Utilities
 * are often static and usually stateless, and do not retain any internal
 * values unless it is universally used and instantiation of the resource is
 * particularly heavy.
 *
 * This test class is primarily focused on asserting an uniform approach to
 * the creation and operation of libraries, and that they have the correct
 * type, scope, context, and expected contractual designations provided.
 * Most other library test classes will extend from this class.
 *
 * @group utilities
 * @covers \oroboros\core\traits\core\BaselineTrait
 * @covers \oroboros\core\traits\core\StaticBaselineTrait
 * @covers \oroboros\core\traits\core\BaselineInternalsTrait
 */
class UtilityTest
    extends \oroboros\tests\core\baseline\StaticBaselineTest
{
    /**
     * Can be overridden to provide a baseline expected test
     * object using the getTestClass method.
     */
//    const TEST_CLASS = '\\oroboros\\Oroboros';

    /**
     * Can be overridden to provide a baseline invalid expected test
     * object using the getInvalidTestClass method.
     */
    const TEST_CLASS_INVALID = false;

    /**
     * Can be overridden to provide a baseline misconfigured test
     * object using the getMisconfiguredTestClass method.
     */
    const TEST_CLASS_MISCONFIGURED = false;

    /**
     * Can be overridden to designate that the given
     * contract interface should be integrity checked.
     * If true, the provided testing class MUST implement
     * the given contract.
     */
    const TEST_CLASS_VALIDATE_CLASS_CONTRACT = true;

    /**
     * Can be overridden to designate that the given
     * api interface should be integrity checked.
     */
    const TEST_CLASS_VALIDATE_CLASS_API = false;

    /**
     * Can be overridden to designate that the given
     * enumerated interface should be integrity checked.
     */
    const TEST_CLASS_VALIDATE_CLASS_ENUMERATOR = false;

    /**
     * Can be overridden to designate that the given
     * set of interfaces should be integrity checked.
     * If true, ALL interfaces listed in the getExpectedInterfaces
     * method MUST be implemented by the testing subject.
     */
    const TEST_CLASS_VALIDATE_INTERFACES = true;

    /**
     * Can be overridden to designate that the given
     * set of interfaces should be integrity checked.
     * If true, ALL interfaces listed in the getExpectedInterfaces
     * method MUST be implemented by the testing subject.
     */
    const TEST_CLASS_VALIDATE_BASELINE_INTERFACES = true;

    /**
     * Can be overridden to designate that the given
     * class context designations should be integrity checked.
     */
    const TEST_CLASS_VALIDATE_DESIGNATIONS = true;

    /**
     * Can be overridden to designate that the given
     * class context should be integrity checked.
     */
    const TEST_CLASS_VALIDATE_CLASS_CONTEXT = true;

    /**
     * Can be overridden to designate that the given
     * class scope should be integrity checked.
     */
    const TEST_CLASS_VALIDATE_CLASS_TYPE = true;

    /**
     * Can be overridden to designate that the given
     * class scope should be integrity checked.
     */
    const TEST_CLASS_VALIDATE_CLASS_SCOPE = true;

    /**
     * Declares the root level interface that defines the
     * acceptable class contexts for the current scope.
     */
    const TEST_CLASS_ALLOWED_CLASS_CONTEXTS = '\\oroboros\\core\\interfaces\\enumerated\\context\\ClassContext';

    /**
     * Declares the root level interface that defines the
     * acceptable class types for the current scope.
     */
    const TEST_CLASS_ALLOWED_CLASS_TYPES = '\\oroboros\\core\\interfaces\\enumerated\\type\\UtilityClassTypes';

    /**
     * Declares the root level interface that defines the
     * acceptable class scopes for the current scope.
     */
    const TEST_CLASS_ALLOWED_CLASS_SCOPES = '\\oroboros\\core\\interfaces\\enumerated\\scope\\UtilityClassScopes';

    /**
     * Can be overridden to provide a contract interface that
     * the testing class subject is expected to implement.
     */
    const TEST_CLASS_EXPECTED_CONTRACT = '\\oroboros\\core\\interfaces\\contract\\utilities\\UtilityContract';

    /**
     * Can be overridden to provide an api interface that
     * the testing class subject is expected to either implement,
     * or declare indirectly via the OROBOROS_API class constant.
     */
    const TEST_CLASS_EXPECTED_API = false;

    /**
     * Can be overridden to provide an enumerated interface that
     * the testing class subject is expected to implement.
     */
    const TEST_CLASS_EXPECTED_ENUMERATOR = false;

    /**
     * Can be overridden to provide an explicit class context that
     * the testing class subject is expected to declare via the
     * OROBOROS_CLASS_CONTEXT class constant.
     */
    const TEST_CLASS_EXPECTED_CLASS_CONTEXT = \oroboros\core\interfaces\enumerated\context\ClassContext::CLASS_CONTEXT_UTILITY;

    /**
     * Can be overridden to provide an explicit class context that
     * the testing class subject is expected to declare via the
     * OROBOROS_CLASS_TYPE class constant.
     */
    const TEST_CLASS_EXPECTED_CLASS_TYPE = false;

    /**
     * Can be overridden to provide an explicit class context that
     * the testing class subject is expected to declare via the
     * OROBOROS_CLASS_SCOPE class constant.
     */
    const TEST_CLASS_EXPECTED_CLASS_SCOPE = false;

    /**
     * @group utilities
     * @covers \oroboros\core\traits\core\StaticBaselineTrait
     * @covers \oroboros\core\traits\core\BaselineInternalsTrait
     * @covers \oroboros\core\traits\OroborosTrait
     */
    public function testObjectInitialization()
    {
        $test = $this->getTestClass();
        $this->assertTrue( true );
    }

    /**
     * Designates whether the test class should have the
     * Oroboros class designation of Utility class context.
     *
     * Overriding this method and returning true or false will designate
     * that the integrity check logic should assert that the test class
     * declares an equal designation in the context of being used as a utility.
     *
     * If this is true, it can operate as a utility regardless of its
     * declared class context. If this is false, it cannot operate
     * as a utility, and the system will reject attempts to pass it
     * as one.
     *
     * @note A utility is a general purpose, typically stateless class used
     *     for accomplishing simple operations. A utility with an indirect
     *     declaration may have altered its internal logic to represent a
     *     more focused task, and may add excessive weight to an operation
     *     that is out of scope of the expected result. For example a more
     *     robust library that extended upon an abstract utility may be
     *     performing heavier operations than the underlying utility would.
     *     In most cases, use the lowest level applicable instance of a
     *     utility (which typically will have a direct designation as a
     *     utility) to accomplish the desired task with performance in mind.
     *
     * @return bool
     */
    protected static function getExpectedClassDesignationUtility()
    {
        return true;
    }

    /**
     * Returns the interface that all further contract interface
     * extensions should validate as an instance of.
     *
     * This is used to designate that a family of scoped classes follows
     * the correct contractual interface schema.
     *
     * This method can be overridden in test classes to scope
     * a set of contract interfaces further.
     *
     * @return string
     */
    protected static function getBaseContractInterface()
    {
        return '\\oroboros\\core\\interfaces\\contract\\utilities\\UtilityContract';
    }

    /**
     * Sets up the expected set of 3rd party or PHP internal
     * interfaces that the given test class MUST honor
     */
    public static function declareExpectedInterfaces()
    {
        return array_merge(
            parent::declareExpectedInterfaces(), array(
                '\\oroboros\\core\\interfaces\\contract\\utilities\\UtilityContract'
            ) );
    }

}
