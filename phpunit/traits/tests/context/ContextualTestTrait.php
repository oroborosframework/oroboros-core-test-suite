<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\traits\tests\context;

/**
 * <Oroboros Core Contextual Test Trait>
 * Provides tests that will automatically fire to
 * assert integrity of contextual objects.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category traits
 * @category unit-testing
 * @category tests
 * @package oroboros/tests
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 */
trait ContextualTestTrait
{

    /**
     * Tests context matching for interface assignment.
     * @group core
     * @group context
     * @covers \oroboros\core\traits\core\context\ContextTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     */
    public function testContextContract()
    {
        $test = $this->getTestClass();
        $this->assertContextualContractValid( $test );
        //Contractually valid
        $this->assertTrue( true );
        return;
    }

    /**
     * Tests context meta indexing.
     * @group core
     * @group context
     * @covers \oroboros\core\traits\core\context\ContextTrait
     * @covers \oroboros\core\traits\core\context\meta\MetaContextContextTrait
     * @covers \oroboros\core\traits\core\context\meta\MetaTypeContextTrait
     * @covers \oroboros\core\traits\core\context\meta\MetaSubTypeContextTrait
     * @covers \oroboros\core\traits\core\context\meta\MetaContextContextTrait
     * @covers \oroboros\core\traits\core\context\meta\MetaValueContextTrait
     * @covers \oroboros\core\traits\core\context\meta\MetaCategoryContextTrait
     * @covers \oroboros\core\traits\core\context\meta\MetaSubCategoryContextTrait
     * @covers \oroboros\core\traits\core\context\meta\MetaPackageContextTrait
     * @covers \oroboros\core\traits\core\context\meta\MetaSubPackageContextTrait
     * @covers \oroboros\core\traits\core\context\meta\MetaIndexTrait
     * @covers \oroboros\core\traits\core\context\meta\MetaIndexFactoryTrait
     * @covers \oroboros\core\traits\core\context\meta\MetaIndexBuilderTrait
     * @covers \oroboros\core\traits\patterns\creational\BuilderTrait
     * @covers \oroboros\core\traits\patterns\creational\FactoryTrait
     * @covers \oroboros\core\traits\core\container\IndexTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     */
    public function testContextMetaIndex()
    {
        $test = $this->getTestClass();
        $this->assertContextualMetaIndexValid( $test );
        //Meta-index valid
        $this->assertTrue( true );
        return;
    }

    /**
     * Tests context matching for expected type.
     * @group core
     * @group context
     * @covers \oroboros\core\traits\core\context\ContextTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     */
    public function testContextType()
    {
        $test = $this->getTestClass();
        if ( is_null( $this->getExpectedContextualType() ) )
        {
            //Nothing to test
            $this->assertTrue( true );
            return;
        }
        $this->assertContextualTypeValid( $test );
        //Contextual type valid
        $this->assertTrue( true );
        return;
        $context_object_evaluator = clone $test;
        if ( !is_null( $this->getExpectedContextualType() ) )
        {
            $stringable_object_evaluator = $this->getStringableObject( $this->getExpectedContextualType() );
        }
        //Check Type literal
        $this->assertEquals( $this->getExpectedContextualType(),
            $test->getType(),
            sprintf( 'getType() [%1$s] must be equal to provided scalar or null type [%2$s] in [%3$s]',
                $test->getType(), $this->getExpectedContextualType(), __METHOD__ ) );
        if ( !is_null( $this->getExpectedContextualType() ) )
        {
            //Assert positive type check
            //Check hasType literal
            $this->assertTrue( $test->hasType( $this->getExpectedContextualType() ),
                sprintf( 'hasType() [%1$s] must return true for provided scalar type [%2$s] in [%3$s]',
                    $test->getType(), $this->getExpectedContextualType(),
                    __METHOD__ ) );
            //Check hasType comparator
            $this->assertTrue( $test->hasType( $context_object_evaluator ),
                sprintf( 'hasType() [%1$s] must return true for provided comparator object with scalar type [%2$s] in [%3$s]',
                    $test->getType(), $this->getExpectedContextualType(),
                    __METHOD__ ) );
            //Check hasType invalid
            $this->assertFalse( $test->hasType( new \stdClass() ),
                sprintf( 'hasType() [%1$s] must return false for invalid type [%2$s] in [%3$s]',
                    $test->getType(), 'stdClass', __METHOD__ ) );

            if ( is_string( $test->getType() ) )
            {
                //Assert true for stringable object assertions
                $this->assertTrue( $test->hasType( $stringable_object_evaluator ),
                    sprintf( 'hasType() [%1$s] must return true for provided string type [%2$s] extracted from stringable object [%4$s] in [%3$s].',
                        $test->getType(), $this->getExpectedContextualType(),
                        __METHOD__, get_class( $stringable_object_evaluator ) ) );
            } else
            {
                //Assert false for stringable object assertions
                $this->assertFalse( $test->hasType( $stringable_object_evaluator ),
                    sprintf( 'hasType() [%1$s] must be return false for provided non-string scalar type [%2$s] extracted from stringable object [%4$s] in [%3$s].',
                        $test->getType(), $this->getExpectedContextualType(),
                        __METHOD__, get_class( $stringable_object_evaluator ) ) );
            }
        } else
        {
            //Assert negative type check
            //Check hasType literal
            $this->assertFalse( $test->hasType( $this->getExpectedContextualType() ),
                sprintf( 'hasType() [%1$s] must return false for all provided values on null type [%2$s] in [%3$s]',
                    $test->getType(), $this->getExpectedContextualType(),
                    __METHOD__ ) );
            //Check hasType comparator
            $this->assertFalse( $test->hasType( $context_object_evaluator ),
                sprintf( 'hasType() [%1$s] must return false for all provided values on null type from context evaluator [%2$s] in [%3$s]',
                    $test->getType(), get_class( $context_object_evaluator ),
                    __METHOD__ ) );
            //Check hasType invalid
            $this->assertFalse( $test->hasType( new \stdClass() ),
                sprintf( 'hasType() [%1$s] must return false for invalid type [%2$s] in [%3$s]',
                    $test->getType(), 'stdClass', __METHOD__ ) );
        }
    }

    /**
     * Tests context matching for expected sub-type.
     * @group core
     * @group context
     * @covers \oroboros\core\traits\core\context\ContextTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     */
    public function testContextSubType()
    {
        $test = $this->getTestClass();
        if ( is_null( $this->getExpectedContextualSubType() ) )
        {
            //Nothing to test
            $this->assertTrue( true );
            return;
        }
        $this->assertContextualSubTypeValid( $test );
        //Contextual sub-type valid
        $this->assertTrue( true );
        return;
    }

    /**
     * Tests context matching for expected context.
     * @group core
     * @group context
     * @covers \oroboros\core\traits\core\context\ContextTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     */
    public function testContextContext()
    {
        $test = $this->getTestClass();
        if ( is_null( $this->getExpectedContextualContext() ) )
        {
            //Nothing to test
            $this->assertTrue( true );
            return;
        }
        $this->assertContextualContextValid( $test );
        //Contextual context valid
        $this->assertTrue( true );
        return;
        $context_object_evaluator = clone $test;
        $stringable_object_evaluator = $this->getStringableObject( $this->getExpectedContextualContext() );
        //Check Context literal
        $this->assertEquals( $this->getExpectedContextualContext(),
            $test->getContext(),
            sprintf( 'getContext() [%1$s] must be equal to provided scalar context [%2$s] in [%3$s]',
                $test->getContext(), $this->getExpectedContextualContext(),
                __METHOD__ ) );
        //Check hasContext literal
        $this->assertTrue( $test->hasContext( $this->getExpectedContextualContext() ),
            sprintf( 'hasContext() [%1$s] must return true for provided scalar context [%2$s] in [%3$s]',
                $test->getContext(), $this->getExpectedContextualContext(),
                __METHOD__ ) );
        //Check hasContext comparator
        $this->assertTrue( $test->hasContext( $context_object_evaluator ),
            sprintf( 'hasContext() [%1$s] must return true for provided scalar context [%2$s] in [%3$s]',
                $test->getContext(), $this->getExpectedContextualContext(),
                __METHOD__ ) );
        //Check hasContext invalid
        $this->assertFalse( $test->hasContext( new \stdClass() ),
            sprintf( 'hasContext() [%1$s] must return false for invalid context [%2$s] in [%3$s]',
                $test->getContext(), 'stdClass', __METHOD__ ) );

        if ( is_string( $test->getContext() ) )
        {
            //Assert true for stringable object assertions
            $this->assertTrue( $test->hasContext( $stringable_object_evaluator ),
                sprintf( 'getContext() [%1$s] must return true for provided string context [%2$s] extracted from stringable object [%4$s] in [%3$s].',
                    $test->getContext(), $this->getExpectedContextualContext(),
                    __METHOD__, get_class( $stringable_object_evaluator ) ) );
        } else
        {
            //Assert false for stringable object assertions
            $this->assertFalse( $test->hasContext( $stringable_object_evaluator ),
                sprintf( 'getContext() [%1$s] must be return false for provided non-string scalar context [%2$s] extracted from stringable object [%4$s] in [%3$s].',
                    $test->getContext(), $this->getExpectedContextualContext(),
                    __METHOD__, get_class( $stringable_object_evaluator ) ) );
        }
    }

    /**
     * Tests context matching for expected value.
     * @group core
     * @group context
     * @covers \oroboros\core\traits\core\context\ContextTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     */
    public function testContextValue()
    {
        $test = $this->getTestClass();
        if ( is_null( $this->getExpectedContextualValue() ) )
        {
            //Nothing to test
            $this->assertTrue( true );
            return;
        }
        $this->assertContextualValueEqualsExpected( $test );
        //Contextual value valid
        $this->assertTrue( true );
        return;
        $context_object_evaluator = clone $test;
        $stringable_object_evaluator = $this->getStringableObject( $this->getExpectedContextualValue() );
        //Check Value stringable
        $this->assertEquals( (string) $this->getExpectedContextualValue(),
            (string) $test,
            sprintf( '__toString() [%1$s] must be equal to string cast expected value [%2$s] in [%3$s]',
                (string) $test, (string) $this->getExpectedContextualValue(),
                __METHOD__ ) );
        //Check Value literal
        $this->assertEquals( $this->getExpectedContextualValue(),
            $test->getValue(),
            sprintf( 'getValue() [%1$s] must be equal to provided scalar value [%2$s] in [%3$s]',
                $test->getValue(), $this->getExpectedContextualValue(),
                __METHOD__ ) );
        //Check hasValue literal
        $this->assertTrue( $test->hasValue( $this->getExpectedContextualValue() ),
            sprintf( 'hasValue() [%1$s] must return true for provided scalar value [%2$s] in [%3$s]',
                $test->getValue(), $this->getExpectedContextualValue(),
                __METHOD__ ) );
        //Check hasValue comparator
        $this->assertTrue( $test->hasValue( $context_object_evaluator ),
            sprintf( 'hasValue() [%1$s] must return true for provided scalar value [%2$s] in [%3$s]',
                $test->getValue(), $this->getExpectedContextualValue(),
                __METHOD__ ) );
        //Check hasValue invalid
        $this->assertFalse( $test->hasValue( new \stdClass() ),
            sprintf( 'hasValue() [%1$s] must return false for invalid value [%2$s] in [%3$s]',
                $test->getValue(), 'stdClass', __METHOD__ ) );

        if ( is_string( $test->getValue() ) )
        {
            //Assert true for stringable object assertions
            $this->assertTrue( $test->hasValue( $stringable_object_evaluator ),
                sprintf( 'getValue() [%1$s] must return true for provided string value [%2$s] extracted from stringable object [%4$s] in [%3$s].',
                    $test->getValue(), $this->getExpectedContextualValue(),
                    __METHOD__, get_class( $stringable_object_evaluator ) ) );
        } else
        {
            //Assert false for stringable object assertions
            $this->assertFalse( $test->hasValue( $stringable_object_evaluator ),
                sprintf( 'getValue() [%1$s] must be return false for provided non-string scalar value [%2$s] extracted from stringable object [%4$s] in [%3$s].',
                    $test->getValue(), $this->getExpectedContextualValue(),
                    __METHOD__, get_class( $stringable_object_evaluator ) ) );
        }
    }

    /**
     * Tests context matching for expected category.
     * @group core
     * @group context
     * @covers \oroboros\core\traits\core\context\ContextTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     */
    public function testContextCategory()
    {
        $test = $this->getTestClass();
        if ( is_null( $this->getExpectedContextualCategory() ) )
        {
            //Nothing to test
            $this->assertTrue( true );
            return;
        }
        $this->assertContextualCategoryValid( $test );
        //Contextual category valid
        $this->assertTrue( true );
        return;
        $context_object_evaluator = clone $test;
        if ( !is_null( $this->getExpectedContextualCategory() ) )
        {
            $stringable_object_evaluator = $this->getStringableObject( $this->getExpectedContextualCategory() );
        }
        //Check Type literal
        $this->assertEquals( $this->getExpectedContextualCategory(),
            $test->getCategory(),
            sprintf( 'hasCategory() [%1$s] must be equal to provided scalar or null category [%2$s] in [%3$s]',
                $test->getCategory(), $this->getExpectedContextualCategory(),
                __METHOD__ ) );
        if ( !is_null( $this->getExpectedContextualCategory() ) )
        {
            //Assert positive type check
            //Check hasType literal
            $this->assertTrue( $test->hasCategory( $this->getExpectedContextualCategory() ),
                sprintf( 'hasCategory() [%1$s] must return true for provided scalar category [%2$s] in [%3$s]',
                    $test->getCategory(),
                    $this->getExpectedContextualCategory(), __METHOD__ ) );
            //Check hasType comparator
            $this->assertTrue( $test->hasCategory( $context_object_evaluator ),
                sprintf( 'hasCategory() [%1$s] must return true for provided comparator object with scalar category [%2$s] in [%3$s]',
                    $test->getCategory(),
                    $this->getExpectedContextualCategory(), __METHOD__ ) );
            //Check hasType invalid
            $this->assertFalse( $test->hasCategory( new \stdClass() ),
                sprintf( 'hasCategory() [%1$s] must return false for invalid category [%2$s] in [%3$s]',
                    $test->getCategory(), 'stdClass', __METHOD__ ) );

            if ( is_string( $test->getCategory() ) )
            {
                //Assert true for stringable object assertions
                $this->assertTrue( $test->hasCategory( $stringable_object_evaluator ),
                    sprintf( 'hasCategory() [%1$s] must return true for provided string category [%2$s] extracted from stringable object [%4$s] in [%3$s].',
                        $test->getCategory(),
                        $this->getExpectedContextualCategory(), __METHOD__,
                        get_class( $stringable_object_evaluator ) ) );
            } else
            {
                //Assert false for stringable object assertions
                $this->assertFalse( $test->hasCategory( $stringable_object_evaluator ),
                    sprintf( 'hasCategory() [%1$s] must be return false for provided non-string scalar category [%2$s] extracted from stringable object [%4$s] in [%3$s].',
                        $test->getCategory(),
                        $this->getExpectedContextualCategory(), __METHOD__,
                        get_class( $stringable_object_evaluator ) ) );
            }
        } else
        {
            //Assert negative type check
            //Check hasType literal
            $this->assertFalse( $test->hasCategory( $this->getExpectedContextualCategory() ),
                sprintf( 'hasCategory() [%1$s] must return false for all provided values on null category [%2$s] in [%3$s]',
                    $test->getCategory(),
                    $this->getExpectedContextualCategory(), __METHOD__ ) );
            //Check hasType comparator
            $this->assertFalse( $test->hasCategory( $context_object_evaluator ),
                sprintf( 'hasCategory() [%1$s] must return false for all provided values on null category from context evaluator [%2$s] in [%3$s]',
                    $test->getCategory(),
                    get_class( $context_object_evaluator ), __METHOD__ ) );
            //Check hasType invalid
            $this->assertFalse( $test->hasType( new \stdClass() ),
                sprintf( 'hasCategory() [%1$s] must return false for invalid category [%2$s] in [%3$s]',
                    $test->getCategory(), 'stdClass', __METHOD__ ) );
        }
    }

    /**
     * Tests context matching for expected sub-category.
     * @group core
     * @group context
     * @covers \oroboros\core\traits\core\context\ContextTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     */
    public function testContextSubCategory()
    {
        $test = $this->getTestClass();
        if ( is_null( $this->getExpectedContextualSubCategory() ) )
        {
            //Nothing to test
            $this->assertTrue( true );
            return;
        }
        $this->assertContextualSubCategoryValid( $test );
        //Contextual sub-category valid
        $this->assertTrue( true );
        return;
        $context_object_evaluator = clone $test;
        if ( !is_null( $this->getExpectedContextualSubCategory() ) )
        {
            $stringable_object_evaluator = $this->getStringableObject( $this->getExpectedContextualSubCategory() );
        }
        //Check Type literal
        $this->assertEquals( $this->getExpectedContextualSubCategory(),
            $test->getSubcategory(),
            sprintf( 'hasSubcategory() [%1$s] must be equal to provided scalar or null sub-category [%2$s] in [%3$s]',
                $test->getSubcategory(),
                $this->getExpectedContextualSubCategory(), __METHOD__ ) );
        if ( !is_null( $this->getExpectedContextualSubCategory() ) )
        {
            //Assert positive type check
            //Check hasType literal
            $this->assertTrue( $test->hasSubcategory( $this->getExpectedContextualSubCategory() ),
                sprintf( 'hasSubcategory() [%1$s] must return true for provided scalar sub-category [%2$s] in [%3$s]',
                    $test->getSubcategory(),
                    $this->getExpectedContextualSubCategory(), __METHOD__ ) );
            //Check hasType comparator
            $this->assertTrue( $test->hasSubcategory( $context_object_evaluator ),
                sprintf( 'hasSubcategory() [%1$s] must return true for provided comparator object with scalar sub-category [%2$s] in [%3$s]',
                    $test->getSubcategory(),
                    $this->getExpectedContextualSubCategory(), __METHOD__ ) );
            //Check hasType invalid
            $this->assertFalse( $test->hasSubcategory( new \stdClass() ),
                sprintf( 'hasSubcategory() [%1$s] must return false for invalid sub-category [%2$s] in [%3$s]',
                    $test->getSubcategory(), 'stdClass', __METHOD__ ) );

            if ( is_string( $test->getSubcategory() ) )
            {
                //Assert true for stringable object assertions
                $this->assertTrue( $test->hasSubcategory( $stringable_object_evaluator ),
                    sprintf( 'hasSubcategory() [%1$s] must return true for provided string sub-category [%2$s] extracted from stringable object [%4$s] in [%3$s].',
                        $test->getSubcategory(),
                        $this->getExpectedContextualSubCategory(), __METHOD__,
                        get_class( $stringable_object_evaluator ) ) );
            } else
            {
                //Assert false for stringable object assertions
                $this->assertFalse( $test->hasSubcategory( $stringable_object_evaluator ),
                    sprintf( 'hasSubcategory() [%1$s] must be return false for provided non-string scalar sub-category [%2$s] extracted from stringable object [%4$s] in [%3$s].',
                        $test->getSubcategory(),
                        $this->getExpectedContextualSubCategory(), __METHOD__,
                        get_class( $stringable_object_evaluator ) ) );
            }
        } else
        {
            //Assert negative type check
            //Check hasType literal
            $this->assertFalse( $test->hasSubcategory( $this->getExpectedContextualSubCategory() ),
                sprintf( 'hasSubcategory() [%1$s] must return false for all provided values on null sub-category [%2$s] in [%3$s]',
                    $test->getSubcategory(),
                    $this->getExpectedContextualSubCategory(), __METHOD__ ) );
            //Check hasType comparator
            $this->assertFalse( $test->hasSubcategory( $context_object_evaluator ),
                sprintf( 'hasSubcategory() [%1$s] must return false for all provided values on null sub-category from context evaluator [%2$s] in [%3$s]',
                    $test->getSubcategory(),
                    get_class( $context_object_evaluator ), __METHOD__ ) );
            //Check hasType invalid
            $this->assertFalse( $test->hasType( new \stdClass() ),
                sprintf( 'hasSubcategory() [%1$s] must return false for invalid sub-category [%2$s] in [%3$s]',
                    $test->getSubcategory(), 'stdClass', __METHOD__ ) );
        }
    }

    /**
     * Tests context matching for expected package.
     * @group core
     * @group context
     * @covers \oroboros\core\traits\core\context\ContextTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     */
    public function testContextPackage()
    {
        $test = $this->getTestClass();
        if ( is_null( $this->getExpectedContextualPackage() ) )
        {
            //Nothing to test
            $this->assertTrue( true );
            return;
        }
        $this->assertContextualPackageValid( $test );
        //Contextual package valid
        $this->assertTrue( true );
        return;
    }

    /**
     * Tests context matching for expected sub-package.
     * @group core
     * @group context
     * @covers \oroboros\core\traits\core\context\ContextTrait
     * @covers \oroboros\validate\traits\ValidatorTrait
     */
    public function testContextSubPackage()
    {
        $test = $this->getTestClass();
        if ( is_null( $this->getExpectedContextualSubPackage() ) )
        {
            //Nothing to test
            $this->assertTrue( true );
            return;
        }
        $this->assertContextualSubPackageValid( $test );
        //Contextual sub-package valid
        $this->assertTrue( true );
        return;
    }

    /**
     * Tests context internal setters.
     * @group core
     * @group context
     * @covers \oroboros\core\traits\core\context\ContextTrait
     */
    public function testSetters()
    {
        $test = $this->getContextualTestClass();
        $context_object_evaluator = clone $test;
        $this->assertNotNull( $test->getType(),
            'Contextual setter tester getType() cannot be null for [%1$s] in [%2$s]',
            get_class( $test ), __METHOD__ );
        $this->assertNotNull( $test->getCategory(),
            'Contextual setter tester getCategory() cannot be null for [%1$s] in [%2$s]',
            get_class( $test ), __METHOD__ );
        $this->assertNotNull( $test->getSubcategory(),
            'Contextual setter tester getSubcategory() cannot be null for [%1$s] in [%2$s]',
            get_class( $test ), __METHOD__ );
    }

    protected function getTestClass()
    {
        $class = $this::TEST_CLASS;
        try
        {
            $context = $this->getExpectedContextualContext();
            $value = $this->getExpectedContextualValue();
            return new $class( $this->getDefaultContextualContext(),
                $this->getDefaultContextualValue() );
        } catch ( \Exception $e )
        {
            $this->renderTestclassError( $this::TEST_CLASS,
                sprintf( 'An exception occurred while loading the context testing class instance: [%s]',
                    $e->getMessage() ), $e->getTrace() );
        }
    }

    protected function getInvalidTestClass()
    {
        $class = $this::TEST_CLASS_INVALID;
        if ( !$class )
        {
            return false;
        }
        try
        {
            return new $class( $this->getDefaultContextualContext(),
                $this->getDefaultContextualValue() );
        } catch ( \Exception $e )
        {
            $this->renderTestclassError( $this::TEST_CLASS_INVALID,
                sprintf( 'An exception occurred while loading the context testing invalid class instance: [%s]',
                    $e->getMessage() ), $e->getTrace() );
        }
    }

    protected function getMisconfiguredTestClass()
    {
        $class = $this::TEST_CLASS_MISCONFIGURED;
        if ( !$class )
        {
            return false;
        }
        try
        {
            return new $class( $this->getDefaultContextualContext(),
                $this->getDefaultContextualValue() );
        } catch ( \Exception $e )
        {
            $this->renderTestclassError( $this::TEST_CLASS_MISCONFIGURED,
                sprintf( 'An exception occurred while loading the context testing misconfigured class instance: [%s]',
                    $e->getMessage() ), $e->getTrace() );
        }
    }

    protected function getContextualTestClass()
    {
        $class = '\\oroboros\\testclass\\core\\context\\ContextualTestContext';
        if ( !$class )
        {
            return false;
        }
        try
        {
            return new $class( $this->getDefaultContextualContext(),
                $this->getDefaultContextualValue() );
        } catch ( \Exception $e )
        {
            $this->renderTestclassError( $class,
                sprintf( 'An exception occurred while loading the context testing contextual setter class instance: [%s]',
                    $e->getMessage() ), $e->getTrace() );
        }
    }

}
