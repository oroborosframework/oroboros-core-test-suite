<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\traits\tests\container;

/**
 * <Oroboros Core Set Container Test Trait>
 * Provides tests that will automatically fire to
 * assert integrity of set container objects.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category traits
 * @category unit-testing
 * @category tests
 * @package oroboros/tests
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 */
trait SetContainerTestTrait
{

    protected $_test_values = array(
        'foo' => 'foo value',
        'bar' => 'bar value',
        'baz' => 'baz value',
        'quux' => 'quux value'
    );

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainer()
    {
        $class = $this->getTestClass();
        $this->assertContractInterfaceValid( $class );
        $this->assertInterfaceValid( $class );
        $this->assertApiInterfaceValid( $class );
        $classname = get_class( $class );
        try
        {
            //Testing invalid parameter injection into constructor
            $fail = new $classname( new \stdClass() );
            $this->assertFalse( true,
                sprintf( 'Failed to throw expected ContainerExceptionInterface when '
                    . 'instantiated with invalid parameters getter get() '
                    . 'for instance of [%1$s] in '
                    . '[%2$s] of test class [%3$s]', $classname, __METHOD__,
                    get_class( $this ) ) );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do now suppress testing framework exceptions
            throw $e;
        } catch ( \Psr\Container\ContainerExceptionInterface $e )
        {
            //expected
            $this->assertTrue( true );
        }
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerSerializable()
    {
        $class = $this->getTestClass();
        $this->assertSerializableValid( $class );
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerJsonSerializable()
    {
        $class = $this->getTestClass();
        $this->assertJsonSerializableValid( $class );
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerIterable()
    {
        $class = $this->getTestClass();
        $this->assertIterableValid( $class );
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerCountable()
    {
        $class = $this->getTestClass();
        $this->assertCountableValid( $class );
    }

    /**
     * Tests context matching for resizing, truncation of excess,
     * and disallowing overflow setting.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerResize()
    {
        $class = $this->getTestClass();
        $this->assertResizeKeyIntegrity( $class );
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerArrayAccess()
    {
        $class = $this->getTestClass();
        $this->assertArrayAccessValid( $class );
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerPsr11()
    {
        $class = $this->getTestClass();
        $this->assertPsr11Valid( $class );
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerSizeable()
    {
        $class = $this->getTestClass();
        $this->assertSizeableValid( $class );
    }

    /**
     * Tests context matching for valid parameters.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerPairs()
    {
        $class = $this->getTestClass();
        $this->assertPairsValid( $class );
    }

    /**
     * Tests equivalent interaction between arrays and object representation.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testArrayObjectInterchangable()
    {
        $class = $this->getTestClass();
        $this->assertArrayObjectSubstitution( $class );
    }

    /**
     * Tests key and value constraints for containers.
     * @group core
     * @group container
     * @covers \oroboros\core\traits\core\container\SetTrait
     * @covers \oroboros\core\traits\core\container\ConstrainedContainerTrait
     * @covers \oroboros\core\traits\core\container\ContainerTrait
     */
    public function testContainerConstraints()
    {
        $class = $this->getTestClass();
        $this->assertConstraintsValid( $class );
    }

}
