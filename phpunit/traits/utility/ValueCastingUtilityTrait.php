<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\traits\utility;

/**
 * <Oroboros Core Test Suite Value Casting Utility Trait>
 * Provides methods for insuring that assertion message parameters can resolve
 * against sprintf and interpolation, by returning a representation of a given
 * variable as a stringable result, regardless of whether or not it was
 * stringable to begin with.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category traits
 * @category unit-testing
 * @category utilities
 * @package oroboros/tests
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 */
trait ValueCastingUtilityTrait
{


    /**
     * Gets the type designation of the provided subject
     * @param type $subject
     * @return string
     */
    protected function castValueToString( $subject, $depth = 3 )
    {
        if ( $subject === false )
        {
            return 'boolean: false';
        } elseif ( $subject === true )
        {
            return 'boolean: true';
        } elseif ( $subject === null )
        {
            return 'null';
        } elseif ( is_array( $subject ) )
        {
            $msg = 'array: [ ';
            foreach ( $subject as
                $key =>
                $value )
            {
                $submsg = ( $depth <= 0
                    ? '...'
                    : $this->castValueToString( $value, $depth - 1 ));
                $msg .= $key . ': ' . $submsg;
            }
            $msg .= ' ] ';
            return $msg;
        }
        $received = ( is_object( $subject )
            ? 'object: ' . get_class( $subject )
            : gettype( $subject ) . (!is_resource( $subject )
            ? ': ' . (string) $subject
            : 'resource' ) );
        return $received;
    }

}
