<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\traits\assertions\context;

/**
 * <Oroboros Core CoreContext Evaluation Trait>
 * Provides assertions for insuring the integrity of automatic
 * package and subpackage declarations in contextual objects.
 *
 * Core context objects declare their package and sub-package
 * automatically from their api API_CODEX and API_SCOPE
 * class constants respectively, if those constants are defined and the
 * category and subcategory is not otherwise declared. The api interface
 * may be either directly or indirectly applied, so both cases must be
 * accounted for to resolve the default.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category traits
 * @category unit-testing
 * @category assertions
 * @package oroboros/tests
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 */
trait ContextualCoreEvaluationTrait
{
    /**
     * Core default class context packages are based on
     * the api interface API_CODEX class constant by default.
     *
     * This should be overridden if an alternate package
     * declaration applies.
     *
     * @return null|scalar
     */
    protected static function _declareExpectedContextualPackage()
    {
        $prior = parent::_declareExpectedContextualPackage();
        if ( !is_null( $prior ) )
        {
            //If a non-null context package is declared, use that.
            return $prior;
        }
        $class = get_called_class();
        $test_class = $class::TEST_CLASS;
        $const = $test_class . '::API_CODEX';
        $const_api = $test_class . '::OROBOROS_API';
        if ( !class_exists( $test_class ) || !( defined( $const ) || defined( $const_api ) ) )
        {
            //There is no valid definition
            return null;
        }
        if ( !defined( $const ) )
        {
            //The api may be declared indirectly
            $const = $const_api . '::API_CODEX';
            if ( !( is_string( $const_api ) && interface_exists( $const_api ) &&
                defined( $const ) ) )
            {
                //There is no valid definition
                return null;
            }
        }
        return constant( $const );
    }

    /**
     * Core default class context sub-packages are based on
     * the api interface API_SCOPE class constant by default.
     *
     * This should be overridden if an alternate sub-package
     * declaration applies.
     *
     * @return null|scalar
     */
    protected static function _declareExpectedContextualSubPackage()
    {
        $prior = parent::_declareExpectedContextualSubPackage();
        if ( !is_null( $prior ) )
        {
            //If a non-null context sub-package is declared, use that.
            return $prior;
        }
        $class = get_called_class();
        $test_class = $class::TEST_CLASS;
        $const = $test_class . '::API_SCOPE';
        $const_api = $test_class . '::OROBOROS_API';
        if ( !class_exists( $test_class ) || !( defined( $const ) || defined( $const_api ) ) )
        {
            //There is no valid definition
            return null;
        }
        if ( !defined( $const ) )
        {
            //The api may be declared indirectly
            $const = $const_api . '::API_SCOPE';
            if ( !( is_string( $const_api ) && interface_exists( $const_api ) &&
                defined( $const ) ) )
            {
                //There is no valid definition
                return null;
            }
        }
        return constant( $const );
    }
}
