<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\traits\assertions\context;

/**
 * <Oroboros Core CoreContext Evaluation Trait>
 * Provides assertions for insuring the integrity of efficient serialization and
 * unserialization in contextual objects.
 *
 * Core context objects declare their package and sub-package
 * automatically from their api API_CODEX and API_SCOPE
 * class constants respectively, if those constants are defined and the
 * category and subcategory is not otherwise declared. The api interface
 * may be either directly or indirectly applied, so both cases must be
 * accounted for to resolve the default.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category traits
 * @category unit-testing
 * @category assertions
 * @package oroboros/tests
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 */
trait ContextualSerialEvaluationTrait
{

    /**
     * Performs assertions to insure that the integrity of an
     * serialized object equals its original form.
     * @param \oroboros\core\interfaces\contract\core\context\SerialContextContract $class
     * @param type $message (optional) A fail message to pass.
     *     If not provided, a default fail message will be used.
     * @return void
     */
    protected function assertSerializableContextContractIntegrity( $class,
        $message = null )
    {
        $base_serial_contract = '\\oroboros\\core\\interfaces\\contract\\core\\context\\SerialContextContract';
        if ( is_null( $message ) )
        {
            $message = sprintf( 'Provided [%1$s] does not implement the base '
                . 'contextual serial interface [%2$s] at [%3$s] in [%4$s].',
                get_class( $class ), $base_serial_contract, __METHOD__,
                get_class( $this ) );
        }
        $this->assertInstanceOf( $base_serial_contract, $class, $message );
    }

    /**
     * Performs assertions to insure that the integrity of an
     * serialized object equals its original form.
     * @param \oroboros\core\interfaces\contract\core\context\SerialContextContract $class
     * @param type $message (optional) A fail message to pass.
     *     If not provided, a default fail message will be used.
     * @return void
     */
    protected function assertSerializableContextIntegrity( $class,
        $message = null )
    {
        try
        {
            $serial = serialize( $class );
            $unserial = unserialize( $serial );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do not suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertTrue( false,
                sprintf( 'Failed to restore serialized contextual for '
                    . 'original class for [%1$s] due to an exception of type [%2$s] with message '
                    . '[%3$s] in [%4$s] of test class [%5$s].',
                    get_class( $class ), get_class( $e ), $e->getMessage(),
                    __METHOD__, get_class( $this ) ) );
        }
        if ( is_null( $message ) )
        {
            $message = sprintf( 'Serialization must be equal to the '
                . 'original class for [%1$s] in [%2$s]', get_class( $class ),
                __METHOD__ );
        }
        $this->assertEquals( $class, $unserial, $message );
    }

    /**
     * Performs assertions to insure that the serialized
     * contextual type is restored correctly.
     * @param \oroboros\core\interfaces\contract\core\context\SerialContextContract $class
     * @param type $message (optional) A fail message to pass.
     *     If not provided, a default fail message will be used.
     * @return void
     */
    protected function assertSerializableContextTypeIntegrity( $class,
        $message = null )
    {
        try
        {
            $serial = serialize( $class );
            $unserial = unserialize( $serial );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do not suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertTrue( false,
                sprintf( 'Failed to restore serialized contextual for '
                    . 'original class for [%1$s] due to an exception of type [%2$s] with message '
                    . '[%3$s] in [%4$s] of test class [%5$s].',
                    get_class( $class ), get_class( $e ), $e->getMessage(),
                    __METHOD__, get_class( $this ) ) );
        }
        if ( is_null( $message ) )
        {
            $message = sprintf( 'Restored serialized contextual type must be equal to the '
                . 'original class for [%1$s] in [%2$s]', get_class( $class ),
                __METHOD__ );
        }
        $this->assertEquals( $class->getType(), $unserial->getType(), $message );
    }

    /**
     * Performs assertions to insure that the serialized
     * contextual sub-type is restored correctly.
     * @param \oroboros\core\interfaces\contract\core\context\SerialContextContract $class
     * @param type $message (optional) A fail message to pass.
     *     If not provided, a default fail message will be used.
     * @return void
     */
    protected function assertSerializableContextSubTypeIntegrity( $class,
        $message = null )
    {
        try
        {
            $serial = serialize( $class );
            $unserial = unserialize( $serial );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do not suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertTrue( false,
                sprintf( 'Failed to restore serialized contextual for '
                    . 'original class for [%1$s] due to an exception of type [%2$s] with message '
                    . '[%3$s] in [%4$s] of test class [%5$s].',
                    get_class( $class ), get_class( $e ), $e->getMessage(),
                    __METHOD__, get_class( $this ) ) );
        }
        if ( is_null( $message ) )
        {
            $message = sprintf( 'Restored serialized contextual sub-type must be equal to the '
                . 'original class for [%1$s] in [%2$s]', get_class( $class ),
                __METHOD__ );
        }
        $this->assertEquals( $class->getSubType(), $unserial->getSubType(),
            $message );
    }

    /**
     * Performs assertions to insure that the serialized
     * contextual context is restored correctly.
     * @param \oroboros\core\interfaces\contract\core\context\SerialContextContract $class
     * @param type $message (optional) A fail message to pass.
     *     If not provided, a default fail message will be used.
     * @return void
     */
    protected function assertSerializableContextContextIntegrity( $class,
        $message = null )
    {
        try
        {
            $serial = serialize( $class );
            $unserial = unserialize( $serial );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do not suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertTrue( false,
                sprintf( 'Failed to restore serialized contextual for '
                    . 'original class for [%1$s] due to an exception of type [%2$s] with message '
                    . '[%3$s] in [%4$s] of test class [%5$s].',
                    get_class( $class ), get_class( $e ), $e->getMessage(),
                    __METHOD__, get_class( $this ) ) );
        }
        if ( is_null( $message ) )
        {
            $message = sprintf( 'Restored serialized contextual context must be equal to the '
                . 'original class for [%1$s] in [%2$s]', get_class( $class ),
                __METHOD__ );
        }
        $this->assertEquals( $class->getContext(), $unserial->getContext(),
            $message );
    }

    /**
     * Performs assertions to insure that the serialized
     * contextual value is restored correctly.
     * @param \oroboros\core\interfaces\contract\core\context\SerialContextContract $class
     * @param type $message (optional) A fail message to pass.
     *     If not provided, a default fail message will be used.
     * @return void
     */
    protected function assertSerializableContextValueIntegrity( $class,
        $message = null )
    {
        try
        {
            $serial = serialize( $class );
            $unserial = unserialize( $serial );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do not suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertTrue( false,
                sprintf( 'Failed to restore serialized contextual for '
                    . 'original class for [%1$s] due to an exception of type [%2$s] with message '
                    . '[%3$s] in [%4$s] of test class [%5$s].',
                    get_class( $class ), get_class( $e ), $e->getMessage(),
                    __METHOD__, get_class( $this ) ) );
        }
        if ( is_null( $message ) )
        {
            $message = sprintf( 'Restored serialized contextual value must be equal to the '
                . 'original class for [%1$s] in [%2$s]', get_class( $class ),
                __METHOD__ );
        }
        $this->assertEquals( $class->getValue(), $unserial->getValue(), $message );
    }

    /**
     * Performs assertions to insure that the serialized
     * contextual category is restored correctly.
     * @param \oroboros\core\interfaces\contract\core\context\SerialContextContract $class
     * @param type $message (optional) A fail message to pass.
     *     If not provided, a default fail message will be used.
     * @return void
     */
    protected function assertSerializableContextCategoryIntegrity( $class,
        $message = null )
    {
        try
        {
            $serial = serialize( $class );
            $unserial = unserialize( $serial );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do not suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertTrue( false,
                sprintf( 'Failed to restore serialized contextual for '
                    . 'original class for [%1$s] due to an exception of type [%2$s] with message '
                    . '[%3$s] in [%4$s] of test class [%5$s].',
                    get_class( $class ), get_class( $e ), $e->getMessage(),
                    __METHOD__, get_class( $this ) ) );
        }
        if ( is_null( $message ) )
        {
            $message = sprintf( 'Restored serialized contextual category must be equal to the '
                . 'original class for [%1$s] in [%2$s]', get_class( $class ),
                __METHOD__ );
        }
        $this->assertEquals( $class->getCategory(), $unserial->getCategory(),
            $message );
    }

    /**
     * Performs assertions to insure that the serialized
     * contextual sub-category is restored correctly.
     * @param \oroboros\core\interfaces\contract\core\context\SerialContextContract $class
     * @param type $message (optional) A fail message to pass.
     *     If not provided, a default fail message will be used.
     * @return void
     */
    protected function assertSerializableContextSubCategoryIntegrity( $class,
        $message = null )
    {
        try
        {
            $serial = serialize( $class );
            $unserial = unserialize( $serial );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do not suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertTrue( false,
                sprintf( 'Failed to restore serialized contextual for '
                    . 'original class for [%1$s] due to an exception of type [%2$s] with message '
                    . '[%3$s] in [%4$s] of test class [%5$s].',
                    get_class( $class ), get_class( $e ), $e->getMessage(),
                    __METHOD__, get_class( $this ) ) );
        }
        if ( is_null( $message ) )
        {
            $message = sprintf( 'Restored serialized contextual sub-category must be equal to the '
                . 'original class for [%1$s] in [%2$s]', get_class( $class ),
                __METHOD__ );
        }
        $this->assertEquals( $class->getSubcategory(),
            $unserial->getSubcategory(), $message );
    }

    /**
     * Performs assertions to insure that the serialized
     * contextual package is restored correctly.
     * @param \oroboros\core\interfaces\contract\core\context\SerialContextContract $class
     * @param type $message (optional) A fail message to pass.
     *     If not provided, a default fail message will be used.
     * @return void
     */
    protected function assertSerializableContextPackageIntegrity( $class,
        $message = null )
    {
        try
        {
            $serial = serialize( $class );
            $unserial = unserialize( $serial );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do not suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertTrue( false,
                sprintf( 'Failed to restore serialized contextual for '
                    . 'original class for [%1$s] due to an exception of type [%2$s] with message '
                    . '[%3$s] in [%4$s] of test class [%5$s].',
                    get_class( $class ), get_class( $e ), $e->getMessage(),
                    __METHOD__, get_class( $this ) ) );
        }
        if ( is_null( $message ) )
        {
            $message = sprintf( 'Restored serialized contextual package must be equal to the '
                . 'original class for [%1$s] in [%2$s]', get_class( $class ),
                __METHOD__ );
        }
        $this->assertEquals( $class->getPackage(), $unserial->getPackage(),
            $message );
    }

    /**
     * Performs assertions to insure that the serialized
     * contextual sub-package is restored correctly.
     * @param \oroboros\core\interfaces\contract\core\context\SerialContextContract $class
     * @param type $message (optional) A fail message to pass.
     *     If not provided, a default fail message will be used.
     * @return void
     */
    protected function assertSerializableContextSubPackageIntegrity( $class,
        $message = null )
    {
        try
        {
            $serial = serialize( $class );
            $unserial = unserialize( $serial );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do not suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertTrue( false,
                sprintf( 'Failed to restore serialized contextual for '
                    . 'original class for [%1$s] due to an exception of type [%2$s] with message '
                    . '[%3$s] in [%4$s] of test class [%5$s].',
                    get_class( $class ), get_class( $e ), $e->getMessage(),
                    __METHOD__, get_class( $this ) ) );
        }
        if ( is_null( $message ) )
        {
            $message = sprintf( 'Restored serialized contextual package must be equal to the '
                . 'original class for [%1$s] in [%2$s]', get_class( $class ),
                __METHOD__ );
        }
        $this->assertEquals( $class->getSubPackage(),
            $unserial->getSubPackage(), $message );
    }

}
