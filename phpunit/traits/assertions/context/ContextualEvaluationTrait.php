<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\traits\assertions\context;

/**
 * <Oroboros Core Contextual Evaluation Trait>
 * Provides assertions for insuring the integrity of contextual objects.
 *
 * Most core engine functionality is driven by context objects,
 * and most baseline classes provided for use are also contextual.
 *
 * The integrity of the contextual object functionality is central to
 * stable Oroboros Core interaction. These assertions insure that inherited
 * integrity of contextual referencing is preserved as expected through all
 * levels of singular and multiple inheritance.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category traits
 * @category unit-testing
 * @category assertions
 * @package oroboros/tests
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 */
trait ContextualEvaluationTrait
{

    /**
     * Represents the expected contextual type.
     * If null, it will not be checked.
     * @var scalar
     */
    private static $_expected_contextual_type = null;

    /**
     * Represents the expected contextual subtype.
     * If null, it will not be checked.
     * @var scalar
     */
    private static $_expected_contextual_subtype = null;

    /**
     * Represents the expected contextual context.
     * If null, it will not be checked.
     * @var scalar
     */
    private static $_expected_contextual_context = null;

    /**
     * Represents the expected contextual value.
     * If null, it will not be checked.
     * @var scalar
     */
    private static $_expected_contextual_value = null;

    /**
     * Represents the expected contextual category.
     * If null, it will not be checked.
     * @var scalar
     */
    private static $_expected_contextual_category = null;

    /**
     * Represents the expected contextual sub-category.
     * If null, it will not be checked.
     * @var scalar
     */
    private static $_expected_contextual_subcategory = null;

    /**
     * Represents the expected contextual package.
     * If null, it will not be checked.
     * @var scalar
     */
    private static $_expected_contextual_package = null;

    /**
     * Represents the expected contextual subpackage.
     * If null, it will not be checked.
     * @var scalar
     */
    private static $_expected_contextual_subpackage = null;

    /**
     * Represents the default contextual type.
     * If null, it will not be checked.
     * @var scalar
     */
    private static $_default_contextual_type = null;

    /**
     * Represents the default contextual subtype.
     * If null, it will not be checked.
     * @var scalar
     */
    private static $_default_contextual_subtype = null;

    /**
     * Represents the default contextual context.
     * If null, it will not be checked.
     * @var scalar
     */
    private static $_default_contextual_context = null;

    /**
     * Represents the default contextual value.
     * If null, it will not be checked.
     * @var scalar
     */
    private static $_default_contextual_value = null;

    /**
     * Represents the default contextual category.
     * If null, it will not be checked.
     * @var scalar
     */
    private static $_default_contextual_category = null;

    /**
     * Represents the default contextual sub-category.
     * If null, it will not be checked.
     * @var scalar
     */
    private static $_default_contextual_subcategory = null;

    /**
     * Represents the default contextual package.
     * If null, it will not be checked.
     * @var scalar
     */
    private static $_default_contextual_package = null;

    /**
     * Represents the default contextual subpackage.
     * If null, it will not be checked.
     * @var scalar
     */
    private static $_default_contextual_subpackage = null;

    /**
     * Represents the base contextual contract interface that
     * all contextual objects descend from.
     * @var \oroboros\core\interfaces\contract\core\context\ContextualContract
     */
    private $_base_contextual_contract = '\\oroboros\\core\\interfaces\\contract\\core\\context\\ContextualContract';

    /**
     * <Contextual Contract Evaluation Method>
     * Evaluates that the basic contextual contract interface is implemented.
     *
     * @param object|string $class A reference to any object
     *     or class from the core that is not a core internal
     * @return void
     */
    protected function assertContextualContractValid( $class, $message = null )
    {
        if ( !is_object( $class ) )
        {
            throw new \PHPUnit\Framework\Error\Error( sprintf(
                'Only objects can be contextually evaluated. Received [%1$s] in test class [%2$s] at [%3$s].',
                $this->castValueToString( $class ), get_class( $this ),
                __METHOD__
            ), 0, __FILE__, __LINE__ );
        }
        $contract = $this->getExpectedClassContract();
        if ( !$contract
            || !interface_exists( $contract )
            || !($contract === $this->_base_contextual_contract
            || is_subclass_of( $contract, $this->_base_contextual_contract ))
        )
        {
            //Do not perform assertations if the defined contract
            //is not set, valid, and an instance of contextual.
            return;
        }
        $classname = get_class( $class );
        if ( is_null( $message ) )
        {
            $message = sprintf( 'Failed to assert that [%1$s] honors expected contextual contract '
                . 'interface [%2$s] at [%3$s] in test class [%4$s]', $classname,
                $contract, __METHOD__, get_class( $this ) );
        }
        $this->assertInstanceOf( $contract, $class, $message );
    }

    /**
     * <Contextual Type Evaluation Method>
     * Evaluates that the basic contextual type matches the expected type.
     *
     * @param \oroboros\core\interfaces\contract\core\context\ContextualContract
     * @return void
     */
    protected function assertContextualTypeValid( $class, $message = null )
    {
        $expected = $this->getExpectedContextualType();
        if ( is_null( $expected ) )
        {
            //Do not assert a null context type.
            return;
        }
        $classname = get_class( $class );
        $this->assertContextualContractValid( $class,
            sprintf( 'Failed to assert that [%1$s] has a contextually valid type '
                . 'because it does not correctly implement the contextual contract '
                . '[%2$s] honors expected contextual contract '
                . 'interface [%3$s] at [%4$s] in test class [%5$s]', $classname,
                $this->getExpectedClassContract(),
                $this->_base_contextual_contract, __METHOD__, get_class( $this ) ) );
        if ( is_null( $message ) )
        {
            $message = sprintf( 'Failed to assert that [%1$s] with context type '
                . '[%2$s] has expected context type [%3$s], using [getType()] '
                . 'in test class [%4$s] at line [%5$s] of [%6$s].', $classname,
                $this->castValueToString( $class->getType() ),
                $this->castValueToString( $expected ), get_class( $this ),
                __LINE__, __METHOD__ );
        }
        $this->assertTrue( $class->hasType( $expected ), $message );
        $this->assertEquals( $expected, $class->getType(), $message );
    }

    /**
     * <Contextual SubType Evaluation Method>
     * Evaluates that the basic contextual sub-type matches the expected sub-type.
     *
     * @param \oroboros\core\interfaces\contract\core\context\ContextualContract
     * @return void
     */
    protected function assertContextualSubTypeValid( $class, $message = null )
    {
        $expected = $this->getExpectedContextualSubType();
        if ( is_null( $expected ) )
        {
            //Do not assert a null context sub-type.
            return;
        }
        $classname = get_class( $class );
        $this->assertContextualContractValid( $class,
            sprintf( 'Failed to assert that [%1$s] has a contextually valid sub-type '
                . 'because it does not correctly implement the contextual contract '
                . '[%2$s] honors expected contextual contract '
                . 'interface [%3$s] at [%4$s] in test class [%5$s]', $classname,
                $this->getExpectedClassContract(),
                $this->_base_contextual_contract, __METHOD__, get_class( $this ) ) );
        if ( is_null( $message ) )
        {
            $message = sprintf( 'Failed to assert that [%1$s] with context sub-type '
                . '[%2$s] has expected context sub-type [%3$s], using [getSubType()] '
                . 'in test class [%4$s] at line [%5$s] of [%6$s].', $classname,
                $this->castValueToString( $class->getSubType() ),
                $this->castValueToString( $expected ), get_class( $this ),
                __LINE__, __METHOD__ );
        }
        $this->assertTrue( $class->hasSubType( $expected ), $message );
        $this->assertEquals( $expected, $class->getSubType(), $message );
    }

    /**
     * <Contextual Context Evaluation Method>
     * Evaluates that the basic contextual context matches the expected context.
     *
     * @param \oroboros\core\interfaces\contract\core\context\ContextualContract
     * @return void
     */
    protected function assertContextualContextValid( $class, $message = null )
    {
        $expected = $this->getExpectedContextualContext();
        if ( is_null( $expected ) )
        {
            //Do not assert a null context context.
            return;
        }
        $classname = get_class( $class );
        $this->assertContextualContractValid( $class,
            sprintf( 'Failed to assert that [%1$s] has a contextually valid context '
                . 'because it does not correctly implement the contextual contract '
                . '[%2$s] honors expected contextual contract '
                . 'interface [%3$s] at [%4$s] in test class [%5$s]', $classname,
                $this->getExpectedClassContract(),
                $this->_base_contextual_contract, __METHOD__, get_class( $this ) ) );
        if ( is_null( $message ) )
        {
            $message = sprintf( 'Failed to assert that [%1$s] with context context '
                . '[%2$s] has expected context context [%3$s], using [getContext()] '
                . 'in test class [%4$s] at line [%5$s] of [%6$s].', $classname,
                $this->castValueToString( $class->getContext() ),
                $this->castValueToString( $expected ), get_class( $this ),
                __LINE__, __METHOD__ );
        }
        $this->assertTrue( $class->hasContext( $expected ), $message );
        $this->assertEquals( $expected, $class->getContext(), $message );
    }

    /**
     * <Contextual Value Equal Evaluation Method>
     * Evaluates that the basic contextual value matches the expected value exactly.
     *
     * @param \oroboros\core\interfaces\contract\core\context\ContextualContract
     * @return void
     */
    protected function assertContextualValueEqualsExpected( $class,
        $message = null )
    {
        $expected = $this->getExpectedContextualValue();
        if ( is_null( $expected ) )
        {
            //Do not assert a null context value.
            return;
        }
        $classname = get_class( $class );
        $this->assertContextualContractValid( $class,
            sprintf( 'Failed to assert that [%1$s] has a contextually valid value '
                . 'because it does not correctly implement the contextual contract '
                . '[%2$s] honors expected contextual contract '
                . 'interface [%3$s] at [%4$s] in test class [%5$s]', $classname,
                $this->getExpectedClassContract(),
                $this->_base_contextual_contract, __METHOD__, get_class( $this ) ) );
        if ( is_null( $message ) )
        {
            $message = sprintf( 'Failed to assert that [%1$s] with context value '
                . '[%2$s] equals expected context value [%3$s], using [getValue()] '
                . 'in test class [%4$s] at line [%5$s] of [%6$s].', $classname,
                $this->castValueToString( $class->getValue() ),
                $this->castValueToString( $expected ), get_class( $this ),
                __LINE__, __METHOD__ );
        }
        $this->assertTrue( $class->hasValue( $expected ), $message );
        $this->assertEquals( $expected, $class->getValue(), $message );
    }

    /**
     * <Contextual Category Evaluation Method>
     * Evaluates that the basic contextual category matches the expected category.
     *
     * @param \oroboros\core\interfaces\contract\core\context\ContextualContract
     * @return void
     */
    protected function assertContextualCategoryValid( $class, $message = null )
    {
        $expected = $this->getExpectedContextualCategory();
        if ( is_null( $expected ) )
        {
            //Do not assert a null context category.
            return;
        }
        $classname = get_class( $class );
        $this->assertContextualContractValid( $class,
            sprintf( 'Failed to assert that [%1$s] has a contextually valid category '
                . 'because it does not correctly implement the contextual contract '
                . '[%2$s] honors expected contextual contract '
                . 'interface [%3$s] at [%4$s] in test class [%5$s]', $classname,
                $this->getExpectedClassContract(),
                $this->_base_contextual_contract, __METHOD__, get_class( $this ) ) );
        if ( is_null( $message ) )
        {
            $message = sprintf( 'Failed to assert that [%1$s] with context category '
                . '[%2$s] has expected context category [%3$s], using [getCategory()] '
                . 'in test class [%4$s] at line [%5$s] of [%6$s].', $classname,
                $this->castValueToString( $class->getCategory() ),
                $this->castValueToString( $expected ), get_class( $this ),
                __LINE__, __METHOD__ );
        }
        $this->assertTrue( $class->hasCategory( $expected ), $message );
        $this->assertEquals( $expected, $class->getCategory(), $message );
    }

    /**
     * <Contextual SubCategory Evaluation Method>
     * Evaluates that the basic contextual sub-category matches the expected sub-category.
     *
     * @param \oroboros\core\interfaces\contract\core\context\ContextualContract
     * @return void
     */
    protected function assertContextualSubCategoryValid( $class, $message = null )
    {
        $expected = $this->getExpectedContextualSubCategory();
        if ( is_null( $expected ) )
        {
            //Do not assert a null context sub-category.
            return;
        }
        $classname = get_class( $class );
        $this->assertContextualContractValid( $class,
            sprintf( 'Failed to assert that [%1$s] has a contextually valid sub-category '
                . 'because it does not correctly implement the contextual contract '
                . '[%2$s] honors expected contextual contract '
                . 'interface [%3$s] at [%4$s] in test class [%5$s]', $classname,
                $this->getExpectedClassContract(),
                $this->_base_contextual_contract, __METHOD__, get_class( $this ) ) );
        if ( is_null( $message ) )
        {
            $message = sprintf( 'Failed to assert that [%1$s] with context sub-category '
                . '[%2$s] has expected context sub-category [%3$s], using [getSubCategory()] '
                . 'in test class [%4$s] at line [%5$s] of [%6$s].', $classname,
                $this->castValueToString( $class->getSubCategory() ),
                $this->castValueToString( $expected ), get_class( $this ),
                __LINE__, __METHOD__ );
        }
        $this->assertTrue( $class->hasSubCategory( $expected ), $message );
        $this->assertEquals( $expected, $class->getSubCategory(), $message );
    }

    /**
     * <Contextual Package Evaluation Method>
     * Evaluates that the basic contextual package matches the expected package.
     *
     * @param \oroboros\core\interfaces\contract\core\context\ContextualContract
     * @return void
     */
    protected function assertContextualPackageValid( $class, $message = null )
    {
        $expected = $this->getExpectedContextualPackage();
        if ( is_null( $expected ) )
        {
            //Do not assert a null context package.
            return;
        }
        $classname = get_class( $class );
        $this->assertContextualContractValid( $class,
            sprintf( 'Failed to assert that [%1$s] has a contextually valid package '
                . 'because it does not correctly implement the contextual contract '
                . '[%2$s] honors expected contextual contract '
                . 'interface [%3$s] at [%4$s] in test class [%5$s]', $classname,
                $this->getExpectedClassContract(),
                $this->_base_contextual_contract, __METHOD__, get_class( $this ) ) );
        if ( is_null( $message ) )
        {
            $message = sprintf( 'Failed to assert that [%1$s] with context package '
                . '[%2$s] has expected context package [%3$s], using [getPackage()] '
                . 'in test class [%4$s] at line [%5$s] of [%6$s].', $classname,
                $this->castValueToString( $class->getPackage() ),
                $this->castValueToString( $expected ), get_class( $this ),
                __LINE__, __METHOD__ );
        }
        $this->assertTrue( $class->hasPackage( $expected ), $message );
        $this->assertEquals( $expected, $class->getPackage(), $message );
    }

    /**
     * <Contextual SubPackage Evaluation Method>
     * Evaluates that the basic contextual sub-package matches the expected sub-package.
     *
     * @param \oroboros\core\interfaces\contract\core\context\ContextualContract
     * @return void
     */
    protected function assertContextualSubPackageValid( $class, $message = null )
    {
        $expected = $this->getExpectedContextualSubPackage();
        if ( is_null( $expected ) )
        {
            //Do not assert a null context sub-package.
            return;
        }
        $classname = get_class( $class );
        $this->assertContextualContractValid( $class,
            sprintf( 'Failed to assert that [%1$s] has a contextually valid sub-package '
                . 'because it does not correctly implement the contextual contract '
                . '[%2$s] honors expected contextual contract '
                . 'interface [%3$s] at [%4$s] in test class [%5$s]', $classname,
                $this->getExpectedClassContract(),
                $this->_base_contextual_contract, __METHOD__, get_class( $this ) ) );
        if ( is_null( $message ) )
        {
            $message = sprintf( 'Failed to assert that [%1$s] with context sub-package '
                . '[%2$s] has expected context sub-package [%3$s], using [getSubPackage()] '
                . 'in test class [%4$s] at line [%5$s] of [%6$s].', $classname,
                $this->castValueToString( $class->getSubPackage() ),
                $this->castValueToString( $expected ), get_class( $this ),
                __LINE__, __METHOD__ );
        }
        $this->assertTrue( $class->hasSubPackage( $expected ), $message );
        $this->assertEquals( $expected, $class->getSubPackage(), $message );
    }

    /**
     * <Contextual Meta Index Evaluation Method>
     * Evaluates that the meta index casting is valid.
     *
     * @param \oroboros\core\interfaces\contract\core\context\ContextualContract
     * @return void
     */
    protected function assertContextualMetaIndexValid( $class )
    {
        $classname = get_class( $class );
        try
        {
            //Meta-index MUST always be valid for contextual objects.
            //This test does not skip under any conditions.
            $meta = $class->getMetaIndex();
            $meta_array = $meta->toArray();
            $this->assertArrayHasKey(
                \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_TYPE_META,
                $meta_array,
                sprintf( 'Failed to assert that instance of [%1$s] extracted '
                    . 'from [%2$s] produced expected meta key [%3$s] at '
                    . '[%4$s] of test class [%5$s].', get_class( $meta ),
                $classname,
                \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_TYPE_META,
                __METHOD__, get_class( $this ) ) );
            $this->assertEquals( $meta->get( \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_TYPE_META )->getValue(),
                $class->getType(),
                sprintf( 'Failed to assert that meta type property of [%1$s] equals result of getType() '
                    . 'in [%2$s] at [%3$s] of test class [%4$s].',
                get_class( $meta ), $classname, __METHOD__, get_class( $this ) ) );
            $this->assertArrayHasKey(
                \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_SUBTYPE_META,
                $meta_array,
                sprintf( 'Failed to assert that instance of [%1$s] extracted '
                    . 'from [%2$s] produced expected meta key [%3$s] at '
                    . '[%4$s] of test class [%5$s].', get_class( $meta ),
                $classname,
                \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_SUBTYPE_META,
                __METHOD__, get_class( $this ) ) );
            $this->assertEquals( $meta->get( \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_SUBTYPE_META )->getValue(),
                $class->getSubType(),
                sprintf( 'Failed to assert that meta sub-type property of [%1$s] equals result of getSubType() '
                    . 'in [%2$s] at [%3$s] of test class [%4$s].',
                get_class( $meta ), $classname, __METHOD__, get_class( $this ) ) );
            $this->assertArrayHasKey(
                \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_CONTEXT_META,
                $meta_array,
                sprintf( 'Failed to assert that instance of [%1$s] extracted '
                    . 'from [%2$s] produced expected meta key [%3$s] at '
                    . '[%4$s] of test class [%5$s].', get_class( $meta ),
                $classname,
                \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_CONTEXT_META,
                __METHOD__, get_class( $this ) ) );
            $this->assertEquals( $meta->get( \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_CONTEXT_META )->getValue(),
                $class->getContext(),
                sprintf( 'Failed to assert that meta context property of [%1$s] equals result of getContext() '
                    . 'in [%2$s] at [%3$s] of test class [%4$s].',
                get_class( $meta ), $classname, __METHOD__, get_class( $this ) ) );
            $this->assertArrayHasKey(
                \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_VALUE_META,
                $meta_array,
                sprintf( 'Failed to assert that instance of [%1$s] extracted '
                    . 'from [%2$s] produced expected meta key [%3$s] at '
                    . '[%4$s] of test class [%5$s].', get_class( $meta ),
                $classname,
                \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_VALUE_META,
                __METHOD__, get_class( $this ) ) );
            $this->assertEquals( $meta->get( \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_VALUE_META )->getValue(),
                $class->getValue(),
                sprintf( 'Failed to assert that meta value property of [%1$s] equals result of getValue() '
                    . 'in [%2$s] at [%3$s] of test class [%4$s].',
                get_class( $meta ), $classname, __METHOD__, get_class( $this ) ) );
            $this->assertArrayHasKey(
                \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_CATEGORY_META,
                $meta_array,
                sprintf( 'Failed to assert that instance of [%1$s] extracted '
                    . 'from [%2$s] produced expected meta key [%3$s] at '
                    . '[%4$s] of test class [%5$s].', get_class( $meta ),
                $classname,
                \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_CATEGORY_META,
                __METHOD__, get_class( $this ) ) );
            $this->assertEquals( $meta->get( \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_CATEGORY_META )->getValue(),
                $class->getCategory(),
                sprintf( 'Failed to assert that meta category property of [%1$s] equals result of getCategory() '
                    . 'in [%2$s] at [%3$s] of test class [%4$s].',
                get_class( $meta ), $classname, __METHOD__, get_class( $this ) ) );
            $this->assertArrayHasKey(
                \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_SUBCATEGORY_META,
                $meta_array,
                sprintf( 'Failed to assert that instance of [%1$s] extracted '
                    . 'from [%2$s] produced expected meta key [%3$s] at '
                    . '[%4$s] of test class [%5$s].', get_class( $meta ),
                $classname,
                \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_SUBCATEGORY_META,
                __METHOD__, get_class( $this ) ) );
            $this->assertEquals( $meta->get( \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_SUBCATEGORY_META )->getValue(),
                $class->getSubCategory(),
                sprintf( 'Failed to assert that meta sub-category property of [%1$s] equals result of getSubCategory() '
                    . 'in [%2$s] at [%3$s] of test class [%4$s].',
                get_class( $meta ), $classname, __METHOD__, get_class( $this ) ) );
            $this->assertArrayHasKey(
                \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_PACKAGE_META,
                $meta_array,
                sprintf( 'Failed to assert that instance of [%1$s] extracted '
                    . 'from [%2$s] produced expected meta key [%3$s] at '
                    . '[%4$s] of test class [%5$s].', get_class( $meta ),
                $classname,
                \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_PACKAGE_META,
                __METHOD__, get_class( $this ) ) );
            $this->assertEquals( $meta->get( \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_PACKAGE_META )->getValue(),
                $class->getPackage(),
                sprintf( 'Failed to assert that meta package property of [%1$s] equals result of getPackage() '
                    . 'in [%2$s] at [%3$s] of test class [%4$s].',
                get_class( $meta ), $classname, __METHOD__, get_class( $this ) ) );
            $this->assertArrayHasKey(
                \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_SUBPACKAGE_META,
                $meta_array,
                sprintf( 'Failed to assert that instance of [%1$s] extracted '
                    . 'from [%2$s] produced expected meta key [%3$s] at '
                    . '[%4$s] of test class [%5$s].', get_class( $meta ),
                $classname,
                \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_SUBPACKAGE_META,
                __METHOD__, get_class( $this ) ) );
            $this->assertEquals( $meta->get( \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_SUBPACKAGE_META )->getValue(),
                $class->getSubPackage(),
                sprintf( 'Failed to assert that meta sub-package property of [%1$s] equals result of getSubPackage() '
                    . 'in [%2$s] at [%3$s] of test class [%4$s].',
                get_class( $meta ), $classname, __METHOD__, get_class( $this ) ) );
        } catch ( \PHPUnit\Exception $e )
        {
            //Do not suppress testing framework exceptions
            throw $e;
        } catch ( \Exception $e )
        {
            $this->assertTrue( false,
                sprintf( 'Error encountered obtaining '
                    . 'meta index for [%1$s]. Encountered exception [%2$s] with '
                    . 'message [%3$s] at [%4$s] in test class [%5$s], with backtrace [%6$s]',
                    $classname, get_class($e),
                    $e->getMessage(), __METHOD__,
                    get_class( $this ), $e->getTraceAsString() ) );
        }
    }

    /**
     * Returns the expected contextual type,
     * as declared in `bootstrapExpectedContextualType`.
     * @return null|scalar
     */
    protected function getExpectedContextualType()
    {
        return self::$_expected_contextual_type;
    }

    /**
     * Returns the expected contextual sub-type,
     * as declared in `bootstrapExpectedContextualSubType`.
     * @return null|scalar
     */
    protected function getExpectedContextualSubtype()
    {
        return self::$_expected_contextual_subtype;
    }

    /**
     * Returns the expected contextual context,
     * as declared in `bootstrapExpectedContextualContext`.
     * @return null|scalar
     */
    protected function getExpectedContextualContext()
    {
        return self::$_expected_contextual_context;
    }

    /**
     * Returns the expected contextual value,
     * as declared in `bootstrapExpectedContextualValue`.
     * @return null|scalar
     */
    protected function getExpectedContextualValue()
    {
        return self::$_expected_contextual_value;
    }

    /**
     * Returns the expected contextual category,
     * as declared in `bootstrapExpectedContextualCategory`.
     * @return null|scalar
     */
    protected function getExpectedContextualCategory()
    {
        return self::$_expected_contextual_category;
    }

    /**
     * Returns the expected contextual sub-category,
     * as declared in `bootstrapExpectedContextualSubCategory`.
     * @return null|scalar
     */
    protected function getExpectedContextualSubCategory()
    {
        return self::$_expected_contextual_subcategory;
    }

    /**
     * Returns the expected contextual package,
     * as declared in `bootstrapExpectedContextualPackage`.
     * @return null|scalar
     */
    protected function getExpectedContextualPackage()
    {
        return self::$_expected_contextual_package;
    }

    /**
     * Returns the expected contextual sub-package,
     * as declared in `bootstrapExpectedContextualSubPackage`.
     * @return null|scalar
     */
    protected function getExpectedContextualSubPackage()
    {
        return self::$_expected_contextual_subpackage;
    }

    /**
     * Override this method to declare an expected
     * contextual type for the test class.
     * @return null|scalar
     */
    protected static function _declareExpectedContextualType()
    {
        $class = get_called_class();
        $const = $class . '::TEST_CLASS_EXPECTED_CONTEXTUAL_TYPE';
        if ( !defined( $const ) )
        {
            return null;
        }
        return constant( $const );
    }

    /**
     * Declares the expected contextual type as a shared PHPUnit resource.
     * @beforeClass
     */
    public static function bootstrapExpectedContextualType()
    {
        $class = get_called_class();
        self::$_expected_contextual_type = $class::_declareExpectedContextualType();
    }

    /**
     * Resets the expected contextual type to null.
     * @afterClass
     */
    public static function teardownExpectedContextualType()
    {
        self::$_expected_contextual_type = null;
    }

    /**
     * Override this method to declare an expected
     * contextual sub-type for the test class.
     * @return null|scalar
     */
    protected static function _declareExpectedContextualSubType()
    {
        $class = get_called_class();
        $const = $class . '::TEST_CLASS_EXPECTED_CONTEXTUAL_SUBTYPE';
        if ( !defined( $const ) )
        {
            return null;
        }
        return constant( $const );
    }

    /**
     * Declares the expected contextual sub-type as a shared PHPUnit resource.
     * @beforeClass
     */
    public static function bootstrapExpectedContextualSubType()
    {
        $class = get_called_class();
        self::$_expected_contextual_subtype = $class::_declareExpectedContextualSubType();
    }

    /**
     * Resets the expected contextual sub-type to null.
     * @afterClass
     */
    public static function teardownExpectedContextualSubType()
    {
        self::$_expected_contextual_subtype = null;
    }

    /**
     * Override this method to declare an expected
     * contextual context for the test class.
     * @return null|scalar
     */
    protected static function _declareExpectedContextualContext()
    {
        $class = get_called_class();
        $const = $class . '::TEST_CLASS_EXPECTED_CONTEXTUAL_CONTEXT';
        if ( !defined( $const ) )
        {
            return null;
        }
        return constant( $const );
    }

    /**
     * Declares the expected contextual context as a shared PHPUnit resource.
     * @beforeClass
     */
    public static function bootstrapExpectedContextualContext()
    {
        $class = get_called_class();
        self::$_expected_contextual_context = $class::_declareExpectedContextualContext();
    }

    /**
     * Resets the expected contextual context to null.
     * @afterClass
     */
    public static function teardownExpectedContextualContext()
    {
        self::$_expected_contextual_context = null;
    }

    /**
     * Override this method to declare an expected
     * contextual value for the test class.
     * @return null|scalar
     */
    protected static function _declareExpectedContextualValue()
    {
        $class = get_called_class();
        $const = $class . '::TEST_CLASS_EXPECTED_CONTEXTUAL_VALUE';
        if ( !defined( $const ) )
        {
            return null;
        }
        return constant( $const );
    }

    /**
     * Declares the expected contextual value as a shared PHPUnit resource.
     * @beforeClass
     */
    public static function bootstrapExpectedContextualValue()
    {
        $class = get_called_class();
        self::$_expected_contextual_value = $class::_declareExpectedContextualValue();
    }

    /**
     * Resets the expected contextual value to null.
     * @afterClass
     */
    public static function teardownExpectedContextualValue()
    {
        self::$_expected_contextual_value = null;
    }

    /**
     * Override this method to declare an expected
     * contextual category for the test class.
     * @return null|scalar
     */
    protected static function _declareExpectedContextualCategory()
    {
        $class = get_called_class();
        $const = $class . '::TEST_CLASS_EXPECTED_CONTEXTUAL_CATEGORY';
        if ( !defined( $const ) )
        {
            return null;
        }
        return constant( $const );
    }

    /**
     * Declares the expected contextual category as a shared PHPUnit resource.
     * @beforeClass
     */
    public static function bootstrapExpectedContextualCategory()
    {
        $class = get_called_class();
        self::$_expected_contextual_category = $class::_declareExpectedContextualCategory();
    }

    /**
     * Resets the expected contextual category to null.
     * @afterClass
     */
    public static function teardownExpectedContextualCategory()
    {
        self::$_expected_contextual_category = null;
    }

    /**
     * Override this method to declare an expected
     * contextual sub-category for the test class.
     * @return null|scalar
     */
    protected static function _declareExpectedContextualSubCategory()
    {
        $class = get_called_class();
        $const = $class . '::TEST_CLASS_EXPECTED_CONTEXTUAL_SUBCATEGORY';
        if ( !defined( $const ) )
        {
            return null;
        }
        return constant( $const );
    }

    /**
     * Declares the expected contextual sub-category as a shared PHPUnit resource.
     * @beforeClass
     */
    public static function bootstrapExpectedContextualSubCategory()
    {
        $class = get_called_class();
        self::$_expected_contextual_subcategory = $class::_declareExpectedContextualSubCategory();
    }

    /**
     * Resets the expected contextual sub-category to null.
     * @afterClass
     */
    public static function teardownExpectedContextualSubCategory()
    {
        self::$_expected_contextual_subcategory = null;
    }

    /**
     * Override this method to declare an expected
     * contextual package for the test class.
     * @return null|scalar
     */
    protected static function _declareExpectedContextualPackage()
    {
        $class = get_called_class();
        $const = $class . '::TEST_CLASS_EXPECTED_CONTEXTUAL_PACKAGE';
        if ( !defined( $const ) )
        {
            return null;
        }
        return constant( $const );
    }

    /**
     * Declares the expected contextual package as a shared PHPUnit resource.
     * @beforeClass
     */
    public static function bootstrapExpectedContextualPackage()
    {
        $class = get_called_class();
        self::$_expected_contextual_package = $class::_declareExpectedContextualPackage();
    }

    /**
     * Resets the expected contextual package to null.
     * @afterClass
     */
    public static function teardownExpectedContextualPackage()
    {
        self::$_expected_contextual_package = null;
    }

    /**
     * Override this method to declare an expected
     * contextual sub-package for the test class.
     * @return null|scalar
     */
    protected static function _declareExpectedContextualSubPackage()
    {
        $class = get_called_class();
        $const = $class . '::TEST_CLASS_EXPECTED_CONTEXTUAL_SUBPACKAGE';
        if ( !defined( $const ) )
        {
            return null;
        }
        return constant( $const );
    }

    /**
     * Declares the expected contextual sub-package as a shared PHPUnit resource.
     * @beforeClass
     */
    public static function bootstrapExpectedContextualSubPackage()
    {
        $class = get_called_class();
        self::$_expected_contextual_subpackage = $class::_declareExpectedContextualSubPackage();
    }

    /**
     * Resets the expected contextual sub-package to null.
     * @afterClass
     */
    public static function teardownExpectedContextualSubPackage()
    {
        self::$_expected_contextual_subpackage = null;
    }

    /**
     * Returns the default contextual type,
     * as declared in `bootstrapDefaultContextualType`.
     * @return null|scalar
     */
    protected function getDefaultContextualType()
    {
        return self::$_default_contextual_type;
    }

    /**
     * Returns the default contextual sub-type,
     * as declared in `bootstrapDefaultContextualSubType`.
     * @return null|scalar
     */
    protected function getDefaultContextualSubtype()
    {
        return self::$_default_contextual_subtype;
    }

    /**
     * Returns the default contextual context,
     * as declared in `bootstrapDefaultContextualContext`.
     * @return null|scalar
     */
    protected function getDefaultContextualContext()
    {
        return self::$_default_contextual_context;
    }

    /**
     * Returns the default contextual value,
     * as declared in `bootstrapDefaultContextualValue`.
     * @return null|scalar
     */
    protected function getDefaultContextualValue()
    {
        return self::$_default_contextual_value;
    }

    /**
     * Returns the default contextual category,
     * as declared in `bootstrapDefaultContextualCategory`.
     * @return null|scalar
     */
    protected function getDefaultContextualCategory()
    {
        return self::$_default_contextual_category;
    }

    /**
     * Returns the default contextual sub-category,
     * as declared in `bootstrapDefaultContextualSubCategory`.
     * @return null|scalar
     */
    protected function getDefaultContextualSubCategory()
    {
        return self::$_default_contextual_subcategory;
    }

    /**
     * Returns the default contextual package,
     * as declared in `bootstrapDefaultContextualPackage`.
     * @return null|scalar
     */
    protected function getDefaultContextualPackage()
    {
        return self::$_default_contextual_package;
    }

    /**
     * Returns the default contextual sub-package,
     * as declared in `bootstrapDefaultContextualSubPackage`.
     * @return null|scalar
     */
    protected function getDefaultContextualSubPackage()
    {
        return self::$_default_contextual_subpackage;
    }

    /**
     * Override this method to declare an default
     * contextual type for the test class.
     * @return null|scalar
     */
    protected static function _declareDefaultContextualType()
    {
        $class = get_called_class();
        $const = $class . '::TEST_CLASS_DEFAULT_CONTEXTUAL_TYPE';
        if ( !defined( $const ) )
        {
            return null;
        }
        return constant( $const );
    }

    /**
     * Declares the default contextual type as a shared PHPUnit resource.
     * @beforeClass
     */
    public static function bootstrapDefaultContextualType()
    {
        $class = get_called_class();
        self::$_default_contextual_type = $class::_declareDefaultContextualType();
    }

    /**
     * Resets the default contextual type to null.
     * @afterClass
     */
    public static function teardownDefaultContextualType()
    {
        self::$_default_contextual_type = null;
    }

    /**
     * Override this method to declare an default
     * contextual sub-type for the test class.
     * @return null|scalar
     */
    protected static function _declareDefaultContextualSubType()
    {
        $class = get_called_class();
        $const = $class . '::TEST_CLASS_DEFAULT_CONTEXTUAL_SUBTYPE';
        if ( !defined( $const ) )
        {
            return null;
        }
        return constant( $const );
    }

    /**
     * Declares the default contextual sub-type as a shared PHPUnit resource.
     * @beforeClass
     */
    public static function bootstrapDefaultContextualSubType()
    {
        $class = get_called_class();
        self::$_default_contextual_subtype = $class::_declareDefaultContextualSubType();
    }

    /**
     * Resets the default contextual sub-type to null.
     * @afterClass
     */
    public static function teardownDefaultContextualSubType()
    {
        self::$_default_contextual_subtype = null;
    }

    /**
     * Override this method to declare an default
     * contextual context for the test class.
     * @return null|scalar
     */
    protected static function _declareDefaultContextualContext()
    {
        $class = get_called_class();
        $const = $class . '::TEST_CLASS_DEFAULT_CONTEXTUAL_CONTEXT';
        if ( !defined( $const ) )
        {
            return null;
        }
        return constant( $const );
    }

    /**
     * Declares the default contextual context as a shared PHPUnit resource.
     * @beforeClass
     */
    public static function bootstrapDefaultContextualContext()
    {
        $class = get_called_class();
        self::$_default_contextual_context = $class::_declareDefaultContextualContext();
    }

    /**
     * Resets the default contextual context to null.
     * @afterClass
     */
    public static function teardownDefaultContextualContext()
    {
        self::$_default_contextual_context = null;
    }

    /**
     * Override this method to declare an default
     * contextual value for the test class.
     * @return null|scalar
     */
    protected static function _declareDefaultContextualValue()
    {
        $class = get_called_class();
        $const = $class . '::TEST_CLASS_DEFAULT_CONTEXTUAL_VALUE';
        if ( !defined( $const ) )
        {
            return null;
        }
        return constant( $const );
    }

    /**
     * Declares the default contextual value as a shared PHPUnit resource.
     * @beforeClass
     */
    public static function bootstrapDefaultContextualValue()
    {
        $class = get_called_class();
        self::$_default_contextual_value = $class::_declareDefaultContextualValue();
    }

    /**
     * Resets the default contextual value to null.
     * @afterClass
     */
    public static function teardownDefaultContextualValue()
    {
        self::$_default_contextual_value = null;
    }

    /**
     * Override this method to declare an default
     * contextual category for the test class.
     * @return null|scalar
     */
    protected static function _declareDefaultContextualCategory()
    {
        $class = get_called_class();
        $const = $class . '::TEST_CLASS_DEFAULT_CONTEXTUAL_CATEGORY';
        if ( !defined( $const ) )
        {
            return null;
        }
        return constant( $const );
    }

    /**
     * Declares the default contextual category as a shared PHPUnit resource.
     * @beforeClass
     */
    public static function bootstrapDefaultContextualCategory()
    {
        $class = get_called_class();
        self::$_default_contextual_category = $class::_declareDefaultContextualCategory();
    }

    /**
     * Resets the default contextual category to null.
     * @afterClass
     */
    public static function teardownDefaultContextualCategory()
    {
        self::$_default_contextual_category = null;
    }

    /**
     * Override this method to declare an default
     * contextual sub-category for the test class.
     * @return null|scalar
     */
    protected static function _declareDefaultContextualSubCategory()
    {
        $class = get_called_class();
        $const = $class . '::TEST_CLASS_DEFAULT_CONTEXTUAL_SUBCATEGORY';
        if ( !defined( $const ) )
        {
            return null;
        }
        return constant( $const );
    }

    /**
     * Declares the default contextual sub-category as a shared PHPUnit resource.
     * @beforeClass
     */
    public static function bootstrapDefaultContextualSubCategory()
    {
        $class = get_called_class();
        self::$_default_contextual_subcategory = $class::_declareDefaultContextualSubCategory();
    }

    /**
     * Resets the default contextual sub-category to null.
     * @afterClass
     */
    public static function teardownDefaultContextualSubCategory()
    {
        self::$_default_contextual_subcategory = null;
    }

    /**
     * Override this method to declare an default
     * contextual package for the test class.
     * @return null|scalar
     */
    protected static function _declareDefaultContextualPackage()
    {
        $class = get_called_class();
        $const = $class . '::TEST_CLASS_DEFAULT_CONTEXTUAL_PACKAGE';
        if ( !defined( $const ) )
        {
            return null;
        }
        return constant( $const );
    }

    /**
     * Declares the default contextual package as a shared PHPUnit resource.
     * @beforeClass
     */
    public static function bootstrapDefaultContextualPackage()
    {
        $class = get_called_class();
        self::$_default_contextual_package = $class::_declareDefaultContextualPackage();
    }

    /**
     * Resets the default contextual package to null.
     * @afterClass
     */
    public static function teardownDefaultContextualPackage()
    {
        self::$_default_contextual_package = null;
    }

    /**
     * Override this method to declare an default
     * contextual sub-package for the test class.
     * @return null|scalar
     */
    protected static function _declareDefaultContextualSubPackage()
    {
        $class = get_called_class();
        $const = $class . '::TEST_CLASS_DEFAULT_CONTEXTUAL_SUBPACKAGE';
        if ( !defined( $const ) )
        {
            return null;
        }
        return constant( $const );
    }

    /**
     * Declares the default contextual sub-package as a shared PHPUnit resource.
     * @beforeClass
     */
    public static function bootstrapDefaultContextualSubPackage()
    {
        $class = get_called_class();
        self::$_default_contextual_subpackage = $class::_declareDefaultContextualSubPackage();
    }

    /**
     * Resets the default contextual sub-package to null.
     * @afterClass
     */
    public static function teardownDefaultContextualSubPackage()
    {
        self::$_default_contextual_subpackage = null;
    }

}
