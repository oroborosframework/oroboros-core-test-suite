<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\traits\assertions\context;

/**
 * <Oroboros Core Contextual Auto-Categorical Evaluation Trait>
 * Provides assertions for insuring the integrity of auto-categorical
 * contextual objects.
 *
 * Auto-categorical context objects declare their category and sub-category
 * automatically from their OROBOROS_CLASS_TYPE and OROBOROS_CLASS_SCOPE
 * class constants respectively, if those constants are defined and the
 * category and subcategory is not otherwise declared.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category traits
 * @category unit-testing
 * @category assertions
 * @package oroboros/tests
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 */
trait ContextualAutoCategoricalEvaluationTrait
{

    /**
     * AutoCategorical class context categories are based on
     * the OROBOROS_CLASS_TYPE class constant by default.
     *
     * This should be overridden if an alternate category
     * declaration applies.
     *
     * @return null|scalar
     */
    protected static function _declareExpectedContextualCategory()
    {
        $prior = parent::_declareExpectedContextualCategory();
        if ( !is_null( $prior ) )
        {
            //If a non-null context category is declared, use that.
            return $prior;
        }
        $class = get_called_class();
        $test_class = $class::TEST_CLASS;
        $const = $test_class . '::OROBOROS_CLASS_TYPE';
        if ( !class_exists( $test_class ) || !defined( $const ) )
        {
            return null;
        }
        return constant( $const );
    }

    /**
     * AutoCategorical class context categories are based on
     * the OROBOROS_CLASS_SCOPE class constant by default.
     *
     * This should be overridden if an alternate sub-category
     * declaration applies.
     *
     * @return null|scalar
     */
    protected static function _declareExpectedContextualSubCategory()
    {
        $prior = parent::_declareExpectedContextualSubCategory();
        if ( !is_null( $prior ) )
        {
            //If a non-null context sub-category is declared, use that.
            return $prior;
        }
        $class = get_called_class();
        $test_class = $class::TEST_CLASS;
        $const = $test_class . '::OROBOROS_CLASS_SCOPE';
        if ( !class_exists( $test_class ) || !defined( $const ) )
        {
            return null;
        }
        return constant( $const );
    }
}
