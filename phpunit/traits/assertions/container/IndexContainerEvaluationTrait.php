<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\tests\traits\assertions\container;

/**
 * <Oroboros Core Index Container Evaluation Trait>
 * Provides assertions for insuring the integrity of index container objects.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category traits
 * @category unit-testing
 * @category assertions
 * @package oroboros/tests
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 */
trait IndexContainerEvaluationTrait
{

    /**
     * Add auto-categorical contextual assertion logic.
     */
    use \oroboros\tests\traits\assertions\context\ContextualAutoCategoricalEvaluationTrait;

/**
     * Add auto-package-declarative contextual assertion logic.
     */
    use \oroboros\tests\traits\assertions\context\ContextualCoreEvaluationTrait;

    private static $_test_containers = array();

    /**
     * Initialize the mock core object, and run its initialization
     * @beforeClass
     */
    public static function bootstrapTestContainers()
    {
        self::$_test_containers = array(
            'foo' => new \oroboros\core\internal\container\Set(),
            'bar' => new \oroboros\core\internal\container\Container(),
            'baz' => new \oroboros\core\internal\context\ContextSet(),
        );
    }

    /**
     * Reset the mock object to its default state.
     * @afterClass
     */
    public static function tearDownTestContainers()
    {
        self::$_test_containers = array();
    }

}
