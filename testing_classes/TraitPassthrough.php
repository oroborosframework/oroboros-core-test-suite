<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\testclass;

/**
 * This is a class that exposes all trait methods of any bound trait to a child class.
 * @author Brian Dayhoff <mopsyd@me.com>
 */
abstract class TraitPassthrough
{

    public function __call( $name, $arguments = array() )
    {
        if ( method_exists( $this, $name ) )
        {
            return call_user_func_array( array(
                $this,
                $name ), $arguments );
        }
        throw new \InvalidArgumentException( sprintf( '[%s] is not a valid method of [%s].',
            $name, get_class( $this ) ) );
    }

    public static function __callStatic( $name, $arguments )
    {
        if ( method_exists( $this, $name ) )
        {
            return call_user_func_array( array(
                $this,
                $name ), $arguments );
        }
        throw new \InvalidArgumentException( sprintf( '[%s] is not a valid method of [%s].',
            $name, get_class( $this ) ) );
    }

}
