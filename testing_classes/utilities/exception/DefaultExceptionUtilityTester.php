<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\testclass\utilities\exception;

/**
 * This is a passthrough class for the ExceptionUtilityTrait.
 * @author Brian Dayhoff <mopsyd@me.com>
 */
class DefaultExceptionUtilityTester
    extends \oroboros\testclass\TraitPassthrough
{

    use \oroboros\core\traits\utilities\exception\ExceptionUtilityTrait
    {
        \oroboros\core\traits\utilities\exception\ExceptionUtilityTrait::getException as private test_getException;
    }

    public static function getException( $type = 'exception', $slug = 'unknown',
        $params = array(), \Exception $previous = null, $use_internal = false )
    {
        if ($use_internal)
        {
            try
            {
                throw self::test_getException( $type, $slug, $params, $previous );
            } catch ( \Exception $e )
            {
                return $e;
            }
        }
        return self::test_getException( $type, $slug, $params, $previous );
    }

}
