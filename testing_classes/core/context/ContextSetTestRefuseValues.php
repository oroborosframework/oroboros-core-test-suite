<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\testclass\core\context;

/**
 * This class tests firing the invalid value constraint for context sets,
 * which only fires by extension, but needs unit testing.
 * @author Brian Dayhoff <mopsyd@me.com>
 */
class ContextSetTestRefuseValues
    implements \oroboros\core\interfaces\contract\core\context\ContextSetContract
{

    use \oroboros\core\traits\core\context\ContextSetTrait;

    const OROBOROS_CLASS_TYPE = \oroboros\core\interfaces\enumerated\type\CoreClassTypes::CLASS_TYPE_CORE;
    const OROBOROS_CLASS_SCOPE = \oroboros\core\interfaces\enumerated\scope\CoreClassScopes::CLASS_SCOPE_CORE;
    const OROBOROS_CLASS_CONTEXT = \oroboros\core\interfaces\enumerated\context\CoreClassContexts::CLASS_CONTEXT_CORE;
    const OROBOROS_API = \oroboros\core\interfaces\enumerated\index\ApiIndex::INDEX_CORE_API;
    const OROBOROS_CORE = true;

    /**
     * Invalidates all types
     * @param type $type
     * @return bool
     */
    protected function _contextSetGetByType( $type )
    {
        throw new \Exception( 'All of the things are fail.' );
    }

    /**
     * Invalidates all contexts
     * @param type $context
     * @return bool
     */
    protected function _contextSetGetByContext( $context )
    {
        throw new \Exception( 'All of the things are fail.' );
    }

    /**
     * Invalidates all values
     * @param type $value
     * @return bool
     */
    protected function _contextSetGetByValue( $value )
    {
        throw new \Exception( 'All of the things are fail.' );
    }

    /**
     * Invalidates all categories
     * @param type $category
     * @return bool
     */
    protected function _contextSetGetByCategory( $category )
    {
        throw new \Exception( 'All of the things are fail.' );
    }

    /**
     * Invalidates all sub-categories
     * @param type $subcategory
     * @return bool
     */
    protected function _contextSetGetBySubcategory( $subcategory )
    {
        throw new \Exception( 'All of the things are fail.' );
    }

}
