<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\testclass\core\container;

/**
 * This is a default class to test the base level index logic.
 * It only overrides the single abstract method required to
 * create a valid index object.
 * @author Brian Dayhoff <mopsyd@me.com>
 */
class ContainerIndexTestClass
    extends \oroboros\core\abstracts\core\container\AbstractIndex
    implements \oroboros\core\interfaces\api\core\CoreApi
{

    public function __construct( $size = -1 )
    {
        parent::__construct( $size );
        $this->_setContextType( 'unit-test' );
        $this->_setContextSubType( 'unit-test' );
        $this->_setContextCategory( 'unit-test' );
        $this->_setContextSubCategory( 'unit-test' );
    }

    protected function _declareContract()
    {
        return '\\oroboros\\core\\interfaces\\contract\\core\\container\\ContainerContract';
    }

}
