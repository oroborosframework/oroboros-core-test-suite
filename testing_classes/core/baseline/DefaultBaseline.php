<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\testclass\core\baseline;

/**
 * This is just a simple empty test class.
 * It does nothing special.
 * @author Brian Dayhoff <mopsyd@me.com>
 */
class DefaultBaseline
    extends DefaultStaticBaseline
    implements \oroboros\core\interfaces\contract\core\BaselineContract
{

    use \oroboros\core\traits\core\BaselineTrait
    {
        \oroboros\core\traits\core\BaselineTrait::__construct as private __baselineConstruct;
    }

    const OROBOROS_CLASS_TYPE = 'testing class type';
    const OROBOROS_CLASS_SCOPE = 'testing class scope';
    const OROBOROS_API = 'testing class api';

    public function __construct()
    {
        $vars = func_get_args();
        $params = array_key_exists( 0, $vars )
            ? $vars[0]
            : null;
        $dependencies = array_key_exists( 1, $vars )
            ? $vars[1]
            : null;
        $flags = array_key_exists( 2, $vars )
            ? $vars[2]
            : null;
//        $this->_baselineDisableAutoInitialize();
        $this->_baselineEnableParameterValidation( true );
        $this->_baselineEnableDependencyValidation();
        $this->_baselineEnableFlagValidation();
        $this->_baselineSetFlagPersistence( false );
        $this->_baselineSetDependencyPersistence( false );
        $this->_baselineSetParameterPersistence( false );
        $this->__baselineConstruct( $params, $dependencies, $flags );
    }

    protected function _baselineSetParametersValid()
    {
        return array(
            'foo' => 'string',
            'bar' => 'stdClass',
            'baz' => 'boolean',
            'bazbar' => 'string'
        );
    }

    protected function _baselineSetParametersRequired()
    {
        return array(
            'bazbar' );
    }

    protected function _baselineSetDependenciesValid()
    {
        return array(
            'foo' => 'stdClass',
            'bar' => 'stdClass'
        );
    }

    protected function _baselineSetDependenciesRequired()
    {
        return array(
            'foo',
            'bar'
        );
    }

    protected function _baselineSetFlagsRequired()
    {
        return array(
            'bazbarbaz' );
    }

    protected function _baselineSetFlagsValid()
    {
        return array(
            'foo',
            'bar',
            'baz',
            'bazbarbaz' );
    }

}
