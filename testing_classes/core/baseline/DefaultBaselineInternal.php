<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\testclass\core\baseline;

/**
 * This is a test class that publicly exposes all of the methods in the
 * BaselineInternalsTrait for public use so test cases can directly
 * access them as needed.
 * @author Brian Dayhoff <mopsyd@me.com>
 */
class DefaultBaselineInternal
{

    use \oroboros\core\traits\core\BaselineInternalsTrait
    {
        \oroboros\core\traits\core\BaselineInternalsTrait::_baselineInternalsCheckConfiguration as private b_baselineInternalsCheckConfiguration;
        \oroboros\core\traits\core\BaselineInternalsTrait::_baselineInternalsMergeArrayDefaults as private b_baselineInternalsMergeArrayDefaults;
        \oroboros\core\traits\core\BaselineInternalsTrait::_baselineInternalsCheckValid as private b_baselineInternalsCheckValid;
        \oroboros\core\traits\core\BaselineInternalsTrait::_baselineInternalsThrowInvalidException as private b_baselineInternalsThrowInvalidException;
        \oroboros\core\traits\core\BaselineInternalsTrait::_baselineInternalsValidateValues as private b_baselineInternalsValidateValues;
        \oroboros\core\traits\core\BaselineInternalsTrait::_getException as private b_getException;
        \oroboros\core\traits\core\BaselineInternalsTrait::_getExceptionCode as private b_getExceptionCode;
        \oroboros\core\traits\core\BaselineInternalsTrait::_getExceptionCodeIdentifiers as private b_getExceptionCodeIdentifiers;
        \oroboros\core\traits\core\BaselineInternalsTrait::_getExceptionIdentifier as private b_getExceptionIdentifier;
        \oroboros\core\traits\core\BaselineInternalsTrait::_getExceptionIdentifiers as private b_getExceptionIdentifiers;
        \oroboros\core\traits\core\BaselineInternalsTrait::_getExceptionMessage as private b_getExceptionMessage;
        \oroboros\core\traits\core\BaselineInternalsTrait::_recastException as private b_recastException;
        \oroboros\core\traits\core\BaselineInternalsTrait::_throwException as private b_throwException;
        \oroboros\core\traits\core\BaselineInternalsTrait::_validate as private b_validate;
        \oroboros\core\traits\core\BaselineInternalsTrait::_validationModes as private b_validationModes;
    }

    const OROBOROS_CLASS_TYPE = 'testing class type';
    const OROBOROS_CLASS_SCOPE = 'testing class scope';
    const OROBOROS_API = 'testing class api';

    public function __construct()
    {

    }

    public static function _baselineInternalsCheckConfiguration( $type,
        $default, $required, $valid, $enabled = false )
    {
        return self::b_baselineInternalsCheckConfiguration( $type, $default,
            $required, $valid, $enabled );
    }

    public static function _baselineInternalsMergeArrayDefaults( $provided = null,
        $existing = null )
    {
        return self::b_baselineInternalsMergeArrayDefaults( $provided, $existing );
    }

    public static function _baselineInternalsCheckValid( $mode, $parameter,
        $valid, $override = false )
    {
        return self::b_baselineInternalsCheckValid( $mode, $parameter, $valid,
            $override );
    }

    public static function _baselineInternalsThrowInvalidException( $missing,
        $invalid, $type, $method )
    {
        return self::b_baselineInternalsThrowInvalidException( $missing, $invalid,
            $type, $method );
    }

    public static function _baselineInternalsValidateValues( $evaluate,
        $expected, $skip_extras = true, $use_strict_checks = false,
        $throw_exception_on_failure = false, $return_invalid_on_failure = false )
    {
        return self::b_baselineInternalsValidateValues( $evaluate, $expected,
            $skip_extras, $use_strict_checks, $throw_exception_on_failure,
            $return_invalid_on_failure );
    }

    public static function _getException( $type = 'exception',
        $slug = 'unknown', $params = array(), \Exception $previous = null,
        $context = array() )
    {
        return self::b_getException( $type, $slug, $params, $previous, $context );
    }

    public static function _getExceptionCode( $slug = 'unknown' )
    {
        return self::b_getExceptionCode( $slug );
    }

    public static function _getExceptionCodeIdentifiers()
    {
        return self::b_getExceptionCodeIdentifiers();
    }

    public static function _getExceptionIdentifier( $code = 0 )
    {
        return self::b_getExceptionIdentifier( $code );
    }

    public static function _getExceptionIdentifiers()
    {
        return self::b_getExceptionIdentifiers();
    }

    public static function _getExceptionMessage( $slug = 'unknown' )
    {
        return self::b_getExceptionMessage( $slug );
    }

    public static function _recastException( \Exception $exception, $type = null )
    {
        return self::b_recastException( $exception, $type );
    }

    public static function _throwException( $type = 'exception', $message = '',
        $code = 0, \Exception $previous = null, $context = array() )
    {
        return self::b_throwException( $type, $message, $code, $previous, $context );
    }

    public static function _validate( $mode, $subject, $valid = null,
        $throw_exception_on_invalid = false )
    {
        return self::b_validate( $mode, $subject, $valid, $throw_exception_on_invalid );
    }

    public static function _validationModes()
    {
        return self::b_validationModes();
    }

    public static function getCompiledFingerprint()
    {
        return 'fake-fingerprint';
    }

}
